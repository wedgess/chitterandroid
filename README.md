# Chitter -- Android Chat Application #
-------------------------------------------------

### Project Aim ###
The aim of this project was to learn how a chat application works in terms of communication between a client and server. The project allowed me to work on my current programming skills in PHP, MySQL, Java and XML.
Note this repository is the Android side only. The REST API built with PHP is in a separate repository.

## How the application looks: ##
----------------------------------------

![Alt text](http://i.imgur.com/WNqy8oF.png "Chat Home Screen")

![Alt text](http://i.imgur.com/BFr2Jee.png "Contact Selection Screen")

![Alt text](http://i.imgur.com/eajJkvz.png "Chat Room Screen")

![Alt text](http://i.imgur.com/0GlYnPB.png "Group Info Screen")