package eu.wedgess.chitterandroid;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import eu.wedgess.chitterandroid.activities.ChatHomeActivity;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;

/**
 * Extend application and implement ActivityLifeCycleCallbacks so that we can check when user exits
 * the application of comes back online. This is then updated on each ot their contacts devices whom
 * they are in chats with.
 * Created by Gareth on 24/12/2016.
 */

public class ChitterApplication extends Application {

    public final String TAG = ChitterApplication.class.getSimpleName();

    public boolean wasInBackground = true;

    private Timer mActivityTransitionTimer;
    private TimerTask mActivityTransitionTimerTask;
    private final long MAX_ACTIVITY_TRANSITION_TIME_MS = 2000;  // Time allowed for transitions

    Application.ActivityLifecycleCallbacks activityCallbacks = new Application.ActivityLifecycleCallbacks() {

        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            if (wasInBackground) {
                //Do app-wide came-here-from-background code
                appEntered();
            }
            if (activity instanceof ChatHomeActivity) {
                ChitterModel.getInstance(getApplicationContext()).setShowNotification(false);
                ChitterModel.getInstance(getApplicationContext()).setCurrentChat(null);
            }
            stopActivityTransitionTimer();
        }

        @Override
        public void onActivityPaused(Activity activity) {
            startActivityTransitionTimer();
        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(activityCallbacks);
    }

    public void startActivityTransitionTimer() {
        this.mActivityTransitionTimer = new Timer();
        this.mActivityTransitionTimerTask = new TimerTask() {
            public void run() {
                // Task is run when app is exited
                wasInBackground = true;
                appExited();
            }
        };

        this.mActivityTransitionTimer.schedule(mActivityTransitionTimerTask,
                MAX_ACTIVITY_TRANSITION_TIME_MS);
    }

    public void stopActivityTransitionTimer() {
        if (this.mActivityTransitionTimerTask != null) {
            this.mActivityTransitionTimerTask.cancel();
        }

        if (this.mActivityTransitionTimer != null) {
            this.mActivityTransitionTimer.cancel();
        }

        this.wasInBackground = false;
    }

    private void appEntered() {
        Log.i(TAG, "APP ENTERED");
        // entered app set status as online
        if (ChitterModel.getInstance(getApplicationContext()).getUser(getApplicationContext()) != null) {
            APIRequestUtility.updateLastSeenRequest(Constants.STATUS_ONLINE, getApplicationContext());
        }
        // don't show notifications so long as current chat is null when app enters
        // this also fixes message delivery updates being sent
        if (ChitterModel.getInstance(getApplicationContext()).getCurrentChat() == null) {
            ChitterModel.getInstance(getApplicationContext()).setShowNotification(false);
        }
    }

    private void appExited() {
        Log.i(TAG, "APP EXITED");
        // exited app set new status on server
        if (ChitterModel.getInstance(getApplicationContext()).getUser(getApplicationContext()) != null) {
            APIRequestUtility.updateLastSeenRequest("", getApplicationContext());
        }
        // set models values about showing or not showing notifications
        ChitterModel.getInstance(getApplicationContext()).setShowNotification(true);
        ChitterModel.getInstance(getApplicationContext()).setCurrentChat(null);
    }
}
