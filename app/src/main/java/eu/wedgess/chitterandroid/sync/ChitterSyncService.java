package eu.wedgess.chitterandroid.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import eu.wedgess.chitterandroid.adapters.SyncAdapter;

/**
 * SyncService for SyncAdapter
 * Created by Gareth on 19/01/2017.
 */

public class ChitterSyncService extends Service {

    public static final String TAG = SyncAdapter.class.getSimpleName();
    private static final Object sAdapterLock = new Object();
    private static SyncAdapter sAdapter = null;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "onCreate()");
        synchronized (sAdapterLock) {
            if (sAdapter == null) {
                sAdapter = new SyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sAdapter.getSyncAdapterBinder();
    }
}
