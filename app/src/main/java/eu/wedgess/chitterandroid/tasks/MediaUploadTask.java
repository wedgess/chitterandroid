package eu.wedgess.chitterandroid.tasks;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import eu.wedgess.chitterandroid.utils.APIRequestUtility;

import static eu.wedgess.chitterandroid.utils.APIRequestUtility.BASE_URL;
import static eu.wedgess.chitterandroid.utils.APIRequestUtility.SERVER_UPLOADS_PATH;

/**
 * This class is used to upload media to and from the server.
 * We use HttpURLConnection class to do so as Volley can;t send multipart data from
 * what I have read. This performs the upload in an AsyncTask so it is run in the background thread.
 * <p>
 * Created by Gareth on 19/12/2016.
 */

public class MediaUploadTask extends AsyncTask<Uri, Integer, Integer> {

    private static final String TAG = MediaUploadTask.class.getSimpleName();

    private UploadProgressListener mUploadProgressListener;
    private String mFileName = null;
    private Context mContext;

    public interface UploadProgressListener {
        void onUploadComplete(int responseCode, String uploadPath);

        void onUploadError(final String error);
    }

    public MediaUploadTask(Context context, UploadProgressListener uploadProgressListener) {
        this.mContext = context;
        this.mUploadProgressListener = uploadProgressListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected Integer doInBackground(Uri... mediaUris) {
        return uploadFile(getPath(mediaUris[0]));
    }

    @Override
    protected void onPostExecute(Integer responseCode) {
        super.onPostExecute(responseCode);
        mUploadProgressListener.onUploadComplete(responseCode, BASE_URL + SERVER_UPLOADS_PATH + mFileName);
    }


    /**
     * Gets the file path as a string from a URI.
     * This handles all types of media from documents, images, audio and video
     *
     * @param uri
     * @return
     */
    @SuppressLint("NewApi")
    private String getPath(final Uri uri) {
        // DocumentProvider
        if (DocumentsContract.isDocumentUri(mContext, uri)) {
            // ExternalStorageProvider -- selecting file from external storage
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider - downloads
            else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(mContext, contentUri, null, null);
            }
            // MediaProvider - media document image, video, audio
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(mContext, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(mContext, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The mContext.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {
        Cursor cursor = null;
        // column we want
        final String column = "_data";
        final String[] projection = {
                column
        };

        // store  the value of the _data column, which is a file path
        String filePath = null;

        try
        {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                filePath = cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return filePath;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    private int uploadFile(final String sourceFileUri) {

        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        Log.i(TAG, "Source URI: " + sourceFileUri);
        File sourceFile = new File(sourceFileUri);
        // name of file which we will use on for storing on server
        this.mFileName = sourceFile.getName();
        int serverResponseCode = 0;

        if (!sourceFile.isFile()) {
            Log.e("uploadFile", "Source File not exist :" + sourceFileUri);
            mUploadProgressListener.onUploadError("Source file does not exist: " + sourceFileUri);

            return 0;

        } else {
            try {

                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(BASE_URL + APIRequestUtility.ENDPOINT_UPLOAD_MEDIA);

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                // set file name in the request headers
                conn.setRequestProperty("file", mFileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\""
                        + mFileName + "\"" + lineEnd);

                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();

//                // TODO: Handy for debugging server response
//                // StringBuilder for response
                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String line;
                // while response line is not null -- for response body
                while ((line = reader.readLine()) != null) {
                    sb.append(line).append("\n"); // append line to StringBuilder
                }
                Log.i(TAG, "Server response: " + sb.toString());
//                //TODO: DOwn to here is debugging server response

                Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);

                if (serverResponseCode == 200) {
                    String msg = "File Upload Completed.\n\n See uploaded file here : \n\n"
                            + BASE_URL + SERVER_UPLOADS_PATH + mFileName;
                    Log.i(TAG, msg);
                }

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();

            } catch (MalformedURLException ex) {
                ex.printStackTrace();

                Log.e(TAG, "Error Malformed URL: " + ex.getMessage(), ex);
                mUploadProgressListener.onUploadError("Error Malformed URL: " + ex.getMessage());
            } catch (Exception e) {

//                dialog.dismiss();
                e.printStackTrace();
                Log.e(TAG, "Error, Exception : " + e.getMessage(), e);
                mUploadProgressListener.onUploadError("Error, Exception : " + e.getMessage());
            }
//            dialog.dismiss();
            return serverResponseCode;

        } // End else block
    }
}
