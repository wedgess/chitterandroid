package eu.wedgess.chitterandroid.tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import eu.wedgess.chitterandroid.helpers.UIHelper;

/**
 * AsyncTask to get image from URL for some reason was getting {@link android.os.NetworkOnMainThreadException}
 * when using Picasso from Service so get the image using an AsynTask and HttpUrlConnection.
 * Created by Gareth on 13/02/2017.
 */

public class GetImageTask extends AsyncTask<String, Integer, Bitmap> {

    // listener for when the image has been downloaded
    public interface GetImageListener {
        void onComplete(Bitmap bitmap);
    }

    private GetImageListener imageListener;

    public GetImageTask(GetImageListener listener) {
        this.imageListener = listener;
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        return getBitmapFromURL(strings[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        imageListener.onComplete(bitmap);
    }

    private Bitmap getBitmapFromURL(String strURL) {
        try {
            // get image from url and decode to BitMap
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            // crop bitmap and return it
            return UIHelper.getCroppedBitmap(myBitmap, myBitmap.getWidth());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
