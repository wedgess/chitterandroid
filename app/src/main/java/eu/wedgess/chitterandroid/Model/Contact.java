package eu.wedgess.chitterandroid.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import eu.wedgess.chitterandroid.db.DBConstants;

import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_CREATED_AT;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_EXIST_ON_DEVICE;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_IMG_URL;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_LAST_SEEN;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_NAME;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_PHONE_NUMBER;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_STATUS;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_USER_ID;

/**
 * A java bean class used to hold a contacts information, super class of user
 * Implements {@link Parcelable} so it can be passed into bundles between activities/fragments.
 * <p>
 * Created by Gareth on 05/12/2016.
 */
public class Contact implements Parcelable {

    private long mId;
    private String mName;
    private String mPhoneNumber;
    private String mImgUrl;
    private String mCreatedAt;
    private String mLastSeen;
    private String mStatus;
    private boolean mExistOnDevice;

    public Contact() {
        this(-1, null, null, null, null, null);
    }

    public Contact(long id, String name, String phoneNumber, String createdAt, String lastSeen, String status) {
        this.mId = id;
        this.mName = name;
        this.mPhoneNumber = phoneNumber;
        this.mCreatedAt = createdAt;
        this.mLastSeen = lastSeen;
        this.mStatus = status;
    }

    public Contact(long id, String name, String phoneNumber, String imgUrl, String createdAt, String lastSeen, String status) {
        this(id, name, phoneNumber, createdAt, lastSeen, status);
        this.mImgUrl = imgUrl;
    }

    public long getId() {
        return mId;
    }

    public void setId(long mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String mPhoneNumber) {
        this.mPhoneNumber = mPhoneNumber;
    }

    public String getCreatedAt() {
        return mCreatedAt;
    }

    public void setCreatedAt(String mCreatedAt) {
        this.mCreatedAt = mCreatedAt;
    }

    public String getLastSeen() {
        return mLastSeen;
    }

    public void setLastSeen(String mLastSeen) {
        this.mLastSeen = mLastSeen;
    }

    public String getImgUrl() {
        return this.mImgUrl;
    }

    public void setImgUrl(String url) {
        this.mImgUrl = url;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        this.mStatus = status;
    }

    public boolean existOnDevice() {
        return mExistOnDevice;
    }

    public void setExistOnDevice(boolean existOnDevice) {
        this.mExistOnDevice = existOnDevice;
    }


    /**
     * Builds contents values for User TABLE
     *
     * @return ContentValues
     */
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CONTACTS_USER_ID, this.getId());
        values.put(COLUMN_CONTACTS_NAME, this.getName());
        values.put(COLUMN_CONTACTS_PHONE_NUMBER, this.getPhoneNumber());
        values.put(COLUMN_CONTACTS_IMG_URL, this.getImgUrl());
        values.put(COLUMN_CONTACTS_LAST_SEEN, this.getLastSeen());
        values.put(COLUMN_CONTACTS_CREATED_AT, this.getCreatedAt());
        values.put(DBConstants.COLUMN_CONTACTS_STATUS, this.getStatus());
        values.put(DBConstants.COLUMN_CONTACTS_EXIST_ON_DEVICE, this.existOnDevice() ? 1 : 0);
        return values;
    }

    /**
     * Constructor to build Contact object from content provider
     * {@link eu.wedgess.chitterandroid.db.ChitterContentProvider}
     *
     * @param cursor - the cursor
     */
    public Contact(Cursor cursor) {
        this.mId = cursor.getLong(cursor.getColumnIndex(COLUMN_CONTACTS_USER_ID));
        this.mName = cursor.getString(cursor.getColumnIndex(COLUMN_CONTACTS_NAME));
        this.mPhoneNumber = cursor.getString(cursor.getColumnIndex(COLUMN_CONTACTS_PHONE_NUMBER));
        this.mImgUrl = cursor.getString(cursor.getColumnIndex(COLUMN_CONTACTS_IMG_URL));
        this.mLastSeen = cursor.getString(cursor.getColumnIndex(COLUMN_CONTACTS_LAST_SEEN));
        this.mStatus = cursor.getString(cursor.getColumnIndex(COLUMN_CONTACTS_STATUS));
        this.mCreatedAt = cursor.getString(cursor.getColumnIndex(COLUMN_CONTACTS_CREATED_AT));
        this.mExistOnDevice = cursor.getInt(cursor.getColumnIndex(COLUMN_CONTACTS_EXIST_ON_DEVICE)) == 1;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "mId=" + mId +
                ", mName='" + mName + '\'' +
                ", mPhoneNumber='" + mPhoneNumber + '\'' +
                ", mImgUrl='" + mImgUrl + '\'' +
                ", mCreatedAt='" + mCreatedAt + '\'' +
                ", mLastSeen='" + mLastSeen + '\'' +
                ", mStatus='" + mStatus + '\'' +
                ", mExistOnDevice=" + mExistOnDevice +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        if (mId != contact.mId) return false;
        if (mExistOnDevice != contact.mExistOnDevice) return false;
        if (mName != null ? !mName.equals(contact.mName) : contact.mName != null) return false;
        if (mPhoneNumber != null ? !mPhoneNumber.equals(contact.mPhoneNumber) : contact.mPhoneNumber != null)
            return false;
        if (mImgUrl != null ? !mImgUrl.equals(contact.mImgUrl) : contact.mImgUrl != null)
            return false;
        if (mCreatedAt != null ? !mCreatedAt.equals(contact.mCreatedAt) : contact.mCreatedAt != null)
            return false;
        if (mLastSeen != null ? !mLastSeen.equals(contact.mLastSeen) : contact.mLastSeen != null)
            return false;
        return mStatus != null ? mStatus.equals(contact.mStatus) : contact.mStatus == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (mId ^ (mId >>> 32));
        result = 31 * result + (mName != null ? mName.hashCode() : 0);
        result = 31 * result + (mPhoneNumber != null ? mPhoneNumber.hashCode() : 0);
        result = 31 * result + (mImgUrl != null ? mImgUrl.hashCode() : 0);
        result = 31 * result + (mCreatedAt != null ? mCreatedAt.hashCode() : 0);
        result = 31 * result + (mLastSeen != null ? mLastSeen.hashCode() : 0);
        result = 31 * result + (mStatus != null ? mStatus.hashCode() : 0);
        result = 31 * result + (mExistOnDevice ? 1 : 0);
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.mId);
        dest.writeString(this.mName);
        dest.writeString(this.mPhoneNumber);
        dest.writeString(this.mImgUrl);
        dest.writeString(this.mCreatedAt);
        dest.writeString(this.mLastSeen);
        dest.writeString(this.mStatus);
        dest.writeByte(this.mExistOnDevice ? (byte) 1 : (byte) 0);
    }

    protected Contact(Parcel in) {
        this.mId = in.readLong();
        this.mName = in.readString();
        this.mPhoneNumber = in.readString();
        this.mImgUrl = in.readString();
        this.mCreatedAt = in.readString();
        this.mLastSeen = in.readString();
        this.mStatus = in.readString();
        this.mExistOnDevice = in.readByte() != 0;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };
}
