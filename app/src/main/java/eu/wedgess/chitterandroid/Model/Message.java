package eu.wedgess.chitterandroid.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_CHAT_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_CREATED_AT;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_DELETED;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_IMG_URL;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_MSG;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_SENDER_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_STATUS;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_TYPE;

/**
 * Message POJO which stores a rows values from the Messages table in DB.
 * Implements {@link Parcelable} so it can be passed into bundles between activities/fragments.
 * Created by Gareth on 05/12/2016.
 */

public class Message implements Parcelable {

    public static final int STATUS_PENDING = 0;
    public static final int STATUS_DELIVERED_TO_SERVER = 1;
    public static final int STATUS_DELIVERED_TO_USER = 2;
    public static final int STATUS_READ_BY_USER = 3;

    public static final int TYPE_TEXT = 0;
    public static final int TYPE_IMG = 1;

    private String id;
    private String cId;
    private long senderId;
    private String message;
    private String createdAt;
    private String imgUrl;
    private int status;
    private int type;
    private boolean isDeleted;

    public Message() {
        this(null, null, -1, null, null, STATUS_PENDING, 0);
    }

    public Message(String id, String cid, long sender, String message, String createdAt, int status, int type) {
        this.id = id;
        this.cId = cid;
        this.senderId = sender;
        this.message = message;
        this.createdAt = createdAt;
        this.status = status;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCid() {
        return cId;
    }

    public void setCid(String cId) {
        this.cId = cId;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getImgUrl() {
        return this.imgUrl;
    }

    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public boolean isImage() {
        return this.type == TYPE_IMG;
    }

    public boolean isText() {
        return this.type == TYPE_TEXT;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id='" + id + '\'' +
                ", cId='" + cId + '\'' +
                ", senderId=" + senderId +
                ", message='" + message + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", status=" + status +
                ", type=" + type +
                ", isDeleted=" + isDeleted +
                '}';
    }

    /**
     * Builds contents values for Message TABLE
     *
     * @return ContentValues
     */
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(COLUMN_MESSAGE_ID, this.getId());
        values.put(COLUMN_MESSAGE_CHAT_ID, this.getCid());
        values.put(COLUMN_MESSAGE_SENDER_ID, this.getSenderId());
        values.put(COLUMN_MESSAGE_MSG, this.getMessage());
        values.put(COLUMN_MESSAGE_IMG_URL, this.getImgUrl());
        values.put(COLUMN_MESSAGE_CREATED_AT, this.getCreatedAt());
        values.put(COLUMN_MESSAGE_STATUS, this.getStatus());
        values.put(COLUMN_MESSAGE_TYPE, this.getType());
        values.put(COLUMN_MESSAGE_DELETED, this.isDeleted() ? 1 : 0);
        return values;
    }

    /**
     * Build Message object from cursor object
     *
     * @param cursor - the cursor
     */
    public Message(Cursor cursor) {
        // use id which was passed in
        this.id = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_ID));
        this.cId = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_CHAT_ID));
        this.senderId = cursor.getLong(cursor.getColumnIndex(COLUMN_MESSAGE_SENDER_ID));
        this.message = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_MSG));
        this.createdAt = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_CREATED_AT));
        this.imgUrl = cursor.getString(cursor.getColumnIndex(COLUMN_MESSAGE_IMG_URL));
        this.status = cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGE_STATUS));
        this.type = cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGE_TYPE));
        this.isDeleted = cursor.getInt(cursor.getColumnIndex(COLUMN_MESSAGE_DELETED)) == 1;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.cId);
        dest.writeLong(this.senderId);
        dest.writeString(this.message);
        dest.writeString(this.createdAt);
        dest.writeString(this.imgUrl);
        dest.writeInt(this.status);
        dest.writeInt(this.type);
        dest.writeByte(this.isDeleted ? (byte) 1 : (byte) 0);
    }

    protected Message(Parcel in) {
        this.id = in.readString();
        this.cId = in.readString();
        this.senderId = in.readLong();
        this.message = in.readString();
        this.createdAt = in.readString();
        this.imgUrl = in.readString();
        this.status = in.readInt();
        this.type = in.readInt();
        this.isDeleted = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
