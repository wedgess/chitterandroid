package eu.wedgess.chitterandroid.model;

import android.content.ContentValues;
import android.database.Cursor;

import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.helpers.Constants;

/**
 * Used to sync user chats locally and remotely.
 * Created by Gareth on 10/02/2017.
 */

public class UsersChats {

    private long uid;
    private String cid;

    public UsersChats() {
        this(Constants.INVALID_POSITION, null);
    }

    public UsersChats(long uid, String cid) {

        this.uid = uid;
        this.cid = cid;
    }

    public UsersChats(Cursor usersChatsCursor) {
        this.uid = usersChatsCursor.getLong(usersChatsCursor.getColumnIndex(DBConstants.COLUMN_USERS_CHATS_UID));
        this.cid = usersChatsCursor.getString(usersChatsCursor.getColumnIndex(DBConstants.COLUMN_USERS_CHATS_CID));
    }

    public ContentValues getContentValues() {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DBConstants.COLUMN_USERS_CHATS_UID, this.getUid());
        contentValues.put(DBConstants.COLUMN_USERS_CHATS_CID, this.getCid());
        return contentValues;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public String getCid() {
        return cid;
    }

    public void setCid(String cid) {
        this.cid = cid;
    }

    @Override
    public String toString() {
        return "UsersChats{" +
                "uid=" + uid +
                ", cid='" + cid + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UsersChats that = (UsersChats) o;

        if (uid != that.uid) return false;
        return cid != null ? cid.equals(that.cid) : that.cid == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (uid ^ (uid >>> 32));
        result = 31 * result + (cid != null ? cid.hashCode() : 0);
        return result;
    }
}
