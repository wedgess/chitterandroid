package eu.wedgess.chitterandroid.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.utils.Utils;

import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_CREATED_AT;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_CREATED_BY;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_MUTED;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_NAME;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_PROFILE_IMG_URL;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_TYPE;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_UNREAD_COUNT;

/**
 * ChatRoom POJO which stores a rows values from the ChatRoom table in DB.
 * Implements {@link Parcelable} so it can be passed into bundles between activities/fragments.
 * <p>
 * Created by Gareth on 05/12/2016.
 */

public class ChatRoom implements Parcelable {

    public static final int USER_TO_USER_CHAT = 1;
    public static final int GROUP_CHAT = 2;

    private String id;
    private String title;
    private List<Message> messages;
    private ArrayList<Contact> members;
    private long adminId;
    private int chatType;
    private String createdAt;
    private int unreadCount;
    private String groupProfileImgUrl;
    private boolean muted;

    // default user to user chat
    public ChatRoom() {
        this(null, null, null, null, null, -1, USER_TO_USER_CHAT, Utils.getSQLTimestampFormat().format(new Date()));
    }

    public ChatRoom(String id, String title, List<Message> messages, ArrayList<Contact> members, String profileImgUrl, long adminId, int chatType, String createdAt) {
        this(id, title, messages, members, adminId, chatType, createdAt, profileImgUrl);
    }

    public ChatRoom(String id, String title, List<Message> messages, ArrayList<Contact> members, long adminId, int chatType, String createdAt, String groupProfileImgUrl) {
        this.id = id;
        this.title = title;
        this.messages = messages;
        this.members = members;
        this.adminId = adminId;
        this.chatType = chatType;
        this.createdAt = createdAt;
        this.unreadCount = 0;
        this.groupProfileImgUrl = groupProfileImgUrl;
    }

    public ChatRoom(String id, String title, List<Message> messages, ArrayList<Contact> members, long adminId, int chatType) {
        this(id, title, messages, members, null, adminId, chatType, null);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    public ArrayList<Contact> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Contact> members) {
        this.members = members;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public long getAdminId() {
        return adminId;
    }

    public void setAdmin(long adminId) {
        this.adminId = adminId;
    }

    public int getChatType() {
        return chatType;
    }

    public void setChatType(int chatType) {
        this.chatType = chatType;
    }

    public void setUnreadCount(int count) {
        this.unreadCount = count;
    }

    public int getUnreadCount() {
        return this.unreadCount;
    }

    public String getGroupProfileImgUrl() {
        return groupProfileImgUrl;
    }

    public void setGroupProfileImgUrl(String groupProfileImgUrl) {
        this.groupProfileImgUrl = groupProfileImgUrl;
    }

    public boolean isMuted() {
        return muted;
    }

    public void setMuted(boolean muted) {
        this.muted = muted;
    }

    /**
     * Builds contents values for Chat TABLE
     *
     * @return ContentValues
     */
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();
        values.put(COLUMN_CHAT_ID, this.getId());
        values.put(COLUMN_CHAT_NAME, this.getTitle());
        values.put(COLUMN_CHAT_CREATED_BY, this.getAdminId());
        values.put(COLUMN_CHAT_CREATED_AT, this.getCreatedAt());
        values.put(COLUMN_CHAT_TYPE, this.getChatType());
        values.put(COLUMN_CHAT_PROFILE_IMG_URL, this.getGroupProfileImgUrl());
        values.put(COLUMN_CHAT_UNREAD_COUNT, this.getUnreadCount());
        values.put(DBConstants.COLUMN_CHAT_MUTED, this.isMuted() ? 1 : 0);
        return values;
    }

    @Override
    public String toString() {
        return "ChatRoom{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", messages=" + messages +
                ", members=" + members +
                ", adminId=" + adminId +
                ", chatType=" + chatType +
                ", createdAt='" + createdAt + '\'' +
                ", unreadCount=" + unreadCount +
                ", groupProfileImgUrl='" + groupProfileImgUrl + '\'' +
                ", muted=" + muted +
                '}';
    }

    /**
     * Build ChatRoom object from cursor object
     *
     * @param cursor - the cursor
     */
    public ChatRoom(Cursor cursor) {
        // use id which was passed in
        this.id = cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_ID));
        this.title = cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_NAME));
        this.chatType = cursor.getInt(cursor.getColumnIndex(COLUMN_CHAT_TYPE));
        this.createdAt = cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_CREATED_AT));
        this.adminId = cursor.getLong(cursor.getColumnIndex(COLUMN_CHAT_CREATED_BY));
        this.groupProfileImgUrl = cursor.getString(cursor.getColumnIndex(COLUMN_CHAT_PROFILE_IMG_URL));
        this.unreadCount = cursor.getInt(cursor.getColumnIndex(COLUMN_CHAT_UNREAD_COUNT));
        this.muted = cursor.getInt(cursor.getColumnIndex(COLUMN_CHAT_MUTED)) == 1;
    }

    public void addMessage(Message message) {
        if (this.messages == null) {
            this.messages = new ArrayList<>();
        }
        this.messages.add(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.title);
        dest.writeTypedList(this.messages);
        dest.writeTypedList(this.members);
        dest.writeLong(this.adminId);
        dest.writeInt(this.chatType);
        dest.writeString(this.createdAt);
        dest.writeInt(this.unreadCount);
        dest.writeString(this.groupProfileImgUrl);
        dest.writeByte(this.muted ? (byte) 1 : (byte) 0);
    }

    protected ChatRoom(Parcel in) {
        this.id = in.readString();
        this.title = in.readString();
        this.messages = in.createTypedArrayList(Message.CREATOR);
        this.members = in.createTypedArrayList(Contact.CREATOR);
        this.adminId = in.readLong();
        this.chatType = in.readInt();
        this.createdAt = in.readString();
        this.unreadCount = in.readInt();
        this.groupProfileImgUrl = in.readString();
        this.muted = in.readByte() != 0;
    }

    public static final Creator<ChatRoom> CREATOR = new Creator<ChatRoom>() {
        @Override
        public ChatRoom createFromParcel(Parcel source) {
            return new ChatRoom(source);
        }

        @Override
        public ChatRoom[] newArray(int size) {
            return new ChatRoom[size];
        }
    };
}
