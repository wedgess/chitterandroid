package eu.wedgess.chitterandroid.model;

/**
 * A java bean class used to hold the menu_chat_home users info. Subclass of {@link Contact}
 * only difference is that user has a password.
 * <p>
 * Created by Gareth on 05/12/2016.
 */
public class User extends Contact {

    private String mPassword;
    private String mFcmToken;

    public User() {
        this(-1, null, null, null, null, null, null, null);
    }

    public User(long id, String name, String phoneNumber, String password, String fcmToken, String createdAt,
                String lastSeen, String status) {
        super(id, name, phoneNumber, createdAt, lastSeen, status);
        this.mPassword = password;
        this.mFcmToken = fcmToken;
    }

    public User(long id, String name, String phoneNumber, String password, String fcmToken, String imgUrl,
                String createdAt, String lastSeen, String status) {
        this(id, name, phoneNumber, fcmToken, password, createdAt, lastSeen, status);
        setImgUrl(imgUrl);
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getFcmToken() {
        return mFcmToken;
    }

    public void setFcmToken(String mFcmToken) {
        this.mFcmToken = mFcmToken;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
