package eu.wedgess.chitterandroid.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import eu.wedgess.chitterandroid.R;

/**
 * Created from: http://stackoverflow.com/a/39830788/3748532
 * <p>
 * However the above code doesn;t allow for author name in incoming messages. The layout has been modified
 * to accommodate for the author TextView and getting its width or the message width, which ever is wider.
 * <p>
 * Modified by Gareth on 11/01/2017.
 */
public class ChatBubbleLayout extends RelativeLayout {

    private static final String TAG = ChatBubbleLayout.class.getSimpleName();

    private View viewPartMain;
    private View viewPartSlave;

    private TypedArray a;

    private RelativeLayout.LayoutParams viewPartMainLayoutParams;
    private int viewPartMainWidth;
    private int viewPartMainHeight;

    private RelativeLayout.LayoutParams viewPartSlaveLayoutParams;
    private int viewPartSlaveWidth;
    private int viewPartSlaveHeight;


    public ChatBubbleLayout(Context context) {
        super(context);
    }

    public ChatBubbleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

        a = context.obtainStyledAttributes(attrs, R.styleable.ChatBubbleLayout, 0, 0);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        try {
            viewPartMain = this.findViewById(a.getResourceId(R.styleable.ChatBubbleLayout_viewPartMain, -1));
            viewPartSlave = this.findViewById(a.getResourceId(R.styleable.ChatBubbleLayout_viewPartSlave, -1));
        } catch (Exception e) {
            e.printStackTrace();
        }

//        if (this.findViewById(a.getResourceId(R.styleable.ChatBubbleLayout_viewPartMain, -1)) instanceof TextView) {
//            Log.d(TAG, "Is TextView");
//        } else if (this.findViewById(a.getResourceId(R.styleable.ChatBubbleLayout_viewPartMain, -1)) instanceof RelativeLayout) {
//            Log.d(TAG, "Is RelativeLayout");
//        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        if (viewPartMain == null || viewPartSlave == null || widthSize <= 0) {
            return;
        }

        int availableWidth = widthSize - getPaddingLeft() - getPaddingRight();
        int availableHeight = heightSize - getPaddingTop() - getPaddingBottom();

        viewPartMainLayoutParams = (LayoutParams) viewPartMain.getLayoutParams();
        viewPartMainWidth = viewPartMain.getMeasuredWidth() + viewPartMainLayoutParams.leftMargin + viewPartMainLayoutParams.rightMargin;
        viewPartMainHeight = viewPartMain.getMeasuredHeight() + viewPartMainLayoutParams.topMargin + viewPartMainLayoutParams.bottomMargin;

        viewPartSlaveLayoutParams = (LayoutParams) viewPartSlave.getLayoutParams();
        viewPartSlaveWidth = viewPartSlave.getMeasuredWidth() + viewPartSlaveLayoutParams.leftMargin + viewPartSlaveLayoutParams.rightMargin;
        viewPartSlaveHeight = viewPartSlave.getMeasuredHeight() + viewPartSlaveLayoutParams.topMargin + viewPartSlaveLayoutParams.bottomMargin;

        int viewPartMainLineCount;
        float viewPartMainLastLineWitdh = 0;
        if (viewPartMain instanceof RelativeLayout) {
            // if instance of relative layout it is an incoming message
            // group chats have an author textview so we need to take it into account, when measuring
            TextView authorTV = (TextView) ((RelativeLayout) viewPartMain).getChildAt(0);
            TextView messageTV = (TextView) ((RelativeLayout) viewPartMain).getChildAt(1);
            // get line counts of each TV
            int messageLineCount = messageTV.getLineCount();
            int authorLineCount = authorTV.getLineCount();
            // add to total line count
            viewPartMainLineCount = messageLineCount + authorLineCount;

            float msgWidth = 0;
            // get width of message TV
            if (messageTV.getLayout() != null) {
                msgWidth = messageTV.getLayout().getLineWidth(messageLineCount - 1);
            }
            float authorWidth = 0;
            // get width of author TV
            if (authorTV.getLayout() != null) {
                authorWidth = authorTV.getLayout().getLineWidth(authorLineCount - 1);
            }
            // select the max line width of the 2
            float lineWidth = Math.max(msgWidth, authorWidth);
            // set menu_chat_home line width to the max of the two TVs
            viewPartMainLastLineWitdh = viewPartMainLineCount - 1 > 0 ? lineWidth : 0;
        } else {
            // outgoing message theres only a TextView (Message Text) as menu_chat_home layout
            TextView messageTV = (TextView) viewPartMain;
            viewPartMainLineCount = messageTV.getLineCount();
            viewPartMainLastLineWitdh = viewPartMainLineCount > 0 ? messageTV.getLayout().getLineWidth(viewPartMainLineCount - 1) : 0;
        }
        widthSize = getPaddingLeft() + getPaddingRight();
        heightSize = getPaddingTop() + getPaddingBottom();

        if (viewPartMainLineCount > 1 && !(viewPartMainLastLineWitdh + viewPartSlaveWidth >= viewPartMain.getMeasuredWidth())) {
            widthSize += viewPartMainWidth;
            heightSize += viewPartMainHeight;
        } else if (viewPartMainLineCount > 1 && (viewPartMainLastLineWitdh + viewPartSlaveWidth >= availableWidth)) {
            widthSize += viewPartMainWidth;
            heightSize += viewPartMainHeight + viewPartSlaveHeight;
        } else if (viewPartMainLineCount == 1 && (viewPartMainWidth + viewPartSlaveWidth >= availableWidth)) {
            widthSize += viewPartMain.getMeasuredWidth();
            heightSize += viewPartMainHeight + viewPartSlaveHeight;
        } else {
            widthSize += viewPartMainWidth + viewPartSlaveWidth;
            heightSize += viewPartMainHeight;
        }

        this.setMeasuredDimension(widthSize, heightSize);
        super.onMeasure(MeasureSpec.makeMeasureSpec(widthSize, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(heightSize, MeasureSpec.EXACTLY));
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (viewPartMain == null || viewPartSlave == null) {
            return;
        }

        viewPartMain.layout(
                getPaddingLeft(),
                getPaddingTop(),
                viewPartMain.getWidth() + getPaddingLeft(),
                viewPartMain.getHeight() + getPaddingTop());

        viewPartSlave.layout(
                right - left - viewPartSlaveWidth - getPaddingRight(),
                bottom - top - getPaddingBottom() - viewPartSlaveHeight,
                right - left - getPaddingRight(),
                bottom - top - getPaddingBottom());
    }
}
