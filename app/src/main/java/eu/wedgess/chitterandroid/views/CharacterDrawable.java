package eu.wedgess.chitterandroid.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.helpers.UIHelper;

/**
 * A circle colored drawable with a character in the center. Like Gmail.
 * Should be placed inside an ImageView.
 * Created by Gareth on 24/08/2016.
 */
public class CharacterDrawable extends Drawable {

    public static final String TAG = CharacterDrawable.class.getSimpleName();

    private Paint textPaint;
    private final Paint circlePaint;
    private boolean textEnabled = true;
    private String name;

    public CharacterDrawable(String name, Context context) {
        super();
        this.name = name;
        //Log.d(TAG, "In constructor -------->");
        Resources resources = context.getResources();
        // paint for circles BG color
        this.circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // paint for the text
        this.textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text paint is always white, center the letter and also set its font
        textPaint.setColor(Color.WHITE);
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTypeface(Typeface.create("sans-serif-light", Typeface.BOLD));


        // BG paint color
        circlePaint.setColor(UIHelper.getColorForLetter(name, resources, R.array.letter_tile_colors));
        // anti alias to avoid pixelated edges
        circlePaint.setAntiAlias(true);
    }

    @Override
    public void draw(Canvas canvas) {
        //Log.d(TAG, "In onDraw -------->");
        RectF rect = new RectF(getBounds());
        Rect r = getBounds();

        canvas.drawOval(rect, circlePaint);

        int count = canvas.save();
        canvas.translate(r.left, r.top);

        // draw text
        int width = canvas.getWidth();
        int height = canvas.getHeight();
        //Log.i(CharacterDrawable.class.getSimpleName(), "Drawing charcater: " + String.valueOf(mFirstChar[0]));
        textPaint.setTextSize(((float) (height / 2)));
        canvas.drawText(name.substring(0, 1).toUpperCase(), width / 2, height / 2 - ((textPaint.descent() + textPaint.ascent()) / 2), textPaint);

        canvas.restoreToCount(count);
    }

    /**
     * @param c The char to check
     * @return True if <code>c</code> is in the English alphabet or is a digit,
     * false otherwise
     */
    private static boolean isEnglishLetterOrDigit(char c) {
        return 'A' <= c && c <= 'Z' || 'a' <= c && c <= 'z' || '0' <= c && c <= '9';
    }

    @Override
    public void setAlpha(int alpha) {
        textPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        //if (textEnabled) {
        textPaint.setColorFilter(cf);
        //}
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}
