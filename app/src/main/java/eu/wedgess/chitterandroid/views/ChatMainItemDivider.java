package eu.wedgess.chitterandroid.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import eu.wedgess.chitterandroid.R;

/**
 * Provides item decoration (line divider) for RecyclerView Items
 * Idea taken from - https://goo.gl/axQpSv
 * <p>
 * Created by Gareth on 10/11/2016.
 */
public class ChatMainItemDivider extends RecyclerView.ItemDecoration {
    private Drawable mDivider;
    private int mLeftMargin;

    public ChatMainItemDivider(int leftMargin, Context context) {
        this.mDivider = ContextCompat.getDrawable(context, R.drawable.line_divider);
        this.mLeftMargin = leftMargin;
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        // get number of items in the RecyclerView
        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);
            // get child layout params
            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();
            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();
            // draw divider at position - 48dp is IV size - margin is 16dp and item space is 16dp
            mDivider.setBounds(left + mLeftMargin, top, right, bottom);
            mDivider.draw(c);
        }
    }
}
