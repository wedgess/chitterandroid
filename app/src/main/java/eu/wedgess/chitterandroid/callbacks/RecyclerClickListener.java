package eu.wedgess.chitterandroid.callbacks;

/**
 * Callback for when a recycler item has been clicked or long clicked.
 * <p>
 * Created by Gareth on 20/12/2016.
 */
public interface RecyclerClickListener {

    void onItemClicked(int position);

    void onItemLongClicked(int position);
}

