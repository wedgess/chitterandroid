package eu.wedgess.chitterandroid.callbacks;

/**
 * Calback for when the create contact icon is pressed, a dialog is then opened
 * allowing the user to create the contact on the devices contacts.
 * <p>
 * Created by Gareth on 06/02/2017.
 */

public interface CreateContactListener {
    void onCreateContact(int position);
}
