package eu.wedgess.chitterandroid.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.callbacks.RecyclerClickListener;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;
import eu.wedgess.chitterandroid.views.RoundedImageView;

/**
 * Subclass of {@link BaseCursorAdapter}
 * An Adapter for displaying the users chats which they are participating in
 * used within {@link eu.wedgess.chitterandroid.activities.ChatHomeActivity}
 * <p>
 * Created by Gareth on 14/12/2016.
 */

public class ChatHomeAdapter extends BaseCursorAdapter<ChatHomeAdapter.ChatsViewHolder, ChatRoom> {

    public static final String TAG = ChatHomeAdapter.class.getSimpleName();

    // callback for when a list item is clicked/ longClicked
    private final RecyclerClickListener mItemClickListener;
    private final Context mContext;

    /**
     * Constructor needs the cursor, and click listener
     */
    public ChatHomeAdapter(Cursor cursor, RecyclerClickListener itemClickListener, Context context) {
        super(cursor);
        this.mItemClickListener = itemClickListener;
        this.mContext = context;
    }

    /**
     * Returns a contact at click position
     * Used for getting the contacts of a User to User chat, this is wrong for Group chat
     *
     * @param position
     * @return
     */
    public Contact getContactPos(int position) {
        mDataCursor.moveToPosition(position);
        return new Contact(mDataCursor);
    }

    @Override
    public ChatHomeAdapter.ChatsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_chat_main, parent, false);
        return new ChatsViewHolder(itemView, mItemClickListener);
    }

    @Override
    public void onBindViewHolder(ChatsViewHolder holder, int position) {
        mDataCursor.moveToPosition(position);
        holder.setData(mDataCursor, isSelected(position), mContext);

    }

    /**
     * Returns a {@link ChatRoom} object based on position within adapter
     *
     * @param position -  position in adapter of chatroom
     * @return
     */
    @Override
    public ChatRoom getItemAtPosition(int position) {
        mDataCursor.moveToPosition(position);
        return new ChatRoom(mDataCursor);
    }

    /**
     * ViewHolder pattern, static to avoid memory leaks
     */
    static class ChatsViewHolder extends RecyclerView.ViewHolder {
        TextView chatTitleTV, chatPreviewTV, chatTimeTV, chatUnreadTV;
        ImageView selectedIV;
        RoundedImageView chatImgIV;


        ChatsViewHolder(View itemView, final RecyclerClickListener itemClickListener) {
            super(itemView);
            // bind views
            selectedIV = (ImageView) itemView.findViewById(R.id.iv_item_selected);
            chatImgIV = (RoundedImageView) itemView.findViewById(R.id.iv_chat_img);
            chatTitleTV = (TextView) itemView.findViewById(R.id.tv_chat_title);
            chatPreviewTV = (TextView) itemView.findViewById(R.id.tv_chat_preview);
            chatTimeTV = (TextView) itemView.findViewById(R.id.tv_chat_msg_time);
            chatUnreadTV = (TextView) itemView.findViewById(R.id.tv_chat_unread);

            // set click listener
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemClicked(getAdapterPosition());
                }
            });

            // set long click listener
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    itemClickListener.onItemLongClicked(getAdapterPosition());
                    return false;
                }
            });

            // add click listeners to the items rootView
        }

        /**
         * Set values of viewholders views
         *
         * @param mDataCursor -  current cursor
         * @param isSelected  - if item is selected or not
         * @param context     - current context
         */
        void setData(Cursor mDataCursor, boolean isSelected, final Context context) {

            if (isSelected) {
                if (selectedIV.getVisibility() == View.GONE) {
                    UIHelper.playSelectAnimation(selectedIV, 0f, 1f);
                }
                // selectedIV.setVisibility(View.VISIBLE);
                itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.selected_item_bg));
            } else {
                itemView.setBackgroundResource(UIHelper.getSelectedItemBgAttr(context));
                // selectedIV.setVisibility(View.GONE);
                if (selectedIV.getVisibility() == View.VISIBLE) {
                    UIHelper.playSelectAnimation(selectedIV, 1f, 0f);
                }
            }

            // get the needed values from the cursor to display within views etc...
            int chatType = mDataCursor.getInt(mDataCursor.getColumnIndex(DBConstants.COLUMN_CHAT_TYPE));
            String messagePreview = mDataCursor.getString(mDataCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_MSG));
            String messageDate = mDataCursor.getString(mDataCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_CREATED_AT));
            int messageStatus = mDataCursor.getInt(mDataCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_STATUS));
            int messageType = mDataCursor.getInt(mDataCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_TYPE));
            int chatUnreadCount = mDataCursor.getInt(mDataCursor.getColumnIndex(DBConstants.COLUMN_CHAT_UNREAD_COUNT));
            long msgSenderId = mDataCursor.getLong(mDataCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_SENDER_ID));

            String sender = null;
            if (chatType == ChatRoom.USER_TO_USER_CHAT) {
                // set user to user chat views
                final String profileImgUrl = mDataCursor.getString(mDataCursor.getColumnIndex(DBConstants.COLUMN_CONTACTS_IMG_URL));
                String chatTitleUserToUser = mDataCursor.getString(mDataCursor.getColumnIndex(DBConstants.COLUMN_USER_NAME));
                if (!TextUtils.isEmpty(chatTitleUserToUser)) {
                    chatTimeTV.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    chatTitleTV.setText(chatTitleUserToUser);
                    // make sure group icon does not appear on user to user chat
                    chatTitleTV.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    if (!TextUtils.isEmpty(profileImgUrl)) {
                        //Log.i(TAG, "Picasso should be loading img with URL: " + profileImgUrl);
                        final int imgDimens = (int) context.getResources().getDimension(R.dimen.chat_profile_img_size);
                        // try fetch image from cache first
                        Picasso.with(context)
                                .load(profileImgUrl)
                                .resize(imgDimens, imgDimens)
                                .centerCrop()
                                .placeholder(R.drawable.ic_avatar_placeholder)
                                .networkPolicy(NetworkPolicy.OFFLINE)
                                .into(chatImgIV, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        // fetch from server as image is not cached
                                        Picasso.with(context)
                                                .load(profileImgUrl)
                                                .resize(imgDimens, imgDimens)
                                                .centerCrop()
                                                .placeholder(R.drawable.ic_avatar_placeholder)
                                                .into(chatImgIV);
                                    }
                                });
                    } else {
                        chatImgIV.setImageDrawable(new CharacterDrawable(chatTitleUserToUser, context));
                    }
                }
            } else {
                // group chat so get the chat title (not present in user to uer chats)
                String chatTitleGroup = mDataCursor.getString(mDataCursor.getColumnIndex(DBConstants.COLUMN_CHAT_NAME));
                final String chatGroupProfileImgUrl = mDataCursor.getString(mDataCursor.getColumnIndex(DBConstants.COLUMN_CHAT_PROFILE_IMG_URL));
                if (!TextUtils.isEmpty(chatTitleGroup)) {
                    chatTitleTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_group_chat, 0, 0, 0);
                    chatTitleTV.setCompoundDrawablePadding(UIHelper.dpToPx(8, context));
                    chatTitleTV.setText(chatTitleGroup);
                    final int imgDimens = (int) context.getResources().getDimension(R.dimen.chat_profile_img_size);
                    if (chatGroupProfileImgUrl != null && !TextUtils.isEmpty(chatGroupProfileImgUrl)) {
                        // try load chat group image from cache
                        Picasso.with(context)
                                .load(chatGroupProfileImgUrl)
                                .resize(imgDimens, imgDimens)
                                .placeholder(R.drawable.ic_avatar_placeholder)
                                .centerCrop()
                                .networkPolicy(NetworkPolicy.OFFLINE)
                                .into(chatImgIV, new Callback() {
                                    @Override
                                    public void onSuccess() {

                                    }

                                    @Override
                                    public void onError() {
                                        // fetch from server as image is not cached
                                        Picasso.with(context)
                                                .load(chatGroupProfileImgUrl)
                                                .resize(imgDimens, imgDimens)
                                                .placeholder(R.drawable.ic_avatar_placeholder)
                                                .centerCrop()
                                                .into(chatImgIV);
                                    }
                                });
                    } else {
                        // no chat group image set character drawable instead
                        chatImgIV.setImageDrawable(new CharacterDrawable(chatTitleGroup, context));
                    }
                }
            }
            // if the last message sender is the menu_chat_home user then display their message status
            // select the drawable based on status
            if (ChitterModel.getInstance(context).userIsOwner(msgSenderId, context)) {
                int drawableRes;
                switch (messageStatus) {
                    case Message.STATUS_READ_BY_USER:
                        drawableRes = R.drawable.ic_tick_double_blue;
                        break;
                    case Message.STATUS_DELIVERED_TO_USER:
                        drawableRes = R.drawable.ic_tick_double;
                        break;
                    case Message.STATUS_DELIVERED_TO_SERVER:
                        drawableRes = R.drawable.ic_tick_single;
                        break;
                    case Message.STATUS_PENDING:
                    default:
                        drawableRes = R.drawable.ic_tick_schedule;
                        break;
                }
                Drawable drawable = ContextCompat.getDrawable(context, drawableRes);
                chatTimeTV.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
            } else {
                chatTimeTV.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                sender = mDataCursor.getString(mDataCursor.getColumnIndex(DBConstants.COLUMN_CONTACTS_NAME));
            }

            // if group chat and last message is not owners show the contacts name beside message preview
            if (chatType == ChatRoom.GROUP_CHAT) {
                // use a spannable string builder to color the first part of the string (the senders name)
                SpannableStringBuilder builder = new SpannableStringBuilder();
                SpannableString senderNameStr = new SpannableString(sender == null ? context.getString(R.string.my_msg_prefix) : sender);
                SpannableString messagePreviewStr = new SpannableString(messagePreview);
                senderNameStr.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.material_indigo)),
                        0, senderNameStr.length(), 0);
                builder.append(senderNameStr);
                builder.append(": ");
                if (messageType == Message.TYPE_IMG) {
                    chatPreviewTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_msg_image, 0, 0, 0);
                    chatPreviewTV.setCompoundDrawablePadding(UIHelper.dpToPx(8, context));
                    builder.append(context.getString(R.string.image_postfix));
                } else {
                    chatPreviewTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    builder.append(messagePreviewStr);
                }
                chatPreviewTV.setText(builder, TextView.BufferType.SPANNABLE);
            } else {
                if (messageType == Message.TYPE_IMG) {
                    chatPreviewTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_msg_image, 0, 0, 0);
                    chatPreviewTV.setCompoundDrawablePadding(UIHelper.dpToPx(8, context));
                    chatPreviewTV.setText(context.getString(R.string.image_postfix));
                } else {
                    chatPreviewTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    chatPreviewTV.setText(messagePreview);
                }
            }
            //Log.i(TAG, "Msg preview: " + messagePreview);
            // format last message in chat time
            try {
                chatTimeTV.setText(" " + Utils.getFormattedDate(messageDate, false));
            } catch (ParseException e) {
                e.printStackTrace();
                // on exception use unformatted last message time
                chatTimeTV.setText(messageDate);
            }

            // if unread count is 0 then hide unread badge and set chat time to normal color
            if (chatUnreadCount == 0) {
                chatUnreadTV.setVisibility(View.GONE);
                chatTimeTV.setTextColor(ContextCompat.getColor(context, R.color.text_secondary));
            } else {
                // otherwise show unread badge and set chat time to green
                chatUnreadTV.setText(String.valueOf(chatUnreadCount));
                chatTimeTV.setTextColor(ContextCompat.getColor(context, R.color.unread_badge));
                chatUnreadTV.setVisibility(View.VISIBLE);
            }
        }
    }
}
