package eu.wedgess.chitterandroid.adapters;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.activities.ChatRoomActivity;
import eu.wedgess.chitterandroid.activities.ViewImageActivity;
import eu.wedgess.chitterandroid.callbacks.RecyclerClickListener;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.ChatBubbleLayout;

/**
 * Subclass of {@link BaseCursorAdapter}
 * An Adapter for displaying the chat rooms messages which they are participating in
 * used within {@link ChatRoomActivity}.
 * This class contains ViewHolders for two different layouts:
 * {@link OutgoingMessageViewHolder} for message sent by the user
 * {@link IncomingMessageViewHolder} for messages that the user is receiving for others
 * <p>
 * Created by Gareth on 20/12/2016.
 */

public class ChatRoomAdapter extends BaseCursorAdapter<RecyclerView.ViewHolder, Message> {

    public static final String TAG = ChatRoomAdapter.class.getSimpleName();

    // flags for the two different types of viewholders
    private static final int ITEM_TYPE_OUTGOING = 0;
    private static final int ITEM_TYPE_INCOMING = 1;
    private static final int ITEM_TYPE_IMG_OUTGOING = 2;
    private static final int ITEM_TYPE_IMG_INCOMING = 3;

    private Context mContext;
    // recycler click and long click listener
    private final RecyclerClickListener mItemClickListener;
    // used for type of chat - user to user or group chats
    private int mChatType;
    private static float sFontSize;

    /**
     * Constructor needs the list of items.
     */
    public ChatRoomAdapter(Cursor cursor, int chatType, RecyclerClickListener itemClickListener, Context context) {
        super(cursor);
        this.mChatType = chatType;
        this.mItemClickListener = itemClickListener;
        this.mContext = context;
        sFontSize = UIHelper.dpToPx(Integer.parseInt(Utils.getStringPref(
                Constants.PREF_CHAT_FONT_SIZE, "16", context)), context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // if the viewtype is outgoing message inflate outgoing message view
        if (viewType == ITEM_TYPE_OUTGOING) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.msg_outgoing, parent, false);
            return new OutgoingMessageViewHolder(itemView, mItemClickListener);
        } else if (viewType == ITEM_TYPE_INCOMING) {
            // else viewtype is incoming message inflate outgoing message view
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.msg_incoming, parent, false);
            return new IncomingMessageViewHolder(itemView, mItemClickListener);
        } else if (viewType == ITEM_TYPE_IMG_INCOMING) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.msg_img_incoming, parent, false);
            return new IncomingImgMessageViewHolder(itemView, mItemClickListener);
        } else {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.msg_img_outgoing, parent, false);
            return new OutgoingImgMessageViewHolder(itemView, mItemClickListener);
        }
    }

    /**
     * Used to get the type of view, can be one of two (Incoming or Outgoing)
     *
     * @param position - position of item we are checking view type of
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        mDataCursor.moveToPosition(position);
//        long senderId = mDataCursor.getLong(mDataCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_SENDER_ID));
//        String mediaUrl = mDataCursor.getString(mDataCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_IMG_URL));
        Message message = new Message(mDataCursor);
        // if its the owner of the app we use outgoing messaage viewtype otherwise incoming message viewtype
        if (message.isImage()) {
            if (message.getSenderId() == ChitterModel.getInstance(mContext).getUser(mContext).getId()) {
                return ITEM_TYPE_IMG_OUTGOING;
            } else {
                return ITEM_TYPE_IMG_INCOMING;
            }
            //} else if (message.isText()) {
        } else {
            if (message.getSenderId() == ChitterModel.getInstance(mContext).getUser(mContext).getId()) {
                return ITEM_TYPE_OUTGOING;
            } else {
                return ITEM_TYPE_INCOMING;
            }
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // if viewtype is outgoing message set the data values othersie its an incoming
        // message so set its views data
        if (getItemViewType(position) == ITEM_TYPE_OUTGOING) {
            // move cursor to correct position
            mDataCursor.moveToPosition(position);
            ((OutgoingMessageViewHolder) holder).setData(mDataCursor, isSelected(position), mContext);
        } else if (getItemViewType(position) == ITEM_TYPE_IMG_OUTGOING) {
            mDataCursor.moveToPosition(position);
            ((OutgoingImgMessageViewHolder) holder).setData(mDataCursor, isSelected(position), mContext);
        } else if (getItemViewType(position) == ITEM_TYPE_IMG_INCOMING) {
            mDataCursor.moveToPosition(position);
            ((IncomingImgMessageViewHolder) holder).setData(mDataCursor, isSelected(position), mContext);
        } else {
            // move cursor to correct position
            mDataCursor.moveToPosition(position);
            ((IncomingMessageViewHolder) holder).setData(mDataCursor, isSelected(position), mChatType, mContext);
        }
    }

    /**
     * Gets a {@link Message} object at postiton
     *
     * @param position - position of item to get
     * @return - {@link Message}
     */
    @Override
    public Message getItemAtPosition(int position) {
        mDataCursor.moveToPosition(position);
        return new Message(mDataCursor);
    }

    /**
     * Gets a {@link eu.wedgess.chitterandroid.model.Contact} object at postiton
     *
     * @param position - position of item to get
     * @return - {@link Contact}
     */
    public Contact getContactAtPosition(int position) {
        mDataCursor.moveToPosition(position);
        return new Contact(mDataCursor);
    }

    /**
     * ViewHolder for the Ougoing messages (Messages sent by the user)
     */
    private static class OutgoingMessageViewHolder extends RecyclerView.ViewHolder {
        ChatBubbleLayout bubbleLayout;
        ImageView receiptIV;
        TextView messageTV, timestampTV;
        // date format objects used by viewholder
        private DateFormat dateFormat = Utils.getSQLTimestampFormat();
        private DateFormat timeFormat = Utils.getHourMinTimestampFormat();


        OutgoingMessageViewHolder(View itemView, final RecyclerClickListener itemClickListener) {
            super(itemView);
            bubbleLayout = (ChatBubbleLayout) itemView.findViewById(R.id.chat_bubble_layout);
            messageTV = (TextView) itemView.findViewById(R.id.tv_message_txt);
            messageTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, sFontSize);
            timestampTV = (TextView) itemView.findViewById(R.id.tv_message_timestamp);
            receiptIV = (ImageView) itemView.findViewById(R.id.iv_message_receipt);

            // add click listeners to the items rootView
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemClicked(getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    itemClickListener.onItemLongClicked(getAdapterPosition());
                    return false;
                }
            });
        }

        /**
         * Set the values for the views of layout.
         *
         * @param cursor     - current cursor at certain position
         * @param isSelected - whether or not the item is selected
         * @param mContext   - current context
         */
        void setData(Cursor cursor, boolean isSelected, Context mContext) {
            // get the data from the cursor needed to set views values
            String message = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_MSG));
            String dateCreateAt = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_CREATED_AT));
            long msgSender = cursor.getLong(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_SENDER_ID));
            int status = cursor.getInt(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_STATUS));
            //Log.i(TAG, "Date created at raw: " + dateCreateAt);
            //Log.i(TAG, message);

            /**
             * To group chat bubbles together we need to check the previous and next sender. But only if the cursor
             * has a prev and next item.
             */
            long prevSender = -1;
            long nextSender = -1;
            // get next sender id
            if (cursor.getPosition() < cursor.getCount() - 1 && cursor.moveToNext()) {
                nextSender = cursor.getLong(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_SENDER_ID));
                cursor.moveToPrevious();
            }
            // get previous sender id
            if (cursor.getPosition() < cursor.getCount() && cursor.moveToPrevious()) {
                prevSender = cursor.getLong(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_SENDER_ID));
                cursor.moveToNext();
            }

            // bring bubbles closer together
            int marginTopBottom = UIHelper.dpToPx(2, mContext);
            // if the previous sender is the same as this msg sender then change BG so bubbles are grouped
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) bubbleLayout.getLayoutParams();
            if (prevSender != -1 && msgSender == prevSender) {
                bubbleLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.msg_bg_outgoing_part));
                layoutParams.setMargins(0, 0, 0, 0);
            } else {
                // otherwise show starter/single bubble
                bubbleLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.msg_bg_outgoing));
                layoutParams.setMargins(0, marginTopBottom, 0, marginTopBottom);
                if (nextSender == msgSender) {
                    //Log.i(TAG, "Removing margin for next sender: " + message);
                    layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, 0);
                }
            }
            // set bubble layout paramters with new margins
            bubbleLayout.setLayoutParams(layoutParams);

            if (nextSender != msgSender) {
                layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin,
                        layoutParams.rightMargin, marginTopBottom);
                bubbleLayout.setLayoutParams(layoutParams);
            }

            // when items are selected we need to show the selection by highlingting the background to a light blue
            Drawable background = bubbleLayout.getBackground();
            if (isSelected) {
                itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected_msg));
                background.setColorFilter(ContextCompat.getColor(mContext, R.color.selected_msg), PorterDuff.Mode.SRC_IN);
            } else {
                // not selected so remove the blue highlight BG
                itemView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                background.setColorFilter(null);
            }

            // now set values for message, timestamp and receipt status
            messageTV.setText(message);
            Date date;
            try {
                date = dateFormat.parse(dateCreateAt);
            } catch (ParseException e) {
                e.printStackTrace();
                date = new Date(dateCreateAt);
            }
            timestampTV.setText(timeFormat.format(date));
            int drawableRes;
            // set receipt icon based on message status
            switch (status) {
                case Message.STATUS_READ_BY_USER:
                    drawableRes = R.drawable.ic_tick_in_chat_read;
                    break;
                case Message.STATUS_DELIVERED_TO_USER:
                    drawableRes = R.drawable.ic_tick_in_chat_double;
                    break;
                case Message.STATUS_DELIVERED_TO_SERVER:
                    drawableRes = R.drawable.ic_tick_in_chat_single;
                    break;
                default:
                case Message.STATUS_PENDING:
                    drawableRes = R.drawable.ic_tick_in_chat_schedule;
                    break;
            }
            Drawable drawable = ContextCompat.getDrawable(mContext, drawableRes);
            receiptIV.setImageDrawable(drawable);
        }
    }

    /**
     * ViewHolder for the Ougoing messages (Messages sent by the user)
     */
    private static class IncomingMessageViewHolder extends RecyclerView.ViewHolder {
        TextView messageTV, timestampTV, msgAuthor;
        ChatBubbleLayout baseLayout;
        // date format objects used by viewholder
        private DateFormat dateFormat = Utils.getSQLTimestampFormat();
        private DateFormat timeFormat = Utils.getHourMinTimestampFormat();

        IncomingMessageViewHolder(View itemView, final RecyclerClickListener itemClickListener) {
            super(itemView);
            baseLayout = (ChatBubbleLayout) itemView.findViewById(R.id.chat_bubble_layout);
            msgAuthor = (TextView) itemView.findViewById(R.id.tv_sender_name);
            messageTV = (TextView) itemView.findViewById(R.id.tv_message_txt);
            messageTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, sFontSize);
            timestampTV = (TextView) itemView.findViewById(R.id.tv_message_timestamp);

            // set click listeners on base item view
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemClicked(getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    itemClickListener.onItemLongClicked(getAdapterPosition());
                    return false;
                }
            });

            // add click listeners to the items rootView
        }

        /**
         * Set the values for the views of layout.
         *
         * @param cursor     - current cursor at certain position
         * @param isSelected - whether or not the item is selected
         * @param mContext   - current context
         */
        void setData(Cursor cursor, boolean isSelected, int chatType, Context mContext) {
            // get the data from the cursor needed to set views values
            String message = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_MSG));
            String dateCreateAt = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_CREATED_AT));
            String senderName = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_CONTACTS_NAME));
            long senderId = cursor.getLong(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_SENDER_ID));
            // for group chats incoming messages have the senders name displayed, and only for their first message
            // so if they send 2 messages we only disaplay the authors name in first chat bubble
            boolean hideMsgOwner = false;

            long prevSender = -1;
            long nextSender = -1;

            // get next messages sender id
            if (cursor.getPosition() < cursor.getCount() - 1 && cursor.moveToNext()) {
                nextSender = cursor.getLong(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_SENDER_ID));
                // move cursor back to the current pos
                cursor.moveToPrevious();
            }

            // get previous messages sender if
            if (cursor.getPosition() < cursor.getCount() && cursor.moveToPrevious()) {
                prevSender = cursor.getLong(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_SENDER_ID));
                // move cursor back to current position
                cursor.moveToNext();
            }

            // bring bubbles closer together
            int marginTopBottom = UIHelper.dpToPx(2, mContext);
            // if the previous sender is the same as this msg sender then change BG so bubbles are grouped
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) baseLayout.getLayoutParams();
            if (prevSender != -1 && senderId == prevSender) {
                baseLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.msg_bg_incoming_part));
                layoutParams.setMargins(0, 0, 0, 0);
                hideMsgOwner = true;
            } else {
                // otherwise show starter/single bubble
                baseLayout.setBackground(ContextCompat.getDrawable(mContext, R.drawable.msg_bg_incoming));
                layoutParams.setMargins(0, marginTopBottom, 0, marginTopBottom);
                if (nextSender == senderId) {
                    //Log.i(TAG, "Removing margin for next sender: " + message);
                    layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin, layoutParams.rightMargin, 0);
                } else {
                    layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin,
                            layoutParams.rightMargin, marginTopBottom);
                }
            }
            baseLayout.setLayoutParams(layoutParams);

            if (nextSender != senderId) {
                layoutParams.setMargins(layoutParams.leftMargin, layoutParams.topMargin,
                        layoutParams.rightMargin, marginTopBottom);
                baseLayout.setLayoutParams(layoutParams);
            }

            // if selected we use a blue BG color to highlight the selected message item
            Drawable background = baseLayout.getBackground();
            if (isSelected) {
                itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected_msg));
                background.setColorFilter(ContextCompat.getColor(mContext, R.color.selected_msg), PorterDuff.Mode.SRC_IN);
            } else {
                // otheriwse make sure BG is transparent
                itemView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                background.setColorFilter(null);
            }

            // set the views values (message and timestamp)
            messageTV.setText(message);
            Date date;
            try {
                date = dateFormat.parse(dateCreateAt);
            } catch (ParseException e) {
                e.printStackTrace();
                // error parsing date so just create the date object deprecated way
                date = new Date(dateCreateAt);
            }

            // user to user chats don;t have authors name in chat bubble
            if (chatType == ChatRoom.USER_TO_USER_CHAT) {
                msgAuthor.setVisibility(View.GONE);
            } else {
                msgAuthor.setText(senderName);
                msgAuthor.setTextColor(UIHelper.getColorForLetter(
                        msgAuthor.getText().toString(), mContext.getResources(), R.array.chat_members_colors));
                messageTV.setPadding(messageTV.getPaddingLeft(), 0,
                        messageTV.getPaddingRight(), messageTV.getPaddingBottom());

                // hide author if needed
                if (hideMsgOwner) {
                    msgAuthor.setVisibility(View.GONE);
                } else {
                    msgAuthor.setVisibility(View.VISIBLE);
                }
            }
            timestampTV.setText(timeFormat.format(date));
        }
    }


    /**
     * ViewHolder for the Ougoing messages (Messages sent by the user)
     */
    private static class OutgoingImgMessageViewHolder extends RecyclerView.ViewHolder {
        ImageView imgMessage, receiptIV;
        TextView timestampTV;
        ProgressBar imgLoadPb;
        LinearLayout baseLayout;
        // date format objects used by viewholder
        private DateFormat dateFormat = Utils.getSQLTimestampFormat();
        private DateFormat timeFormat = Utils.getHourMinTimestampFormat();


        OutgoingImgMessageViewHolder(View itemView, final RecyclerClickListener itemClickListener) {
            super(itemView);
            baseLayout = (LinearLayout) itemView.findViewById(R.id.base_img_outgoing);
            imgMessage = (ImageView) itemView.findViewById(R.id.iv_img_msg);
            timestampTV = (TextView) itemView.findViewById(R.id.tv_message_timestamp);
            receiptIV = (ImageView) itemView.findViewById(R.id.iv_message_receipt);
            imgLoadPb = (ProgressBar) itemView.findViewById(R.id.pb_loading_img);

            // add click listeners to the items rootView
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemClicked(getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    itemClickListener.onItemLongClicked(getAdapterPosition());
                    return false;
                }
            });
        }

        /**
         * Set the values for the views of layout.
         *
         * @param cursor     - current cursor at certain position
         * @param isSelected - whether or not the item is selected
         * @param mContext   - current context
         */
        void setData(Cursor cursor, boolean isSelected, final Context mContext) {
            // get the data from the cursor needed to set views values
            final String mediaUrl = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_IMG_URL));
            String dateCreateAt = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_CREATED_AT));
            int status = cursor.getInt(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_STATUS));

            // if selected we use a blue BG color to highlight the selected message item
            Drawable background = baseLayout.getBackground();
            if (isSelected) {
                itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected_msg));
                background.setColorFilter(ContextCompat.getColor(mContext, R.color.selected_msg), PorterDuff.Mode.SRC_IN);
            } else {
                // otheriwse make sure BG is transparent
                itemView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                background.setColorFilter(null);
            }
            final int imgWidth = (int) mContext.getResources().getDimension(R.dimen.msg_img_width);
            final int imgHeight = (int) mContext.getResources().getDimension(R.dimen.msg_img_height);

            // now set values for message, timestamp and receipt status
            Picasso.with(mContext)
                    .load(mediaUrl)
                    .placeholder(R.drawable.ic_placeholder_img)
                    .resize(imgWidth, imgHeight)
                    .centerCrop()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(imgMessage, new Callback() {
                        @Override
                        public void onSuccess() {
                            // only set click listener if image is valid
                            imgMessage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startViewImageActivity(mediaUrl, imgMessage, (Activity) mContext);
                                }
                            });
                            imgLoadPb.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(mediaUrl)
                                    .placeholder(R.drawable.ic_placeholder_img)
                                    .resize(imgWidth, imgHeight)
                                    .error(R.drawable.img_placeholder_broken)
                                    .centerCrop()
                                    .into(imgMessage, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            imgMessage.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    startViewImageActivity(mediaUrl, imgMessage, (Activity) mContext);
                                                }
                                            });
                                        }

                                        @Override
                                        public void onError() {

                                        }
                                    });
                            imgLoadPb.setVisibility(View.GONE);
                        }
                    });
            Date date;
            try {
                date = dateFormat.parse(dateCreateAt);
            } catch (ParseException e) {
                e.printStackTrace();
                date = new Date(dateCreateAt);
            }
            timestampTV.setText(timeFormat.format(date));
            int drawableRes;
            // set receipt icon based on message status
            switch (status) {
                case Message.STATUS_READ_BY_USER:
                    drawableRes = R.drawable.ic_tick_in_chat_read;
                    break;
                case Message.STATUS_DELIVERED_TO_USER:
                    drawableRes = R.drawable.ic_tick_in_chat_double;
                    break;
                case Message.STATUS_DELIVERED_TO_SERVER:
                    drawableRes = R.drawable.ic_tick_in_chat_single;
                    break;
                default:
                case Message.STATUS_PENDING:
                    drawableRes = R.drawable.ic_tick_in_chat_schedule;
                    break;
            }
            Drawable drawable = ContextCompat.getDrawable(mContext, drawableRes);
            receiptIV.setImageDrawable(drawable);
        }
    }


    /**
     * ViewHolder for the Ougoing messages (Messages sent by the user)
     */
    private static class IncomingImgMessageViewHolder extends RecyclerView.ViewHolder {
        ImageView imgMessage;
        TextView timestampTV;
        ProgressBar imgLoadPb;
        LinearLayout baseLayout;
        // date format objects used by viewholder
        private DateFormat dateFormat = Utils.getSQLTimestampFormat();
        private DateFormat timeFormat = Utils.getHourMinTimestampFormat();


        IncomingImgMessageViewHolder(View itemView, final RecyclerClickListener itemClickListener) {
            super(itemView);
            baseLayout = (LinearLayout) itemView.findViewById(R.id.base_img_incoming);
            imgMessage = (ImageView) itemView.findViewById(R.id.iv_img_msg);
            timestampTV = (TextView) itemView.findViewById(R.id.tv_message_timestamp);
            imgLoadPb = (ProgressBar) itemView.findViewById(R.id.pb_loading_img);

            // add click listeners to the items rootView
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    itemClickListener.onItemClicked(getAdapterPosition());
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    itemClickListener.onItemLongClicked(getAdapterPosition());
                    return false;
                }
            });
        }

        /**
         * Set the values for the views of layout.
         *
         * @param cursor     - current cursor at certain position
         * @param isSelected - whether or not the item is selected
         * @param mContext   - current context
         */
        void setData(Cursor cursor, boolean isSelected, final Context mContext) {
            // get the data from the cursor needed to set views values
            final String mediaUrl = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_IMG_URL));
            String dateCreateAt = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_CREATED_AT));

            // if selected we use a blue BG color to highlight the selected message item
            Drawable background = baseLayout.getBackground();
            if (isSelected) {
                itemView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.selected_msg));
                background.setColorFilter(ContextCompat.getColor(mContext, R.color.selected_msg), PorterDuff.Mode.SRC_IN);
            } else {
                // otheriwse make sure BG is transparent
                itemView.setBackgroundColor(ContextCompat.getColor(mContext, android.R.color.transparent));
                background.setColorFilter(null);
            }

            final int imgWidth = (int) mContext.getResources().getDimension(R.dimen.msg_img_width);
            final int imgHeight = (int) mContext.getResources().getDimension(R.dimen.msg_img_height);

            // now set values for message, timestamp and receipt status
            Picasso.with(mContext)
                    .load(mediaUrl)
                    .resize(imgWidth, imgHeight)
                    .placeholder(R.drawable.ic_placeholder_img)
                    .centerCrop()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(imgMessage, new Callback() {
                        @Override
                        public void onSuccess() {
                            imgMessage.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startViewImageActivity(mediaUrl, imgMessage, (Activity) mContext);
                                }
                            });
                            imgLoadPb.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            Picasso.with(mContext)
                                    .load(mediaUrl)
                                    .placeholder(R.drawable.ic_placeholder_img)
                                    .resize(imgWidth, imgHeight)
                                    .centerCrop()
                                    .error(R.drawable.img_placeholder_broken)
                                    .into(imgMessage, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            imgMessage.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    startViewImageActivity(mediaUrl, imgMessage, (Activity) mContext);
                                                }
                                            });
                                        }

                                        @Override
                                        public void onError() {

                                        }
                                    });
                            imgLoadPb.setVisibility(View.GONE);
                        }
                    });
            Date date;
            try {
                date = dateFormat.parse(dateCreateAt);
            } catch (ParseException e) {
                e.printStackTrace();
                date = new Date(dateCreateAt);
            }
            timestampTV.setText(timeFormat.format(date));
        }
    }


    public static void startViewImageActivity(String imgUrl, ImageView iv, Activity context) {
        // start ViewImageActivity on Toolbar image click
        Intent intent = new Intent(context, ViewImageActivity.class);
        // set the image url within the intents extras for loading image
        intent.putExtra(ViewImageActivity.IMAGE_URL_EXTRA, imgUrl);

        // set up shared elements between actvities
        final ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                context,
                Pair.create((View) iv, iv.getTransitionName()));
        context.startActivity(intent, options.toBundle());
    }
}
