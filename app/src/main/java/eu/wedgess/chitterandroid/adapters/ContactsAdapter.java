package eu.wedgess.chitterandroid.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.callbacks.CreateContactListener;
import eu.wedgess.chitterandroid.callbacks.RecyclerClickListener;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;
import eu.wedgess.chitterandroid.views.RoundedImageView;

/**
 * Subclass of {@link BaseCursorAdapter}
 * Used for displaying a list of contacts within RecyclerView.
 */
public class ContactsAdapter extends BaseCursorAdapter<ContactsAdapter.ContactsViewHolder, Contact> {

    private Context mContext;
    private final RecyclerClickListener mItemClickListener;
    // callback for when a contact is not in user device contacts they can click a button to add the contact
    // this is the buttons click callback
    private CreateContactListener mCreateContactCallback;

    /**
     * Constructor
     */
    public ContactsAdapter(Cursor dataCursor, RecyclerClickListener recyclerClickListener,
                           CreateContactListener createContactCallback, Context context) {
        super(dataCursor);
        this.mContext = context;
        this.mItemClickListener = recyclerClickListener;
        this.mCreateContactCallback = createContactCallback;
    }

    @Override
    public ContactsAdapter.ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contact, parent, false);
        return new ContactsViewHolder(itemView, mItemClickListener, mCreateContactCallback);
    }

    @Override
    public void onBindViewHolder(ContactsViewHolder holder, int position) {
        // move cursor to correct position
        mDataCursor.moveToPosition(position);
        holder.setData(mDataCursor, isSelected(position), mContext);
    }

    /**
     * Gets the {@link Contact} at a position in the adapter
     *
     * @param position - position of contact to get
     * @return - {@link Contact}
     */
    @Override
    public Contact getItemAtPosition(int position) {
        mDataCursor.moveToPosition(position);
        return new Contact(mDataCursor);
    }

    // ViewHolder for views, static as to hold no references to outer class
    static class ContactsViewHolder extends RecyclerView.ViewHolder {
        TextView contactNameTV, contactLastSeenTV;
        RoundedImageView contactImgIV;
        ImageView selectedIV;
        ImageButton contactAddIB;
        CreateContactListener createContactCallback;

        // constructor takes the recyclerClickListener and createContactCallback
        // as this is where they are used
        ContactsViewHolder(View itemView, final RecyclerClickListener mItemClickListener,
                           final CreateContactListener createContactCallback) {
            super(itemView);
            contactImgIV = (RoundedImageView) itemView.findViewById(R.id.iv_contact_img);
            selectedIV = (ImageView) itemView.findViewById(R.id.iv_item_selected);
            contactNameTV = (TextView) itemView.findViewById(R.id.tv_contact_name);
            contactLastSeenTV = (TextView) itemView.findViewById(R.id.tv_contact_number);
            contactAddIB = (ImageButton) itemView.findViewById(R.id.ib_add_contact);
            this.createContactCallback = createContactCallback;

            // set recyler click listeners
            if (mItemClickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mItemClickListener.onItemClicked(getAdapterPosition());
                    }
                });

                itemView.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        mItemClickListener.onItemLongClicked(getAdapterPosition());
                        return false;
                    }
                });
            }
        }

        /**
         * Set the data for the item at position
         *
         * @param cursor     - cursor at position
         * @param isSelected - whether item is selected or not
         * @param context    - current context
         */
        void setData(Cursor cursor, boolean isSelected, final Context context) {
            // get values needed from the cursor
            long id = cursor.getLong(cursor.getColumnIndex(DBConstants.COLUMN_CONTACTS_USER_ID));
            String name = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_CONTACTS_NAME));
            String profileImgUrl = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_CONTACTS_IMG_URL));
            String lastSeen = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_CONTACTS_LAST_SEEN));
            boolean contactExistOnDevice = (cursor.getInt(cursor.getColumnIndex(DBConstants.COLUMN_CONTACTS_EXIST_ON_DEVICE)) == 1);
            // if the contact is this user then set the values manually
            if (id == 0) {
                Contact contact = ChitterModel.getInstance(context).getUser(context);
                name = contact.getName();
                profileImgUrl = contact.getImgUrl();
                lastSeen = contact.getLastSeen();
                // color the me part of the string
                SpannableStringBuilder builder = new SpannableStringBuilder();
                SpannableString nameStr = new SpannableString(name);
                SpannableString meString = new SpannableString(context.getString(R.string.current_user_me_postfix));
                meString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.material_indigo)),
                        0, meString.length(), 0);
                builder.append(nameStr);
                builder.append(" ");
                builder.append(meString);
                contactNameTV.setText(builder, TextView.BufferType.SPANNABLE);
            } else {
                contactNameTV.setText(name);
            }

            // if the contact is selected play the select anaimtion
            if (isSelected) {
                if (selectedIV.getVisibility() == View.GONE) {
                    UIHelper.playSelectAnimation(selectedIV, 0f, 1f);
                }
                // selectedIV.setVisibility(View.VISIBLE);
                itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.selected_item_bg));
            } else {
                // unselected set selectItemBG
                itemView.setBackgroundResource(UIHelper.getSelectedItemBgAttr(context));
                // selectedIV.setVisibility(View.GONE);
                // play unselected animtion
                if (selectedIV.getVisibility() == View.VISIBLE) {
                    UIHelper.playSelectAnimation(selectedIV, 1f, 0f);
                }
            }

            // if profile image is empty set character drawable
            if (TextUtils.isEmpty(profileImgUrl)) {
                contactImgIV.setImageDrawable(new CharacterDrawable(name, context));
            } else {
                // otherwise display image
                final int imgDimens = (int) context.getResources().getDimension(R.dimen.chat_profile_img_size);
                final String finalProfileImgUrl = profileImgUrl;
                // try load image from cache first
                Picasso.with(context)
                        .load(profileImgUrl)
                        .resize(imgDimens, imgDimens)
                        .placeholder(R.drawable.ic_avatar_placeholder)
                        .centerCrop()
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(contactImgIV, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                // fetch from server as image is not cached
                                Picasso.with(context)
                                        .load(finalProfileImgUrl)
                                        .resize(imgDimens, imgDimens)
                                        .placeholder(R.drawable.ic_avatar_placeholder)
                                        .centerCrop()
                                        .into(contactImgIV);
                            }
                        });
            }

            // set contacts last seen
            if (lastSeen.equals(Constants.STATUS_ONLINE)) {
                contactLastSeenTV.setText(lastSeen);
                contactLastSeenTV.setTextColor(ContextCompat.getColor(context, R.color.material_green));
            } else {
                try {
                    contactLastSeenTV.setText(Utils.formatLastSeenDate(lastSeen));
                } catch (ParseException e) {
                    e.printStackTrace();
                    contactLastSeenTV.setText(lastSeen);
                } finally {
                    contactLastSeenTV.setTextColor(ContextCompat.getColor(context, R.color.text_secondary));
                }
            }

            // show add contact button if user does not exist on device and ID is not 0
            // if id is not the current user, then show the add to contacts button
            if (!contactExistOnDevice && !ChitterModel.getInstance(context).userIsOwner(id, context) && id != 0) {
                contactAddIB.setVisibility(View.VISIBLE);
                contactAddIB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (createContactCallback != null) {
                            createContactCallback.onCreateContact(getAdapterPosition());
                        }
                    }
                });
            } else {
                contactAddIB.setVisibility(View.GONE);
            }
        }
    }
}
