package eu.wedgess.chitterandroid.adapters;

import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * This class extends the selectable adapter. This class is used for adapters that items can be multiselected
 * and they use cursor loaders.
 * <p>
 * It is used by the following adapter classes.
 * <p>
 * {@link ChatHomeAdapter}
 * {@link ChatRoomAdapter}
 * {@link ContactsAdapter}
 * <p>
 * <p>
 * The basic implmentation of swapping cursors was taken from the below SO answer:
 * http://stackoverflow.com/questions/32992239/using-a-recyclerview-with-data-from-loadermanager
 * Created by Gareth on 30/01/2017.
 */

public abstract class BaseCursorAdapter<VH extends RecyclerView.ViewHolder, T>
        extends SelectableAdapter<VH> {

    // the cursor which is set in subclasses constructor by calling super(this)
    Cursor mDataCursor;

    // subclass must implement their own onCreateViewHolder - using generics to say
    // its a type of ViewHolder
    @Override
    public abstract VH onCreateViewHolder(ViewGroup parent, int viewType);

    // subclass must implement their own onBindViewHolder - using generics to say i
    // ts a type of ViewHolder
    @Override
    public abstract void onBindViewHolder(VH holder, final int position);

    // subclasses can implement their own getItemAtPosition
    public abstract T getItemAtPosition(int position);

    BaseCursorAdapter(Cursor dataCursor) {
        this.mDataCursor = dataCursor;
    }

    /**
     * Swap out old cursor and set new cursor
     * @param cursor -  new cursor
     * @return - old cursor
     */
    public Cursor swapCursor(Cursor cursor) {
        if (mDataCursor == cursor) {
            return null;
        }
        Cursor oldCursor = mDataCursor;
        this.mDataCursor = cursor;
        if (cursor != null) {
            this.notifyDataSetChanged();
        }
        return oldCursor;
    }

    @Override
    public int getItemCount() {
        return (mDataCursor == null) ? 0 : mDataCursor.getCount();
    }

}
