package eu.wedgess.chitterandroid.adapters;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.IncomingMessageHelper;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.model.UsersChats;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.Utils;

/**
 * This is a SyncAdapter which will sync the users chats and messages from the remote DB to the local DB
 * It runs in the BG thread, and is called from {@link eu.wedgess.chitterandroid.utils.NetworkChangeReceiver}
 * when the device is running Android OS Marshmallow and below.
 * <p>
 * For Nougat devices and above we use {@link eu.wedgess.chitterandroid.utils.NetworkChangeService} as
 * Nougat no longer allows us to register certain receivers in the AndroidManifest so the
 * {@link eu.wedgess.chitterandroid.utils.NetworkChangeReceiver} does not get called for these devices.
 * The workaround is to register the receivers within the service and have it running in the BG to listen
 * to network changes.
 * <p>
 * The chats need to be synced first and then the messages, as the chat id is a foreign key within messages.
 * <p>
 * Created by Gareth on 19/01/2017.
 */

public class SyncAdapter extends AbstractThreadedSyncAdapter {

    public static final String TAG = SyncAdapter.class.getSimpleName();

    private final List<UsersChats> mServerUsersChats = new ArrayList<>();
    private final List<UsersChats> mDeviceUsersChats = new ArrayList<>();
    private final List<Message> mServerMessagesSinceLastReceived = new ArrayList<>();

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
    }

    @Override
    public void onPerformSync(Account account, Bundle bundle, String s,
                              ContentProviderClient contentProviderClient, SyncResult syncResult) {

        // get the user json string from prefs
        User user = Utils.getUserFromPrefs(getContext());
        if (user == null) {
            // error cancel sync
            Log.e(TAG, "Sync error: user from preferences cannot be parsed or does not exist");
        } else {
            // get device messages for user
            getMessagesSinceLast(user.getId());
        }
    }

    private void getMessagesSinceLast(final long uid) {
        String uidStr = String.valueOf(uid);
        // get most recent message date, we will sync messages since this date
        Cursor lastMessageDateCursor = getContext().getContentResolver().query(
                ChitterContentProvider.CONTENT_URI_MESSAGES_LAST_RECEIVED,
                null, null, new String[]{uidStr, uidStr}, null);

        if (lastMessageDateCursor != null) {
            // if no messages are in local DB then sync messages from now
            String lastReceived = Constants.SYNC_FROM_NOW;
            // oif messages are present locally
            if (lastMessageDateCursor.moveToFirst()) {
                // get the last received messages date
                lastReceived = lastMessageDateCursor.getString(lastMessageDateCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_CREATED_AT));
                //Log.i(TAG, lastMessageDateCursor.getString(lastMessageDateCursor.getColumnIndex(DBConstants.COLUMN_MESSAGE_CREATED_AT)));
            }
            lastMessageDateCursor.close();
            // get the messages since last received from the server
            APIRequestUtility.getMessagesSinceLastMessageRecieved(uid, lastReceived, getContext(), new APIRequestUtility.CommonListServerCallback() {
                @Override
                public void onComplete(List items) {
                    // add all messages from server since last local received message
                    mServerMessagesSinceLastReceived.addAll(items);
                    // get users chats from server
                    getUsersChats(uid);
                }
            });
        }
    }


    private void getUsersChats(long uid) {
        String uidStr = String.valueOf(uid);
        Cursor usersChatsCursor = getContext().getContentResolver().query(
                ChitterContentProvider.CONTENT_URI_USERS_CHATS, null, null, new String[]{uidStr, uidStr}, null);
        if (usersChatsCursor != null) {
            // add each of the user chats to the device list
            while (usersChatsCursor.moveToNext()) {
                mDeviceUsersChats.add(new UsersChats(usersChatsCursor));
            }
            usersChatsCursor.close();
        }

        // get servers users_chats
        APIRequestUtility.getUsersChats(uid, getContext(), new APIRequestUtility.CommonListServerCallback() {
            @Override
            public void onComplete(List items) {
                // add all the user chats items to servers user chats list
                mServerUsersChats.addAll(items);
                // sync local and remote user chats
                syncUsersChats();
            }
        });
    }

    /**
     * Sync server chats to the devices local DB
     */
    private synchronized void syncUsersChats() {
        Log.i(TAG, "In syncUserChats()");
        // server and device have same amount of columns just check there all equal
        if (mServerUsersChats.size() == mDeviceUsersChats.size()) {
            for (int i = 0; i < mServerUsersChats.size(); i++) {
                UsersChats server = mServerUsersChats.get(i);
                UsersChats device = mServerUsersChats.get(i);
                Log.i(TAG, "server item: " + server.toString());
                Log.i(TAG, "device item: " + device.toString());
                Log.i(TAG, "Items are equal: " + device.equals(server));
            }
        } else {
            // list size differs need to add new onces or remove
            if (mServerUsersChats.size() > mDeviceUsersChats.size()) {
                Log.i(TAG, "server item size before: " + mServerUsersChats.size());
                mServerUsersChats.removeAll(mDeviceUsersChats);
                Log.i(TAG, "server item size afer removeAll: " + mServerUsersChats.size());
                for (UsersChats usersChat : mServerUsersChats) {
                    Log.i(TAG, "Device is missing chat with ID: " + usersChat.getCid());
                    Cursor cursor = getContext().getContentResolver().query(ChitterContentProvider.CONTENT_URI_CHAT_ITEM, null,
                            null, new String[]{usersChat.getCid()}, null);
                    // make sure chat does not already exist, just to be sure
                    if (cursor != null && cursor.moveToFirst()) {
                        // create chat locally, by getting it from remote using Chat ID
                        APIRequestUtility.createLocalChatFromRemote(usersChat.getCid(), getContext(), null);
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                }
            } else if (mDeviceUsersChats.size() > mServerUsersChats.size()) {
                mDeviceUsersChats.removeAll(mServerUsersChats);
                // we can create the local chat on the remote (Need to see if needed)
                for (UsersChats usersChat : mDeviceUsersChats) {
                    Log.i(TAG, "Server is missing chat with ID: " + usersChat.getCid());
                }
            }
            // clear list device chats list
            if (!mDeviceUsersChats.isEmpty()) {
                mDeviceUsersChats.clear();
            }
            // clear list server chats
            if (!mServerUsersChats.isEmpty()) {
                mServerUsersChats.clear();
            }
        }

        // done syncing chats, now sync messages
        syncMessages();
    }

    // TODO: Sync contacts from server
//    private void syncContacts() {
//        Cursor cursor = getContext().getContentResolver().query(ChitterContentProvider.CONTENT_URI_CONTACTS, null, null, null, null);
//        List<Contact> localContacts = new ArrayList<>();
//        if (cursor != null) {
//            while (cursor.moveToNext()) {
//                localContacts.add(new Contact(cursor));
//            }
//            cursor.close();
//        }
//
//        if (!localContacts.isEmpty()) {
//
//        }
//    }

    /**
     * Sync messages form the servers DB to local DB since the last received local DBs message.
     * This date is the last time the user was only as if they were offline they would have received
     * no messages.
     */
    private void syncMessages() {
        if (!mServerMessagesSinceLastReceived.isEmpty()) {
            Log.i(TAG, mServerMessagesSinceLastReceived.size() + " Messages to SYNC");
            String notifTitle = null;
            for (Message message : mServerMessagesSinceLastReceived) {
                // need to get the chat room as we need it for the notification title
                Cursor cursor = getContext().getContentResolver().query(ChitterContentProvider.CONTENT_URI_CHAT_ITEM, null,
                        null, new String[]{message.getCid()}, null);
                if (cursor != null && cursor.moveToFirst()) {
                    ChatRoom chatRoom = new ChatRoom(cursor);
                    if (chatRoom.getChatType() == ChatRoom.USER_TO_USER_CHAT) {
                        notifTitle = String.valueOf(message.getSenderId());
                    } else {
                        notifTitle = chatRoom.getTitle();
                    }
                }
                // close cursor as were done with it
                if (cursor != null) {
                    cursor.close();
                }

                Cursor messageExistCursor = getContext().getContentResolver().query(ChitterContentProvider.CONTENT_URI_MESSAGE_ITEM,
                        null, null, new String[]{message.getId()}, null);
                boolean messageExist = false;
                if (messageExistCursor != null) {
                    // if there is an item then message exists
                    if (messageExistCursor.moveToFirst()) {
                        messageExist = true;
                    }
                    // message doesn't locally
                    Log.i(TAG, "Cursor size: " + messageExistCursor.getCount());
                    Log.i(TAG, "Cursor moveToFirst: " + messageExistCursor.moveToFirst());
                    messageExistCursor.close();
                }
                Log.i(TAG, "Inserting message: " + !messageExist);
                if (!messageExist) {
                    // inserts the item in local DB and shows notification if needed
                    IncomingMessageHelper.handleNewMessage(message, notifTitle, getContext());
                }
            }
            // clear list when done
            if (!mServerMessagesSinceLastReceived.isEmpty()) {
                mServerMessagesSinceLastReceived.clear();
            }
        } else {
            Log.i(TAG, "Message list is empty");
        }
    }
}