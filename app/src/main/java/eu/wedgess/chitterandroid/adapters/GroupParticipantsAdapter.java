package eu.wedgess.chitterandroid.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.ArrayList;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;

/**
 * This adapter does not use a cursor it uses a list as the items have not yet been added to the DB.
 * It is used from within the {@link eu.wedgess.chitterandroid.activities.CreateGroupChatActivity}
 */
public class GroupParticipantsAdapter extends RecyclerView.Adapter<GroupParticipantsAdapter.ContactsViewHolder> {

    private Context mContext;
    private ArrayList<Contact> participants;

    /**
     * Constructor
     */
    public GroupParticipantsAdapter(ArrayList<Contact> participants, Context context) {
        this.participants = participants;
        this.mContext = context;
    }

    @Override
    public GroupParticipantsAdapter.ContactsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contact, parent, false);
        return new ContactsViewHolder(itemView);
    }

    private Contact getItemAtPos(int pos) {
        return participants.get(pos);
    }

    @Override
    public void onBindViewHolder(ContactsViewHolder holder, int position) {
        holder.setData(getItemAtPos(position), mContext);
    }

    @Override
    public int getItemCount() {
        return (participants == null) ? 0 : participants.size();
    }

    // Viewholder for items views
    static class ContactsViewHolder extends RecyclerView.ViewHolder {
        TextView contactNameTV, contactNumberTV;
        ImageView contactImgIV;

        ContactsViewHolder(View itemView) {
            super(itemView);
            contactImgIV = (ImageView) itemView.findViewById(R.id.iv_contact_img);
            contactNameTV = (TextView) itemView.findViewById(R.id.tv_contact_name);
            contactNumberTV = (TextView) itemView.findViewById(R.id.tv_contact_number);
        }

        /**
         * Set the views data based on the contact passed in
         *
         * @param contact - contact at position
         * @param context - cureent context
         */
        void setData(final Contact contact, final Context context) {
            contactNameTV.setText(contact.getName());

            // if img url is empty use character drawable
            if (TextUtils.isEmpty(contact.getImgUrl())) {
                contactImgIV.setImageDrawable(new CharacterDrawable(contact.getName(), context));
            } else {
                // img url is not empty load image from URL
                final int imgDimens = (int) context.getResources().getDimension(R.dimen.chat_profile_img_size);
                // try load image from cache first
                Picasso.with(context)
                        .load(contact.getImgUrl())
                        .resize(imgDimens, imgDimens)
                        .placeholder(R.drawable.ic_avatar_placeholder)
                        .centerCrop()
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(contactImgIV, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                // fetch from server as image is not cached
                                Picasso.with(context)
                                        .load(contact.getImgUrl())
                                        .resize(imgDimens, imgDimens)
                                        .placeholder(R.drawable.ic_avatar_placeholder)
                                        .centerCrop()
                                        .into(contactImgIV);
                            }
                        });
            }

            // if contacts last seen is online don;t parse it as we would get an exception
            // jsut set the last seen as online
            if (contact.getLastSeen().equals(Constants.STATUS_ONLINE)) {
                contactNumberTV.setText(contact.getLastSeen());
                contactNumberTV.setTextColor(ContextCompat.getColor(context, R.color.material_green));
            } else {
                try {
                    contactNumberTV.setText(Utils.formatLastSeenDate(contact.getLastSeen()));
                } catch (ParseException e) {
                    e.printStackTrace();
                    // exception just display unformatted last seen
                    contactNumberTV.setText(contact.getLastSeen());
                }
            }
        }
    }
}
