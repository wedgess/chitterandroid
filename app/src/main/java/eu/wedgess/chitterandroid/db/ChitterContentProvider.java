package eu.wedgess.chitterandroid.db;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;

/**
 * The reason for using a Content Provider is because due to having to use the sync adapter framework
 * we need to have a ContentProvider, if the application doesn't require a content provider then we
 * can create a Stub ContentProvider. Rather than creating a stub content provider I have decided to
 * use it for the menu_chat_home DB operations.
 * https://developer.android.com/training/sync-adapters/creating-stub-provider.html
 * <p>
 * What also made me choose a Content Provider is this answer.
 * http://stackoverflow.com/a/19262624/3748532
 * Created by Gareth on 10/01/2017.
 * <p>
 * Used: As an example: https://goo.gl/eRQ61c
 * <p>
 * Look at:
 * <p>
 * --- Sync Adapter
 * http://stackoverflow.com/a/11299639/3748532
 * https://developer.android.com/training/sync-adapters/index.html
 * http://chariotsolutions.com/blog/post/android-data-sync/
 * https://software.intel.com/en-us/android/articles/handling-offline-capability-and-data-sync-in-an-android-app-part-2
 */

public class ChitterContentProvider extends ContentProvider {

    public static final String TAG = ChitterContentProvider.class.getSimpleName();

    // All URIs share these parts
    public static final String AUTHORITY = "eu.wedgess.chitterandroid.db.ChitterContentProvider";
    private static final String SCHEME = "content://";

    private static final String PATH_CONTACTS = "contacts";
    private static final String PATH_CHATS = "chats";
    private static final String PATH_CHAT_MSG = "chats_msg_join";
    private static final String PATH_CHAT_MSGS = "chats_msgs";
    private static final String PATH_MESSAGES = "messages";
    private static final String PATH_MESSAGES_LAST_RECEIVED = "messages_last_received";
    private static final String PATH_USERS_CHATS = "users_chats";

    private static final int CODE_CONTACTS = 1;
    private static final int CODE_CONTACTS_BY_ID = 2;
    private static final int CODE_CHATS = 5;
    private static final int CODE_CHATS_MSG_JOIN = 6;
    private static final int CODE_CHAT_BY_ID = 7;
    private static final int CODE_MESSAGES = 8;
    private static final int CODE_MESSAGE_BY_ID = 9;
    private static final int CODE_CHATS_MSGS = 10;
    private static final int CODE_CHATS_MSG = 11;
    private static final int CODE_USERS_CHATS = 12;
    private static final int CODE_USERS_CHATS_BY_ID = 13;
    private static final int CODE_USERS_CHATS_EXIST = 14;
    private static final int CODE_USERS_CHATS_MEMBERS = 15;
    private static final int CODE_MESSAGES_LAST_RECEIVED = 16;
    private static final int CODE_CHATS_MSGS_STATUS = 17;

    private static final String CONTENT_TYPE_CONTACTS = "eu.wedgess.chitterandroid.contacts_items";
    private static final String CONTENT_TYPE_CONTACT_ITEM = "eu.wedgess.chitterandroid.contacts_item";
    private static final String CONTENT_TYPE_CHAT_MSG_JOIN = "eu.wedgess.chitterandroid.chat_items_msg_join";
    private static final String CONTENT_TYPE_CHATS = "eu.wedgess.chitterandroid.chat_items";
    private static final String CONTENT_TYPE_CHAT_ITEM = "eu.wedgess.chitterandroid.chat_item";
    private static final String CONTENT_TYPE_MESSAGES = "eu.wedgess.chitterandroid.message_items";
    private static final String CONTENT_TYPE_MESSAGE_ITEM = "eu.wedgess.chitterandroid.message_item";
    private static final String CONTENT_TYPE_MESSAGE_ITEM_RECEIVED = "eu.wedgess.chitterandroid.message_item.received";
    private static final String CONTENT_TYPE_CHATS_MESSAGE = "eu.wedgess.chitterandroid.chats_message";
    private static final String CONTENT_TYPE_CHATS_MESSAGES = "eu.wedgess.chitterandroid.chats_messages";
    private static final String CONTENT_TYPE_CHATS_MESSAGES_STATUS = "eu.wedgess.chitterandroid.chats_messages_status";
    private static final String CONTENT_TYPE_USERS_CHATS = "eu.wedgess.chitterandroid.users_chats_items";
    private static final String CONTENT_TYPE_USERS_CHATS_ITEM = "eu.wedgess.chitterandroid.users_chats_item";
    private static final String CONTENT_TYPE_USERS_CHATS_EXIST = "eu.wedgess.chitterandroid.users_chats_exist";
    private static final String CONTENT_TYPE_USERS_CHATS_MEMBERS = "eu.wedgess.chitterandroid.users_chats_members";

    public static final Uri CONTENT_URI = Uri.parse(SCHEME + AUTHORITY);
    public static final Uri CONTENT_URI_CONTACTS = Uri.withAppendedPath(CONTENT_URI, "contacts");
    public static final Uri CONTENT_URI_CONTACT_ITEM = Uri.withAppendedPath(CONTENT_URI, "contacts_item");
    public static final Uri CONTENT_URI_CHATS = Uri.withAppendedPath(CONTENT_URI, "chats");
    public static final Uri CONTENT_URI_CHAT_MSG_JOIN = Uri.withAppendedPath(CONTENT_URI, "chats_msg_join");
    public static final Uri CONTENT_URI_CHAT_ITEM = Uri.withAppendedPath(CONTENT_URI, "chats_item");
    public static final Uri CONTENT_URI_MESSAGES = Uri.withAppendedPath(CONTENT_URI, "messages");
    public static final Uri CONTENT_URI_MESSAGES_LAST_RECEIVED = Uri.withAppendedPath(CONTENT_URI, PATH_MESSAGES_LAST_RECEIVED);
    public static final Uri CONTENT_URI_MESSAGE_ITEM = Uri.withAppendedPath(CONTENT_URI, "messages_item");
    public static final Uri CONTENT_URI_CHATS_MESSAGES_STATUS = Uri.withAppendedPath(CONTENT_URI, "chats_msgs_status");
    public static final Uri CONTENT_URI_CHATS_MESSAGES = Uri.withAppendedPath(CONTENT_URI, "chats_msgs/");
    public static final Uri CONTENT_URI_USERS_CHATS = Uri.withAppendedPath(CONTENT_URI, "users_chats");
    public static final Uri CONTENT_URI_USERS_CHATS_EXIST = Uri.withAppendedPath(CONTENT_URI, "users_chats_exist");
    public static final Uri CONTENT_URI_USERS_CHATS_MEMBERS = Uri.withAppendedPath(CONTENT_URI, "users_chats_members/");

    private static UriMatcher uriMatcher;
    private final ThreadLocal<Boolean> mIsInBatchMode = new ThreadLocal<Boolean>();

    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, PATH_CONTACTS, CODE_CONTACTS);
        uriMatcher.addURI(AUTHORITY, PATH_CONTACTS + "_item", CODE_CONTACTS_BY_ID);
        uriMatcher.addURI(AUTHORITY, PATH_CHATS, CODE_CHATS);
        uriMatcher.addURI(AUTHORITY, PATH_CHATS + "_item", CODE_CHAT_BY_ID);
        uriMatcher.addURI(AUTHORITY, PATH_CHAT_MSG, CODE_CHATS_MSG_JOIN);
        uriMatcher.addURI(AUTHORITY, PATH_MESSAGES, CODE_MESSAGES);
        uriMatcher.addURI(AUTHORITY, PATH_MESSAGES + "_item", CODE_MESSAGE_BY_ID);
        uriMatcher.addURI(AUTHORITY, PATH_CHAT_MSGS, CODE_CHATS_MSG);
        uriMatcher.addURI(AUTHORITY, PATH_CHAT_MSGS+ "_status", CODE_CHATS_MSGS_STATUS);
        uriMatcher.addURI(AUTHORITY, PATH_CHAT_MSGS + "/*", CODE_CHATS_MSGS);
        uriMatcher.addURI(AUTHORITY, PATH_USERS_CHATS, CODE_USERS_CHATS);
        uriMatcher.addURI(AUTHORITY, PATH_USERS_CHATS + "/*", CODE_USERS_CHATS_BY_ID);
        uriMatcher.addURI(AUTHORITY, "users_chats_exist", CODE_USERS_CHATS_EXIST);
        uriMatcher.addURI(AUTHORITY, "users_chats_members" + "/*", CODE_USERS_CHATS_MEMBERS);
        uriMatcher.addURI(AUTHORITY, PATH_MESSAGES_LAST_RECEIVED, CODE_MESSAGES_LAST_RECEIVED);
    }

    @Override
    public boolean onCreate() {
        return true;
    }

    /**
     * QUERY TO GET MOST RECENT MESSAGE - Use this timestamp to sync from sync date to this date
     * MIGHT NOT EVEN NEED THIS LAST SYNC DATE SHOULD BE ENOUGH
     * <p>
     * SELECT * FROM messages WHERE m_cid IN (SELECT cid FROM users_chats WHERE cid IN (SELECT cid
     * from users_chats WHERE uid = 3) AND users_chats.uid <> 3 GROUP BY users_chats.cid) ORDER BY m_created_at DESC LIMIT 1
     */

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {

        Cursor result = null;

        int uriType = uriMatcher.match(uri);
        switch (uriType) {
            case CODE_CONTACTS:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .query(DBConstants.TABLE_CONTACTS, DBConstants.ALL_CONTACT_FIELDS, null,
                                null, null, null, DBConstants.SORT_CONTACT_NAME, null);
                break;
            case CODE_CONTACTS_BY_ID:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .query(DBConstants.TABLE_CONTACTS, DBConstants.ALL_CONTACT_FIELDS,
                                DBConstants.COLUMN_CONTACTS_USER_ID + " IS ?",
                                selectionArgs, null, null,
                                null, null);
                break;
            case CODE_CHATS:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .query(DBConstants.TABLE_CHATS, DBConstants.ALL_CHAT_FIELDS, null, null, null,
                                null, null, null);
                break;
            case CODE_CHAT_BY_ID:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .query(DBConstants.TABLE_CHATS,
                                DBConstants.ALL_CHAT_FIELDS,
                                DBConstants.COLUMN_CHAT_ID + " IS ?",
                                selectionArgs, null, null,
                                null, null);
                break;
            case CODE_MESSAGES:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        // 4 selection args, last sync, last online date and uid(x2)
                        .rawQuery("SELECT * FROM messages where m_created_at < ? AND messages.m_status <> 0 AND messages.m_cid IN (SELECT cid FROM users_chats WHERE cid IN (SELECT cid from users_chats WHERE uid = ?) AND users_chats.uid <> ? GROUP BY users_chats.cid)", selectionArgs);
                break;
            case CODE_MESSAGE_BY_ID:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .query(DBConstants.TABLE_MESSAGES, DBConstants.ALL_MESSAGE_FIELDS,
                                DBConstants.COLUMN_MESSAGE_ID + " IS ?",
                                selectionArgs, null, null,
                                null, null);
                break;
            case CODE_CHATS_MSG_JOIN:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        // gets the chat row, most recent message row, and the sender of most recent message
                        // we need an (msg.m_sender_id = contacts.u_id) or (users_chats.uid = contacts.u_id)
                        // as if the message is the user they aren;t stored in contacts. So if its the user
                        // then match users_chats.uid otherwise get sender id from contacts.
                        // if we just use msg.m_sender_id then any chats which this user has sent last will not
                        // show up on menu_chat_home chat screen.
                        .rawQuery("select * from chats left join (select * from messages where messages.m_deleted = 0 group by m_cid order by m_created_at desc) msg on chats.c_id = msg.m_cid left join users_chats on chats.c_id = users_chats.cid left join contacts on (msg.m_sender_id = contacts.u_id) or (users_chats.uid = contacts.u_id) where contacts.u_name <> \"\" AND msg.m_id <> \"\" group by chats.c_id ORDER BY msg.m_created_at DESC", null);
                break;
            case CODE_CHATS_MSGS:
                final String m_cid = uri.getLastPathSegment();
                selectionArgs = new String[]{m_cid};

                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .rawQuery("select * from messages left join contacts on m_sender_id = contacts.u_id where m_cid = ? and m_deleted = 0 ORDER BY m_created_at ASC", selectionArgs);
                break;
            case CODE_CHATS_MSGS_STATUS:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .rawQuery("select * from messages where m_status = 1 OR m_status = 2 AND m_sender_id <> ?",
                                selectionArgs);
                break;
            case CODE_USERS_CHATS:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .rawQuery("SELECT * FROM users_chats WHERE cid IN (SELECT cid from users_chats WHERE uid = ?) AND uid <> ?", selectionArgs);
                break;
            case CODE_USERS_CHATS_BY_ID:
                final String u_cid = uri.getLastPathSegment();
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .rawQuery("SELECT * FROM users_chats LEFT JOIN contacts ON users_chats.uid = contacts.u_id WHERE cid = ?", new String[]{u_cid});
                break;
            case CODE_USERS_CHATS_EXIST:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .rawQuery("SELECT uc.cid, count(uid) as members_count FROM users_chats uc WHERE uc.cid IN (SELECT uc1.cid FROM users_chats uc1 LEFT JOIN users_chats uc2 ON uc1.cid = uc2.cid WHERE uc1.uid = ? AND uc2.uid = ?) GROUP BY uc.cid", selectionArgs);
                //.query(DBConstants.TABLE_USERS_CHATS, new String[]{"uid", "cid"}, "uid = ? OR uid = ?", selectionArgs, "cid", "COUNT(uid) = 2", null);
                //.query(DBConstants.TABLE_USERS_CHATS, new String[]{"uid", "cid"}, "uid = ? OR uid = ?", selectionArgs, "cid", "COUNT(uid) = 2", null);
                break;
            case CODE_USERS_CHATS_MEMBERS:
                // SELECT users.* FROM `users_chats` LEFT JOIN users ON users_chats.uid = users.u_id WHERE cid = "c69ecf40-b441-43aa-b1c0-3495ef52a1cf"
                final String uc_cid = uri.getLastPathSegment();
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .rawQuery("SELECT contacts.* FROM users_chats LEFT JOIN contacts ON users_chats.uid = contacts.u_id WHERE cid = ? ORDER BY contacts.u_id ASC", new String[]{uc_cid});
                //Log.d(TAG, "Contacts result count for chat id: " + result.getCount());
//                while (result.moveToNext()) {
//                    Log.i(TAG, new Contact(result).toString());
//                }
                break;
            case CODE_MESSAGES_LAST_RECEIVED:
                result = DBHandler
                        .getInstance(getContext())
                        .getReadableDatabase()
                        .rawQuery("SELECT m_created_at FROM messages WHERE m_cid IN (SELECT cid FROM users_chats WHERE cid IN (SELECT cid from users_chats WHERE uid = ?) AND users_chats.uid <> ? AND m_status = 2 OR m_status = 3 GROUP BY users_chats.cid) ORDER BY m_created_at DESC LIMIT 1", selectionArgs);
                break;
            default:
                throw new UnsupportedOperationException("URI: " + uri + ", Not yet implemented!");
        }

        // notify URI of query/change
        if (result != null) {
            result.setNotificationUri(getContext().getContentResolver(), uri);
        }
        return result;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case CODE_MESSAGES_LAST_RECEIVED:
                return CONTENT_TYPE_MESSAGE_ITEM_RECEIVED;
            case CODE_CONTACTS:
                return CONTENT_TYPE_CONTACTS;
            case CODE_CONTACTS_BY_ID:
                return CONTENT_TYPE_CONTACT_ITEM;
            case CODE_CHATS:
                return CONTENT_TYPE_CHATS;
            case CODE_CHATS_MSG_JOIN:
                return CONTENT_TYPE_CHAT_MSG_JOIN;
            case CODE_CHAT_BY_ID:
                return CONTENT_TYPE_CHAT_ITEM;
            case CODE_MESSAGES:
                return CONTENT_TYPE_MESSAGES;
            case CODE_MESSAGE_BY_ID:
                return CONTENT_TYPE_MESSAGE_ITEM;
            case CODE_CHATS_MSG:
                return CONTENT_TYPE_CHATS_MESSAGE;
            case CODE_CHATS_MSGS:
                return CONTENT_TYPE_CHATS_MESSAGES;
            case CODE_CHATS_MSGS_STATUS:
                return CONTENT_TYPE_CHATS_MESSAGES_STATUS;
            case CODE_USERS_CHATS:
                return CONTENT_TYPE_USERS_CHATS;
            case CODE_USERS_CHATS_BY_ID:
                return CONTENT_TYPE_USERS_CHATS_ITEM;
            case CODE_USERS_CHATS_EXIST:
                return CONTENT_TYPE_USERS_CHATS_EXIST;
            case CODE_USERS_CHATS_MEMBERS:
                return CONTENT_TYPE_USERS_CHATS_MEMBERS;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        int uriType = uriMatcher.match(uri);
        switch (uriType) {
            case CODE_CONTACTS:
                DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .insert(DBConstants.TABLE_CONTACTS, null, contentValues);
                break;
            case CODE_CHATS:
                DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .insert(DBConstants.TABLE_CHATS, null, contentValues);
                break;
            case CODE_MESSAGES:
                DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .insert(DBConstants.TABLE_MESSAGES, null, contentValues);
                break;
            case CODE_USERS_CHATS:
                DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .insert(DBConstants.TABLE_USERS_CHATS, null, contentValues);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return uri;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String selection, String[] selectionArgs) {
        int updateCount = 0;
        int uriType = uriMatcher.match(uri);
        //Log.i(TAG, "URITYPE: " + uriType);
        switch (uriType) {
            case CODE_CONTACTS:
                updateCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .update(DBConstants.TABLE_CONTACTS, contentValues, selection, selectionArgs);
                break;
            case CODE_CONTACTS_BY_ID:
                //String idStr = uri.getLastPathSegment();
                String where = DBConstants.COLUMN_CONTACTS_USER_ID + " = ?";
                if (!TextUtils.isEmpty(selection)) {
                    where += " AND " + selection;
                }
                updateCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .update(DBConstants.TABLE_CONTACTS, contentValues, where, selectionArgs);
                break;
            case CODE_MESSAGES:
                updateCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .update(DBConstants.TABLE_MESSAGES, contentValues, selection, selectionArgs);
                break;
            case CODE_MESSAGE_BY_ID:
                updateCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .update(DBConstants.TABLE_MESSAGES, contentValues, DBConstants.COLUMN_MESSAGE_ID + " = ?", selectionArgs);
                break;
            case CODE_CHAT_BY_ID:
                //String chatId = uri.getLastPathSegment();
                String whereChat = DBConstants.COLUMN_CHAT_ID + " = ?";
                if (!TextUtils.isEmpty(selection)) {
                    whereChat += " AND " + selection;
                }
                updateCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .update(DBConstants.TABLE_CHATS, contentValues, whereChat, selectionArgs);
                break;
            default:
                // no support for updating photos!
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        // notify all listeners of changes:
        if (updateCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return updateCount;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int delCount = 0;
        switch (uriMatcher.match(uri)) {
            case CODE_CONTACTS:
                delCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .delete(DBConstants.TABLE_CONTACTS, selection, selectionArgs);
                break;
            case CODE_CONTACTS_BY_ID:
                String idStr = uri.getLastPathSegment();
                String where = DBConstants.COLUMN_CONTACTS_USER_ID + " = " + idStr;
                if (!TextUtils.isEmpty(selection)) {
                    where += " AND " + selection;
                }
                delCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .delete(DBConstants.TABLE_CONTACTS, where, selectionArgs);
                break;
            case CODE_CHAT_BY_ID:
                delCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .delete(DBConstants.TABLE_CHATS, selection, selectionArgs);
                // don't delete from users chats for now
                // delete chat from users_chats table as well
                break;
            case CODE_USERS_CHATS:
                delCount = DBHandler.getInstance(getContext())
                        .getWritableDatabase()
                        .delete(DBConstants.TABLE_USERS_CHATS, selection, selectionArgs);
                break;
            default:
                // no support for updating photos!
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
        // notify all listeners of changes:
        if (delCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return delCount;
    }

    @Override
    public ContentProviderResult[] applyBatch(
            ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        SQLiteDatabase db = DBHandler.getInstance(getContext()).getWritableDatabase();
        mIsInBatchMode.set(true);
        // the next line works because SQLiteDatabase
        // uses a thread local SQLiteSession object for
        // all manipulations
        db.beginTransaction();
        try {
            final ContentProviderResult[] retResult = super.applyBatch(operations);
            db.setTransactionSuccessful();
            getContext().getContentResolver().notifyChange(CONTENT_URI, null);
            return retResult;
        } finally {
            mIsInBatchMode.remove();
            db.endTransaction();
        }
    }

    private boolean isInBatchMode() {
        return mIsInBatchMode.get() != null && mIsInBatchMode.get();
    }
}
