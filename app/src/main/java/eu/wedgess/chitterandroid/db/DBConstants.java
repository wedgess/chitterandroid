package eu.wedgess.chitterandroid.db;

/**
 * Database Constants for column names, and queries
 * <p>
 * Created by Gareth on 10/11/2016.
 */
@SuppressWarnings({"static final StringBufferReplaceableBystatic final String", "StringBufferReplaceableByString"})
public class DBConstants {

    private DBConstants() {
        // non instantiatable
    }

    // DB
    static final String DATABASE_NAME = "chitter_db";

    static final int DATABASE_VERSION = 1;

    // Server User Table DB column names
    public static final String TABLE_USERS = "users";

    public static final String COLUMN_USER_ID = "u_id";
    public static final String COLUMN_USER_NAME = "u_name";
    public static final String COLUMN_USER_PHONE_NUM = "u_phone_number";
    public static final String COLUMN_USER_FCM_TOKEN = "u_fcm_token";
    public static final String COLUMN_USER_PASSWORD = "u_password";
    public static final String COLUMN_USER_IMG_URL = "u_profile_img_url";
    public static final String COLUMN_USER_LAST_SEEN = "u_last_seen";
    public static final String COLUMN_USER_CREATED_AT = "u_created_at";
    public static final String COLUMN_USER_STATUS = "u_status";
    public static final String TABLE_MESSAGES = "messages";

    public static final String COLUMN_MESSAGE_ID = "m_id";

    public static final String COLUMN_MESSAGE_CHAT_ID = "m_cid";
    public static final String COLUMN_MESSAGE_SENDER_ID = "m_sender_id";
    public static final String COLUMN_MESSAGE_IMG_URL = "m_media_url";
    public static final String COLUMN_MESSAGE_MSG = "m_message";
    public static final String COLUMN_MESSAGE_STATUS = "m_status";
    public static final String COLUMN_MESSAGE_TYPE = "m_type";
    public static final String COLUMN_MESSAGE_CREATED_AT = "m_created_at";
    public static final String COLUMN_MESSAGE_DELETED = "m_deleted";

    // CREATE HEADER TABLE QUERY
    static final String CREATE_TABLE_MESSAGES = new StringBuilder("CREATE TABLE ")
            .append(TABLE_MESSAGES).append("(")
            .append(COLUMN_MESSAGE_ID).append(" TEXT PRIMARY KEY, ")
            .append(COLUMN_MESSAGE_CHAT_ID).append(" INTEGER, ")
            .append(COLUMN_MESSAGE_SENDER_ID).append(" INTEGER, ")
            .append(COLUMN_MESSAGE_IMG_URL).append(" TEXT, ")
            .append(COLUMN_MESSAGE_MSG).append(" TEXT, ")
            .append(COLUMN_MESSAGE_STATUS).append(" TEXT, ")
            .append(COLUMN_MESSAGE_CREATED_AT).append(" TEXT, ")
            .append(COLUMN_MESSAGE_DELETED).append(" INTEGER DEFAULT 0, ")
            .append(COLUMN_MESSAGE_TYPE).append(" INTEGER DEFAULT 0, ")
            .append(" FOREIGN KEY (").append(COLUMN_MESSAGE_SENDER_ID)
            .append(") REFERENCES ").append(TABLE_USERS).append("(")
            .append(COLUMN_USER_ID).append("))")
            .toString();

    public static final String[] ALL_MESSAGE_FIELDS = {
            COLUMN_MESSAGE_ID,
            COLUMN_MESSAGE_CHAT_ID,
            COLUMN_MESSAGE_SENDER_ID,
            COLUMN_MESSAGE_IMG_URL,
            COLUMN_MESSAGE_MSG,
            COLUMN_MESSAGE_STATUS,
            COLUMN_MESSAGE_CREATED_AT,
            COLUMN_MESSAGE_DELETED,
            COLUMN_MESSAGE_TYPE
    };

    public static final String TABLE_CHATS = "chats";

    public static final String COLUMN_CHAT_ID = "c_id";
    public static final String COLUMN_CHAT_NAME = "c_name";
    public static final String COLUMN_CHAT_TYPE = "c_type";
    public static final String COLUMN_CHAT_CREATED_BY = "c_created_by";
    public static final String COLUMN_CHAT_CREATED_AT = "c_created_at";
    public static final String COLUMN_CHAT_UNREAD_COUNT = "c_unread_count";
    public static final String COLUMN_CHAT_MEMBERS = "c_members";
    public static final String COLUMN_CHAT_PROFILE_IMG_URL = "c_img_url";
    public static final String COLUMN_CHAT_MUTED = "c_muted";

    public static final String[] ALL_CHAT_FIELDS = {
            COLUMN_CHAT_ID,
            COLUMN_CHAT_NAME,
            COLUMN_CHAT_TYPE,
            COLUMN_CHAT_CREATED_AT,
            COLUMN_CHAT_CREATED_BY,
            COLUMN_CHAT_UNREAD_COUNT,
            COLUMN_CHAT_PROFILE_IMG_URL,
            COLUMN_CHAT_MUTED};

    // REQUEST BODY PARAMS CREATE TABLE
    static final String CREATE_TABLE_CHATS = new StringBuilder("CREATE TABLE ")
            .append(TABLE_CHATS).append("(")
            .append(COLUMN_CHAT_ID).append(" TEXT PRIMARY KEY, ")
            .append(COLUMN_CHAT_NAME).append(" TEXT, ")
            .append(COLUMN_CHAT_TYPE).append(" INTEGER, ")
            .append(COLUMN_CHAT_CREATED_BY).append(" INTEGER, ")
            .append(COLUMN_CHAT_CREATED_AT).append(" TEXT, ")
            .append(COLUMN_CHAT_UNREAD_COUNT).append(" INTEGER, ")
            .append(COLUMN_CHAT_PROFILE_IMG_URL).append(" TEXT, ")
            .append(COLUMN_CHAT_MUTED).append(" INTEGER DEFAULT 0, ")
            .append(" FOREIGN KEY (").append(COLUMN_CHAT_CREATED_BY)
            .append(") REFERENCES ").append(TABLE_USERS).append("(")
            .append(COLUMN_USER_ID).append("))")
            .toString();

    public static final String TABLE_CONTACTS = "contacts";

    public static final String COLUMN_CONTACTS_USER_ID = "u_id";


    public static final String COLUMN_CONTACTS_NAME = "u_name";
    public static final String COLUMN_CONTACTS_PHONE_NUMBER = "u_phone_number";
    public static final String COLUMN_CONTACTS_CREATED_AT = "u_created_at";
    public static final String COLUMN_CONTACTS_IMG_URL = "u_profile_img_url";
    public static final String COLUMN_CONTACTS_LAST_SEEN = "u_last_seen";
    public static final String COLUMN_CONTACTS_STATUS = "u_status";
    public static final String COLUMN_CONTACTS_EXIST_ON_DEVICE = "u_exist_on_device";
    public static final String SORT_CONTACT_NAME = COLUMN_USER_NAME + " ASC";

    public static final String[] ALL_CONTACT_FIELDS = {
            COLUMN_CONTACTS_USER_ID,
            COLUMN_CONTACTS_NAME,
            COLUMN_CONTACTS_PHONE_NUMBER,
            COLUMN_CONTACTS_CREATED_AT,
            COLUMN_CONTACTS_IMG_URL,
            COLUMN_CONTACTS_LAST_SEEN,
            COLUMN_CONTACTS_STATUS,
            COLUMN_CONTACTS_EXIST_ON_DEVICE
    };

    // CREATE REQUEST TABLE QUERY
    static final String CREATE_TABLE_CONTACTS = new StringBuilder()
            .append("CREATE TABLE ")
            .append(TABLE_CONTACTS).append(" (")
            .append(COLUMN_CONTACTS_USER_ID).append(" INTEGER PRIMARY KEY, ")
            .append(COLUMN_CONTACTS_NAME).append(" TEXT, ")
            .append(COLUMN_CONTACTS_PHONE_NUMBER).append(" TEXT, ")
            .append(COLUMN_CONTACTS_IMG_URL).append(" TEXT, ")
            .append(COLUMN_CONTACTS_LAST_SEEN).append(" TEXT, ")
            .append(COLUMN_CONTACTS_CREATED_AT).append(" TEXT, ")
            .append(COLUMN_CONTACTS_STATUS).append(" TEXT, ")
            .append(COLUMN_CONTACTS_EXIST_ON_DEVICE).append(" INTEGER DEFAULT 0")
            .append(")")
            .toString();

    public static final String TABLE_USERS_CHATS = "users_chats";

    public static final String COLUMN_USERS_CHATS_MEMBERS_COUNT = "members_count";
    public static final String COLUMN_USERS_CHATS_UID = "uid";
    public static final String COLUMN_USERS_CHATS_CID = "cid";

    static final String CREATE_TABLE_USERS_CHATS = new StringBuilder()
            .append("CREATE TABLE ")
            .append(TABLE_USERS_CHATS).append(" (")
            .append(COLUMN_USERS_CHATS_UID).append(" TEXT, ")
            .append(COLUMN_USERS_CHATS_CID).append(" TEXT")
            .append(", UNIQUE(")
            .append(COLUMN_USERS_CHATS_UID)
            .append(", ")
            .append(COLUMN_USERS_CHATS_CID)
            .append(") ON CONFLICT REPLACE")
            .append(")")
            .toString();
}
