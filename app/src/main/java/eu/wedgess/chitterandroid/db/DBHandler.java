package eu.wedgess.chitterandroid.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;

import static eu.wedgess.chitterandroid.db.DBConstants.CREATE_TABLE_CHATS;
import static eu.wedgess.chitterandroid.db.DBConstants.CREATE_TABLE_CONTACTS;
import static eu.wedgess.chitterandroid.db.DBConstants.CREATE_TABLE_MESSAGES;
import static eu.wedgess.chitterandroid.db.DBConstants.CREATE_TABLE_USERS_CHATS;
import static eu.wedgess.chitterandroid.db.DBConstants.DATABASE_NAME;
import static eu.wedgess.chitterandroid.db.DBConstants.DATABASE_VERSION;

/**
 * Helper class for DB, takes care of creating and updating DB.
 * It is a singleton.
 * <p>
 * Created by Gareth on 10/11/2016.
 */
@SuppressWarnings("StringBufferReplaceableByString")
public class DBHandler extends SQLiteOpenHelper {

    private static final String TAG = DBHandler.class.getSimpleName();
    private static DBHandler mInstance;
    private final Context context;

    public static synchronized DBHandler getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (mInstance == null) {
            mInstance = new DBHandler(context);
        }
        return mInstance;
    }

    private DBHandler(final Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context.getApplicationContext();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // creating apps SQLite tables
        //db.execSQL(CREATE_TABLE_USERS);
        db.execSQL(CREATE_TABLE_CONTACTS);
        db.execSQL(CREATE_TABLE_MESSAGES);
        db.execSQL(CREATE_TABLE_CHATS);
        db.execSQL(CREATE_TABLE_USERS_CHATS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);
    }

    private void notifyProviderOnContactsChange() {
        context.getContentResolver().notifyChange(
                ChitterContentProvider.CONTENT_URI_CONTACTS, null, false);
    }

    public List<Contact> getAllContacts() {

        final List<Contact> contactList = new ArrayList<>();
//
//        AsyncQueryHandler asyncQueryHandler = new AsyncQueryHandler(context.getApplicationContext()
//                .getContentResolver()) {
//            @Override
//            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
//                super.onQueryComplete(token, cookie, cursor);
//                if (cursor != null) {
//                    try {
//                        while (cursor.moveToNext()) {
//                            contactList.add(new Contact(cursor));
//                        }
//                    } finally {
//                        cursor.close();
//                    }
//                }
//            }
//        };
//        asyncQueryHandler.startQuery(1, null, ChitterContentProvider.CONTENT_URI_CONTACTS, null, null, null, null);
        Cursor cursor = context.getApplicationContext().getContentResolver()
                .query(ChitterContentProvider.CONTENT_URI_CONTACTS, null, null, null, null, null);
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    contactList.add(new Contact(cursor));
                }
            } finally {
                cursor.close();
            }
        }

//        Cursor cursor = context.getApplicationContext()
//                .getContentResolver().query(ChitterContentProvider.CONTENT_URI_CONTACTS, null, null, null, null);


        return contactList;
    }

    public Contact getContactById(long id) {
        Cursor cursor = context.getApplicationContext().getContentResolver().query(
                ChitterContentProvider.CONTENT_URI_CONTACT_ITEM, null, null, new String[] {String.valueOf(id)}, null, null);
        Contact existingContact = null;
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                existingContact = new Contact(cursor);
            }
            cursor.close();
        }
        return existingContact;
    }

    public void addUserToContacts(Contact contact, Context context) {
        Contact existingContact = getContactById(contact.getId());
        if (existingContact == null) {
            context.getApplicationContext().getContentResolver()
                    .insert(ChitterContentProvider.CONTENT_URI_CONTACTS, contact.getContentValues());
            Log.i(TAG, "Inserted contact with ID: " + contact.getId());
        } else {
            Log.i(TAG, "Contact NOT Added - already exists");
            if (!existingContact.equals(contact)) {
                Log.i(TAG, "Existing contact UPDATED");
                //DBUtils.updateContactRow(contact1, context);
                DBHandler.getInstance(context).updateContact(contact);
            }
        }
    }


    public boolean updateContact(Contact contact) {
        int result = context.getApplicationContext().getContentResolver().update(ChitterContentProvider.CONTENT_URI_CONTACT_ITEM,
                contact.getContentValues(), null, new String[]{String.valueOf(contact.getId())});
        if (result > 0) {
            context.getApplicationContext().getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CONTACTS, null);
            return true;
        } else {
            return false;
        }
    }

    public boolean updateMessage(Message message) {
        Context appContext = context.getApplicationContext();
        int result = appContext.getContentResolver().update(ChitterContentProvider.CONTENT_URI_MESSAGE_ITEM,
                message.getContentValues(), null, new String[]{message.getId()});
        if (result > 0) {
            //appContext.getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHATS_MESSAGES, null);
            appContext.getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN, null);
            appContext.getContentResolver().notifyChange(
                    Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_CHATS_MESSAGES, message.getCid()), null);
            return true;
        } else {
            return false;
        }
    }

    //TODO: this can all be done in the background
    public boolean updateChatUnreadCount(String chatId, boolean isReset) {
        Context appContext = context.getApplicationContext();
        Cursor chatCursor = appContext.getContentResolver().query(ChitterContentProvider.CONTENT_URI_CHAT_ITEM, null,
                null, new String[]{chatId}, null);
        ChatRoom chatRoom = null;
        if (chatCursor != null && chatCursor.moveToFirst()) {
            chatRoom = new ChatRoom(chatCursor);
            if (isReset) {
                // reset count to 0 as user is in chat room
                chatRoom.setUnreadCount(0);
            } else {
                // update chat room unread count
                int unreadCount = chatRoom.getUnreadCount();
                //Log.i(TAG, "Unread count before update: " + unreadCount);
                chatRoom.setUnreadCount((unreadCount + 1));
            }
        }
        if (chatRoom != null) {
            int result = appContext.getContentResolver().update(ChitterContentProvider.CONTENT_URI_CHAT_ITEM,
                    chatRoom.getContentValues(), null, new String[]{chatRoom.getId()});
            if (result > 0) {
                //Log.i(TAG, "Unread Notifying msg_join URI of changes: " + chatRoom.getUnreadCount());
                appContext.getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN, null);
                return true;
            }
        }
        return false;

    }

    public boolean updateChatRoom(ChatRoom chatRoom) {
        Context appContext = context.getApplicationContext();
        if (chatRoom != null) {
            int result = appContext.getContentResolver().update(ChitterContentProvider.CONTENT_URI_CHAT_ITEM,
                    chatRoom.getContentValues(), null, new String[]{chatRoom.getId()});
            if (result > 0) {
                appContext.getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN, null);
                return true;
            }
        }
        return false;
    }

    public void addMessage(Message message, Context context) {
        Context appContext = context.getApplicationContext();
        // insert message
        appContext.getContentResolver().insert(ChitterContentProvider.CONTENT_URI_MESSAGES, message.getContentValues());
        // update chat home loader
        appContext.getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN, null);
//        appContext.getContentResolver().notifyChange(
//                Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_CHATS_MESSAGES, message.getCid()), null);
    }

    public void createChat(ChatRoom chatRoom, int[] uids) {
        context.getContentResolver().insert(ChitterContentProvider.CONTENT_URI_CHATS, chatRoom.getContentValues());
        ContentValues contentValues = new ContentValues(uids.length);
        contentValues.put(DBConstants.COLUMN_USERS_CHATS_CID, chatRoom.getId());
        for (int uid : uids) {
            contentValues.put(DBConstants.COLUMN_USERS_CHATS_UID, uid);
            context.getContentResolver().insert(ChitterContentProvider.CONTENT_URI_USERS_CHATS, contentValues);
        }
        context.getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN, null);
    }

    public int deleteChatById(String cid, boolean includeUsersChats) {
        int affectedRows = context.getContentResolver().delete(ChitterContentProvider.CONTENT_URI_CHAT_ITEM,
                DBConstants.COLUMN_CHAT_ID + " = ?", new String[]{cid});
        if (includeUsersChats) {
            affectedRows += context.getContentResolver().delete(ChitterContentProvider.CONTENT_URI_USERS_CHATS,
                    DBConstants.COLUMN_USERS_CHATS_CID + " = ?", new String[]{cid});
        }
        if (affectedRows > 0) {
            context.getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN, null);
        }
        return affectedRows;
    }
}
