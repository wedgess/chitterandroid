package eu.wedgess.chitterandroid.helpers;

/**
 * Created by Gareth on 22/12/2016.
 */

public class Constants {

    public static final int CHAT_INFO_REQ_CODE = 112;

    private Constants() {
        // non instantiable
    }

    // preferences keys
    public static final String PREF_NOTIF_LIGHTS = "notif_lights";
    public static final String PREF_SEND_ON_ENTER = "send_on_enter";
    public static final String PREF_CHAT_FONT_SIZE = "chat_text_size";
    public static final String PREF_FCM_TOKEN = "fcm_token";
    public static final String PREF_NOTIF_VIBRATION = "notif_vibrate";
    public static final String PREF_USER_JSON = "user";

    // status
    public static final String STATUS_ONLINE = "Online";
    public static final String SYNC_FROM_NOW = "now";
    public static final int INVALID_POSITION = -1;

    // loader IDs
    public static final int LOADER_ID_CONTACTS = 1;
    public static final int LOADER_ID_CHAT_HOME = 2;
    public static final int LOADER_ID_MESSAGES = 3;
    public static final int LOADER_ID_CHAT_CONTACTS = 4;
    public static final int LOADER_ID_GROUP_INFO = 5;

    // permission request codes
    public static final int WRITE_CONTACT_PERMISSION = 101;

    public static final int WRITE_CONTACT_START = 102;
    public static final int NEW_MESSAGE_CONTACTS_REQUEST = 911;
    public static final int CHECK_CONTACTS_REQUEST = 912;

    // AsyncQueryHandler IDS
    // ChatRoomActivities
    public static final int QUERY_ID_UPDATE_MESSAGES_STATUS = 1;
    public static final int QUERY_ID_GET_CHAT_MEMBERS = 2;
    public static final int QUERY_ID_CREATE_CHAT = 3;
    public static final int QUERY_ID_UPDATE_CONTACT_NAME = 4;
    public static final int QUERY_ID_CHAT_EXIST = 5;

    // Bundle keys
    public static final String KEY_CONTACT = "keyContact";
    public static final String KEY_CHAT = "keyChat";
    public static final String KEY_GROUP_IMG_LOCATION = "groupImgKey";
}
