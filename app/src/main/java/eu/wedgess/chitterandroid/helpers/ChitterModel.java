package eu.wedgess.chitterandroid.helpers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.utils.JsonParser;
import eu.wedgess.chitterandroid.utils.Utils;

import static eu.wedgess.chitterandroid.helpers.Constants.PREF_USER_JSON;

/**
 * For holding all the data about applications user.
 * <p>
 * Created by Gareth on 05/12/2016.
 */

public class ChitterModel {

    private final String TAG = ChitterModel.class.getSimpleName();

    private static ChitterModel sInstance;
    private User mUser;
    private String mCurrentChat = null;
    private RequestQueue mRequestQueue;
    // default is to show notifications
    private boolean mShowNotification = true;

    public boolean showNotification() {
        return mShowNotification;
    }

    public void setShowNotification(boolean mShowNotification) {
        this.mShowNotification = mShowNotification;
    }

    // initialize the requestQue for Volley
    private ChitterModel(Context context) {
        mRequestQueue = getRequestQueue(context);
    }

    public static ChitterModel getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new ChitterModel(context);
        }
        return sInstance;
    }

    /**
     * Gets the current user if not already set
     *
     * @param context
     * @return {@link User}
     */
    public User getUser(Context context) {
        // if the user is null try get the user from shared preferences
        if (mUser == null) {
            User user = Utils.getUserFromPrefs(context);
            if (user != null) {
                this.mUser = user;
            }
        }
        return mUser;
    }

    // set the models users
    public void setUser(User user) {
        // when a user is set they will always be online so set them as online
        user.setLastSeen(Constants.STATUS_ONLINE);
        this.mUser = user;
    }

    // get current chat ID of chat room that user is in
    public String getCurrentChat() {
        return mCurrentChat;
    }

    // set the current chat that user is in
    public void setCurrentChat(String currentChat) {
        this.mCurrentChat = currentChat;
    }

    // check if the user with id (id) is owner
    public boolean userIsOwner(long id, Context context) {
        return id == getUser(context).getId();
    }

    // update user in prefs and model
    public void updateUser(User user, Context context) {
        setUser(user);
        updateUserInPrefs(user, context);
    }

    private void updateUserInPrefs(User user, Context context) {
        JSONObject userJson = null;
        try {
            userJson = JsonParser.userToJson(user);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (userJson != null) {
            Utils.setStringPref(PREF_USER_JSON, userJson.toString(), context);
            Log.i(TAG, "updated user in preferences --> " + Utils.getStringPref(PREF_USER_JSON, "", context));
        }
    }

    // get volleys requestQueue
    private RequestQueue getRequestQueue(Context context) {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, Context context) {
        getRequestQueue(context).add(req);
    }
}
