package eu.wedgess.chitterandroid.helpers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.activities.ChatHomeActivity;
import eu.wedgess.chitterandroid.activities.ChatRoomActivity;
import eu.wedgess.chitterandroid.adapters.SyncAdapter;
import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.fcm.ChitterFirebaseMessagingService;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.tasks.GetImageTask;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;

/**
 * Used to handle new incoming messages from both {@link ChitterFirebaseMessagingService} &
 * {@link SyncAdapter}.
 * If in the chat or chat home then no notification is shown, the receiver will handle the message sound.
 * But if app is exited or in a different chat than the received message notification is shown.
 * This class handles updating chat unread count etc...
 * <p>
 * Created by Gareth on 03/02/2017.
 */

public class IncomingMessageHelper {

    public static final String TAG = IncomingMessageHelper.class.getSimpleName();
    private static HashMap<String, Integer> sNotificationMap = new HashMap<>();

    private IncomingMessageHelper() {
        // non instantiable
    }

    public static void removeNotificationByChatId(String cid, Context context) {
        // if map contains key Chat ID, cancel notification and remove cid from map
        if (sNotificationMap.containsKey(cid)) {
            Log.d(TAG, "Map contains key --> " + cid);
            NotificationManager notifyMgr = (NotificationManager) context
                    .getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            notifyMgr.cancel(sNotificationMap.get(cid));
            sNotificationMap.remove(cid);
        } else {
            Log.d(TAG, "Map DOES NOT contains key --> " + cid);
        }
    }

    public static void handleNewMessage(final Message message, final String notifTitle, final Context appContext) {

        Log.i(TAG, "In handleNewMessage()");
        // get the chat item from local DB
        Cursor cursor = appContext.getContentResolver().query(ChitterContentProvider.CONTENT_URI_CHAT_ITEM, null,
                null, new String[]{message.getCid()}, null);
        // if the chat exists
        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            Log.i(TAG, "In handleNewMessage() Cursor not null for chatroom");
            // add message and get the chat
            DBHandler.getInstance(appContext).addMessage(message, appContext);
            ChatRoom chatRoom = new ChatRoom(cursor);
            cursor.close();
            handleMessage(chatRoom, message, notifTitle, false, appContext);
        } else {
            Log.i(TAG, "In handleNewMessage() Cursor is null for chatroom");
            // chat with id msgChatId doesn;t exist locally so create it, pull from remote DB,
            // this handles creating users who don;t exist in device contacts, they get created in Contact table
            final Message finalMessage = message;
            APIRequestUtility.createLocalChatFromRemote(message.getCid(), appContext,
                    new APIRequestUtility.ChatCreatedServerCallback() {
                        @Override
                        public void onComplete(boolean error, ChatRoom chat) {
                            if (error) {
                                Log.e(TAG, "Error returned from server when creating chat");
                            } else {
                                // chat has been created locally so add message and update chat unread count
                                DBHandler.getInstance(appContext).addMessage(finalMessage, appContext);
                                DBHandler.getInstance(appContext).updateChatUnreadCount(finalMessage.getCid(), false);
                                handleMessage(chat, finalMessage, notifTitle, true, appContext);
                            }
                        }
                    });
        }
    }

    public static void handleMessage(ChatRoom chatRoom, Message message, String notifTitle, boolean newChat, Context appContext) {
        Log.i(TAG, "in handleMessage()");
        boolean updateMessageCount = false;
        ChitterModel model = ChitterModel.getInstance(appContext);
        // app in BG
        if (model.showNotification()) {
            Log.i(TAG, ">>>>>>>>>>>>>>> App in BG");
            prepareNotificationData(chatRoom, message, notifTitle, appContext);
            updateMessageCount = true;
        }
        // probably in home screen
        else if (model.getCurrentChat() == null) {
            Log.i(TAG, ">>>>>>>>>>>>>>> At Home screen");
            sendMessageToReceiver(message, appContext);
            updateMessageCount = true;
        }
        // in the current chat
        else if (model.getCurrentChat().equals(message.getCid())) {
            Log.i(TAG, ">>>>>>>>>>>>>>> In the current chat let receiver handle it");
            // in the current chat let the receiver handle the message sound
            sendMessageToReceiver(message, appContext);
        }
        // in another chat so show notification
        else {
            Log.i(TAG, ">>>>>>>>>>>>>>> In the another chat so show notification");
            prepareNotificationData(chatRoom, message, notifTitle, appContext);
            updateMessageCount = true;
        }

        if (!newChat && updateMessageCount) {
            DBHandler.getInstance(appContext).updateChatUnreadCount(message.getCid(), false);
        }
        // send delivered receipt back to sender
        if (message.getStatus() == Message.STATUS_DELIVERED_TO_SERVER) {
            // update message status on server, if succesful local DB's message is then updated
            APIRequestUtility.updateMessageStatus(message, Message.STATUS_DELIVERED_TO_USER, appContext);
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     */
    private static void prepareNotificationData(final ChatRoom chatRoom, final Message message, String title, final Context appContext) {
        //String title = notification.getTitle();
        // new bundle to store the values of the extras
        final Bundle bundle = new Bundle();

        Log.i(TAG, "Notification title: " + title);
        // if its an id, it is if user to user chat, if group it will be chat name
        Contact contact = null;
        if (!TextUtils.isEmpty(title) && Utils.isNumeric(title)) {
            //Log.i(TAG, "Is digits ONLY");
            // get the contact by ID
            Cursor cursor = appContext.getContentResolver().query(
                    ChitterContentProvider.CONTENT_URI_CONTACT_ITEM, null, null, new String[]{title}, null, null);
            if (cursor != null) {
                Log.d(TAG, "Cursor not null");
                Log.d(TAG, "Cursor Moving to FIrst");
                if (cursor.moveToFirst()) {
                    // get contact from cursor
                    contact = new Contact(cursor);
                    // get title as contact name
                    title = contact.getName();
                    bundle.putParcelable(Constants.KEY_CONTACT, contact);
                }
                cursor.close();
            }
        } else {
            Log.d(TAG, "Not numeric or digit title: " + title);
        }
        int chatUnreadCount = chatRoom.getUnreadCount() + 1;
        // and unread count is greater than 0 then add the amount of messages for notification title
        if (chatUnreadCount > 1) {
            title = appContext.getString(R.string.notif_title_new_message, title, chatUnreadCount);
        }
        // need to set chat unread count so it can be cleared when clicking notification
        // to be brought to chat
        chatRoom.setUnreadCount(chatUnreadCount);
        sendNotification(contact, message, chatRoom, title, bundle, appContext);
    }

    private static void sendNotification(Contact contact, final Message message, final ChatRoom chatRoom,
                                         final String title, Bundle bundle, final Context appContext) {
        // ONLY if the chat is not muted show notification
        if (!chatRoom.isMuted()) {
            Log.i(TAG, "Showing notification");
            Intent intent = new Intent(appContext, ChatHomeActivity.class);
            // add chat to bundle
            bundle.putParcelable(Constants.KEY_CHAT, chatRoom);
            // chat exists in DB so its an existing chat
            bundle.putInt(ChatRoomActivity.KEY_NEW_CHAT, ChatRoomActivity.EXISTING_CHAT);
            bundle.putBoolean(ChatRoomActivity.KEY_FROM_NOTIF, true);
            intent.putExtras(bundle);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            final PendingIntent pendingIntent = PendingIntent.getActivity(appContext, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            // get the profile img url to show in notifications
            String profileUrl = "";
            if (chatRoom.getChatType() == ChatRoom.GROUP_CHAT) {
                if (!TextUtils.isEmpty(chatRoom.getGroupProfileImgUrl())) {
                    profileUrl = chatRoom.getGroupProfileImgUrl();
                }
            } else {
                if (contact != null && !TextUtils.isEmpty(contact.getImgUrl())) {
                    profileUrl = contact.getImgUrl();
                }
            }

            // if url is not empty then get the image, using Picasso for some reason gave network on menu_chat_home thread
            // so use AsyncTask to get image from URL so that it is in the BG
            if (!TextUtils.isEmpty(profileUrl)) {
                new GetImageTask(new GetImageTask.GetImageListener() {
                    @Override
                    public void onComplete(Bitmap bitmap) {
                        NotificationCompat.Builder builder = UIHelper.buildNotification(title, message, pendingIntent, appContext);
                        if (bitmap != null) {
                            builder.setLargeIcon(bitmap);
                        }
                        notificationShow(builder, message.getCid(), appContext);
                    }
                }).execute(profileUrl);
            } else {
                // mo profile image so create a character drawable to display in notification tray
                NotificationCompat.Builder builder = UIHelper.buildNotification(title, message, pendingIntent, appContext);
                builder.setLargeIcon(UIHelper.drawableToBitmap(new CharacterDrawable(title, appContext), appContext));

                notificationShow(builder,
                        message.getCid(), appContext);
            }
        }
    }

    /**
     * Generates a unique int from a string. This is used to generate an ID
     * for notifications.
     *
     * @param stringId
     * @return
     */
    public static int generateNotificationID(String stringId) {
        int h = 0;
        for (int i = 0; i < stringId.length(); i++) {
            h = 31 * h + stringId.charAt(i);
        }
        return h;
    }

    private static void notificationShow(NotificationCompat.Builder builder, String cid, Context appContext) {
        int notifID = generateNotificationID(cid);
        NotificationManager notificationManager =
                (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);

        sNotificationMap.put(cid, notifID);
        notificationManager.notify(notifID, builder.build());
    }

    private static void sendMessageToReceiver(Message message, Context appContext) {
        Log.i(TAG, "Sending result back to receiver");
        // intent needs to use the same action
        Intent broadcastIntent = new Intent(ChitterFirebaseMessagingService.ACTION);
        // send back response as an extra
        broadcastIntent.putExtra(ChitterFirebaseMessagingService.BROADCAST_MESSAGE, message);
        // resnt broadcast back to the local broadcast receiver in MainActivity
        LocalBroadcastManager.getInstance(appContext).sendBroadcast(broadcastIntent);
    }
}