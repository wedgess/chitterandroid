package eu.wedgess.chitterandroid.helpers;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.PendingIntent;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.utils.Utils;

/**
 * Helper class for anything UI related
 * Created by Gareth on 24/01/2017.
 */

public class UIHelper {

    private static final int SEND_BTN_ANIMATION_DURATION = 200;
    private static final long SELECTED_ANIMATION_DURATION = 400;

    private UIHelper() {
        // non instantiable
    }

    /**
     * Get the selectableItemBackground programmatically so that the recycler items give
     * touch feedback. Such as ripple effect, setting an items background as R.color.normal_bg
     * or and other color will override touch feedback.
     *
     * @param context - the current context
     * @return - resource id for attribute
     */
    public static int getSelectedItemBgAttr(Context context) {
        int[] attrs = new int[]{R.attr.selectableItemBackground};
        TypedArray typedArray = context.obtainStyledAttributes(attrs);
        int resId = typedArray.getResourceId(0, 0);
        typedArray.recycle();
        return resId;
    }

    /**
     * Checks if TextInputLayouts are empty or not and sets error message if so
     *
     * @param til      - til to check
     * @param errorMsg - error msg to display on til
     * @return - empty or not
     */
    public static boolean tilIsNotEmpty(TextInputLayout til, String errorMsg) {
        if (TextUtils.isEmpty(til.getEditText().getText())) {
            til.setErrorEnabled(true);
            til.setError(errorMsg);
            return false;
        } else {
            if (til.isErrorEnabled()) {
                til.setErrorEnabled(false);
            }
            return true;
        }
    }

    /**
     * Converts DP to Pixels
     *
     * @param dp      - dp to convert
     * @param context - this context
     * @return - pixels
     */
    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) ((dp * displayMetrics.density) + 0.5);
    }

    /**
     * Converts Pixels to DP
     *
     * @param px      - pixels to convert to DP
     * @param context - this context
     * @return - DP
     */
    @SuppressWarnings("unused")
    public static int pxToDp(int px, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) ((px / displayMetrics.density) + 0.5);
    }

    /**
     * Sets color of chat author name or chat circle depending on first character of users name
     *
     * @param key The key used to generate the tile color
     * @return A new or previously chosen color for <code>key</code> used as the
     * tile background color
     */
    public static int getColorForLetter(String key, Resources resources, int arrayResId) {
        TypedArray mColors = resources.obtainTypedArray(arrayResId);
        // String.hashCode() is not supposed to change across java versions, so
        // this should guarantee the same key always maps to the same color
        int color = Math.abs(key.hashCode()) % mColors.length() - 1;
        if (color < 0) {
            // change from negative to positive
            color *= -1;
        }
        try {
            return mColors.getColor(color, Color.BLACK);
        } finally {
            mColors.recycle();
        }
    }


    // send buttons animator set
    public static AnimatorSet getSendAnimatorSet(final ImageButton attachFileBTN, float scaleFrom, float scaleTo) {
        // animate x and y scale of attach button
        ObjectAnimator attachAnimatorX = ObjectAnimator.ofFloat(attachFileBTN, View.SCALE_X, scaleFrom, scaleTo)
                .setDuration(SEND_BTN_ANIMATION_DURATION);
        ObjectAnimator attachAnimatorY = ObjectAnimator.ofFloat(attachFileBTN, View.SCALE_Y, scaleFrom, scaleTo)
                .setDuration(SEND_BTN_ANIMATION_DURATION);

        // anticipate overshoot so it bounces out
        attachAnimatorX.setInterpolator(new AnticipateOvershootInterpolator());
        attachAnimatorY.setInterpolator(new AnticipateOvershootInterpolator());
        // put both animations in a set to play at the same time
        AnimatorSet setAttach = new AnimatorSet();
        setAttach.playTogether(attachAnimatorX, attachAnimatorY);
        return setAttach;
    }

    // play item selection animation for multi select (ChatHome & Contacts)
    public static void playSelectAnimation(final ImageView selectedIV, float scaleFrom, float scaleTo) {
        ObjectAnimator selectedAnimatorX = ObjectAnimator.ofFloat(selectedIV, View.SCALE_X, scaleFrom, scaleTo)
                .setDuration(SELECTED_ANIMATION_DURATION);
        ObjectAnimator selectedAnimatorY = ObjectAnimator.ofFloat(selectedIV, View.SCALE_Y, scaleFrom, scaleTo)
                .setDuration(SELECTED_ANIMATION_DURATION);
        // animator set for send button so animations play at same time
        AnimatorSet unselectedSet = new AnimatorSet();
        unselectedSet.setInterpolator(new AnticipateOvershootInterpolator());
        unselectedSet.playTogether(selectedAnimatorX, selectedAnimatorY);

        // if deselecting item
        if (scaleFrom == 1f && scaleTo == 0f) {
            unselectedSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    // when animation ends hide the send button or we still get its clicks
                    selectedIV.setVisibility(View.GONE);
                }
            });
        } else {
            // selecting item
            selectedIV.setScaleX(0f);
            selectedIV.setScaleY(0f);
            selectedIV.setVisibility(View.VISIBLE);
        }
        unselectedSet.start();
    }

    /**
     * Create a circular bitmap from normal square bitmap
     *
     * @param bitmap - bitmap to crop
     * @param radius - radius of bitmap
     * @return cropped BitMap
     */
    public static Bitmap getCroppedBitmap(Bitmap bitmap, int radius) {
        Bitmap scaledBitMap;

        // scale bitmap if needed
        if (bitmap.getWidth() != radius || bitmap.getHeight() != radius) {
            float smallest = Math.min(bitmap.getWidth(), bitmap.getHeight());
            float factor = smallest / radius;
            scaledBitMap = Bitmap.createScaledBitmap(bitmap, (int) (bitmap.getWidth() / factor), (int) (bitmap.getHeight() / factor), false);
        } else {
            scaledBitMap = bitmap;
        }

        // max clarity
        Bitmap output = Bitmap.createBitmap(radius, radius,
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, radius, radius);

        // draw circle on canvas then draw the bitmap
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(Color.WHITE);
        canvas.drawCircle(radius / 2 + 0.7f,
                radius / 2 + 0.7f, radius / 2 + 0.1f, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(scaledBitMap, rect, rect, paint);

        // recycle original bitmap
        bitmap.recycle();

        // return cropped circle bitmap
        return output;
    }

    public static Bitmap drawableToBitmap(Drawable drawable, Context context) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        float width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : context.getResources().getDimension(R.dimen.chat_profile_img_size);
        float height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : context.getResources().getDimension(R.dimen.chat_profile_img_size);

        Bitmap bitmap = Bitmap.createBitmap((int) width, (int) height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    // Build Chitters message notification
    public static NotificationCompat.Builder buildNotification(String title, Message message,
                                                               PendingIntent pendingIntent, Context appContext) {
        Uri soundUri = Uri.parse("android.resource://" + appContext.getPackageName() + "/" + R.raw.incoming);
        //Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String lights = Utils.getStringPref(Constants.PREF_NOTIF_LIGHTS, "5", appContext);
        int color = 0;
        switch (lights) {
            case "1":
                color = Color.RED;
                break;
            case "2":
                color = Color.BLUE;
                break;
            case "3":
                color = Color.GREEN;
                break;
            case "4":
                color = Color.WHITE;
                break;
            case "5":
                color = Color.CYAN;
                break;
            case "6":
                color = Color.YELLOW;
                break;
            case "7":
                color = Color.MAGENTA;
                break;
        }
        String vibration = Utils.getStringPref(Constants.PREF_NOTIF_VIBRATION, "1", appContext);
        long[] vibrationIntensity = new long[]{0, 0, 0};
        switch (vibration) {
            case "1":
                vibrationIntensity = new long[]{0, 250, 1000};
                break;
            case "2":
                vibrationIntensity = new long[]{0, 50, 1000};
                break;
            case "3":
                vibrationIntensity = new long[]{0, 1000, 1000};
                break;
        }

        String msg = message.getMessage();
        if (message.isImage()) {
            msg = new String(Character.toChars(0x1F4F7)) + appContext.getString(R.string.image_postfix);
        }

        NotificationCompat.Builder notificationCompatBuilder = new NotificationCompat.Builder(appContext)
                .setSmallIcon(R.drawable.ic_chitter_notification)
                .setContentTitle(title)
                //.setDefaults(DEFAULT_VIBRATE | FLAG_SHOW_LIGHTS)
                .setContentText(msg)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(appContext, R.color.colorPrimary))
                .setSound(soundUri)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setWhen(System.currentTimeMillis())
                .setTicker(title + ": " + msg)
                .setContentIntent(pendingIntent);

        if (color != 0) {
            notificationCompatBuilder.setLights(color, 300, 100);
        }
        if (!vibration.equals("0")) {
            notificationCompatBuilder.setVibrate(vibrationIntensity);
        }

        return notificationCompatBuilder;
    }

    // send buttons animator set
    public static AnimatorSet getScaleAnimatorSet(final ImageButton btn, float scaleFrom, float scaleTo, int duration) {
        // animate x and y scale of attach button
        ObjectAnimator attachAnimatorX = ObjectAnimator.ofFloat(btn, View.SCALE_X, scaleFrom, scaleTo)
                .setDuration(duration);
        ObjectAnimator attachAnimatorY = ObjectAnimator.ofFloat(btn, View.SCALE_Y, scaleFrom, scaleTo)
                .setDuration(duration);

        // anticipate overshoot so it bounces out
        if (scaleTo == 0) {
            attachAnimatorX.setInterpolator(new DecelerateInterpolator());
            attachAnimatorY.setInterpolator(new DecelerateInterpolator());
        } else {
            attachAnimatorX.setInterpolator(new AccelerateInterpolator());
            attachAnimatorY.setInterpolator(new AccelerateInterpolator());
        }
        // put both animations in a set to play at the same time
        AnimatorSet setAttach = new AnimatorSet();
        setAttach.playTogether(attachAnimatorX, attachAnimatorY);
        return setAttach;
    }
}
