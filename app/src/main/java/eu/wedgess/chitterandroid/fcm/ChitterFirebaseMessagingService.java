package eu.wedgess.chitterandroid.fcm;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.IncomingMessageHelper;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.JsonParser;
import eu.wedgess.chitterandroid.utils.Utils;

/**
 * Created by Gareth on 02/11/2016.
 */
public class ChitterFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = ChitterFirebaseMessagingService.class.getSimpleName();

    public static final String BROADCAST_BUNDLE = "broadcast_bundle";
    public static final String BROADCAST_MESSAGE = "broadcast_msg";
    public static final String BROADCAST_STATUS = "broadcast_status";
    public static final String BROADCAST_UID = "broadcast_uid";
    public static final String ACTION = ChitterFirebaseMessagingService.class.getName();

    @Override
    public void onSendError(String s, Exception e) {
        super.onSendError(s, e);
        e.printStackTrace();
        Log.e(TAG, "FCM messaging error, doing full sync..");
        ContentResolver.requestSync(Utils.createDummyAccount(getApplicationContext()),
                ChitterContentProvider.AUTHORITY, Bundle.EMPTY);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // synchronize on lock so that we get one message at a a time
        synchronized (TAG) {
            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                Log.d(TAG, "Message data payload: " + remoteMessage.getData());

                try {
                    // get the data as a JSON object
                    JSONObject response = new JSONObject(remoteMessage.getData());
                    // will always have a message object so parse to a POJO
                    Message message = null;
                    if (response.has(APIRequestUtility.FCM_RESPONSE_MSG)) {
                        message = JsonParser.jsonToMessage(new JSONObject(response.getString(APIRequestUtility.FCM_RESPONSE_MSG)));
                    }

                    // if the response has a message_status then its a read receipt message
                    if (message != null && response.has(APIRequestUtility.FCM_RESPONSE_MSG_STATUS)) {
                        Log.i(TAG, "Is read receipt");
                        Message currentMessage = null;
                        Cursor cursor = getContentResolver().query(ChitterContentProvider.CONTENT_URI_MESSAGE_ITEM,
                                null, null, new String[]{message.getId()}, null);
                        if (cursor != null) {
                            if (cursor.moveToFirst()) {
                                currentMessage = new Message(cursor);
                            }
                            cursor.close();
                        }
                        Log.d(TAG, "Setting message: " + message.getId() + " status as:" + message.getStatus());
                        // set message status as the status went in the read receipt
                        message.setStatus(Integer.parseInt((String) response.get(APIRequestUtility.FCM_RESPONSE_MSG_STATUS)));
                        if (currentMessage != null) {
                            if (message.getStatus() > currentMessage.getStatus()) {
                                DBHandler.getInstance(getApplicationContext()).updateMessage(message);
                            }
                        } else {
                            DBHandler.getInstance(getApplicationContext()).updateMessage(message);
                        }


                        // update message status in DB
                        //DBHandler.getInstance(getApplicationContext()).updateMessage(message);
                    }
                    // its a response to update a users last seen status
                    else if (response.has(APIRequestUtility.FCM_RESPONSE_UPDATE_LAST_SEEN)) {
                        // get last seen status
                        String lastSeen = response.getString(APIRequestUtility.FCM_RESPONSE_UPDATE_LAST_SEEN);
                        // get the uid of whom to update
                        long uid = Long.parseLong(response.getString(APIRequestUtility.FCM_RESPONSE_UID));
                        //Log.i(TAG, "Updating contact with id: " + uid + " status to: " + lastSeen);
                        // get contacts uri for single contacts
                        Cursor cursor = getContentResolver().query(ChitterContentProvider.CONTENT_URI_CONTACT_ITEM,
                                null, null, new String[]{String.valueOf(uid)}, null);
                        if (cursor != null) {
                            if (cursor.moveToFirst()) {
                                // get contact from cursor
                                Contact contact = new Contact(cursor);
                                contact.setLastSeen(lastSeen);
                                DBHandler.getInstance(getApplicationContext()).updateContact(contact);
                                sendLastSeenToReceiver(lastSeen, uid);
                                String currentCID = ChitterModel.getInstance(getApplicationContext()).getCurrentChat();
                                if (currentCID != null) {
                                    Uri chatMembersURI = Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_USERS_CHATS_MEMBERS, currentCID);
                                    getContentResolver().notifyChange(chatMembersURI, null);
                                }
                            }
                            cursor.close();
                        }
                    } else if (response.has(APIRequestUtility.FCM_RESPONSE_UPDATE_USER_PROFILE)) {
                        //TODO: Do update stuff to user profile
                        final String contactId = response.getString(APIRequestUtility.FCM_RESPONSE_UPDATE_USER_PROFILE);
                        final String contactName = response.getString(DBConstants.COLUMN_USER_NAME);
                        final String contactStatus = response.getString(DBConstants.COLUMN_USER_STATUS);
                        final String contactImgUrl = response.getString(DBConstants.COLUMN_USER_IMG_URL);
                        Cursor cursor = getContentResolver().query(ChitterContentProvider.CONTENT_URI_CONTACT_ITEM,
                                null, null, new String[]{contactId}, null);
                        if (cursor != null) {
                            if (cursor.moveToFirst()) {
                                final Contact contact = new Contact(cursor);
                                // only update contact name if user does not exist on device,
                                // otherwise we use name stored in device contacts
                                if (!contact.existOnDevice()) {
                                    contact.setName(contactName);
                                }
                                contact.setStatus(contactStatus);
                                contact.setImgUrl(contactImgUrl);
                                getContentResolver().update(ChitterContentProvider.CONTENT_URI_CONTACT_ITEM,
                                        contact.getContentValues(), null, new String[]{contactId});
                                Log.i(TAG, "Updated user with id: " + contact.getId() + " and name: " + contactName);
                                getApplicationContext().getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CONTACTS, null);
                                getApplicationContext().getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN, null);
                            }
                            cursor.close();
                        }
                    } else {
                        // NOTE: This stops messages duplicating when user comes back online. FCM can send duplicate messages
                        // after coming back online, as the SyncAdapter handles getting any changes we do not need FCM for this.
                        // check if message exists before handling the message (notification, insertion etc)
                        Cursor messageExistCursor = getContentResolver().query(ChitterContentProvider.CONTENT_URI_MESSAGE_ITEM,
                                null, null, new String[]{message.getId()}, null);
                        boolean messageExist = false;
                        if (messageExistCursor != null) {

                            // if no first then message doesn't exist locally
                            if (messageExistCursor.moveToFirst()) {
                                messageExist = true;
                            }
//                            Log.i(TAG, "Cursor size: " + messageExistCursor.getCount());
//                            Log.i(TAG, "Cursor moveToFirst: " + messageExistCursor.moveToFirst());
                            messageExistCursor.close();
                        }

                        // message doesn;t exist in DB go ahead and create it
                        if (!messageExist) {
                            // get the notification title of the incoming message
                            final String notifTitle = response.getString(APIRequestUtility.RESPONSE_NOTIF_TITLE);
                            IncomingMessageHelper.handleNewMessage(message, notifTitle, getApplicationContext());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Send the contacts last seen status to the receivers
     *
     * @param lastSeen new last seen
     * @param uid
     */
    private void sendLastSeenToReceiver(String lastSeen, long uid) {
        //Log.i(TAG, "Sending result back to receiver");
        // intent needs to use the same action
        Intent broadcastIntent = new Intent(ChitterFirebaseMessagingService.ACTION);
        Bundle bundle = new Bundle();
        bundle.putString(BROADCAST_STATUS, lastSeen);
        bundle.putLong(BROADCAST_UID, uid);
        broadcastIntent.putExtra(BROADCAST_BUNDLE, bundle);
        // resnt broadcast back to the local broadcast receiver in MainActivity
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }
}
