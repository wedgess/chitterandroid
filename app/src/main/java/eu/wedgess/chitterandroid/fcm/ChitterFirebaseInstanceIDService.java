package eu.wedgess.chitterandroid.fcm;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.Utils;

/**
 * Created by Gareth on 02/11/2016.
 */
public class ChitterFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = ChitterFirebaseMessagingService.class.getSimpleName();

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onTokenRefresh() {
        System.out.println("Here refresh token");
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to server
     * <p>
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(final String token) {
        // set FCM token in shared preferences first as APIRequestUtility gets token from prefs
        Utils.setStringPref(Constants.PREF_FCM_TOKEN, token, getApplicationContext());
        // if user json in preferences is not empty assume user is logged in
        User user = Utils.getUserFromPrefs(getApplicationContext());
        if (user != null) {
            // update FCM token on server, when sending messages we get users FCM toke from server
            // so no need to update on individual devices
            APIRequestUtility.updateFcmTokenOnServer(getApplicationContext());
        }
    }
}
