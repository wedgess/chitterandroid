package eu.wedgess.chitterandroid.utils;

import android.text.InputType;
import android.text.Spanned;
import android.text.method.NumberKeyListener;

/**
 * used to filter phone number, numbers must be Irish numbers.
 * <p>
 * Created by : http://stackoverflow.com/a/12629726
 * Modified by : Gareth on 23/12/2016.
 */
public class PhoneNumberFilter extends NumberKeyListener {

    @Override
    public int getInputType() {
        return InputType.TYPE_CLASS_PHONE;
    }

    @Override
    protected char[] getAcceptedChars() {
        return new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {

        // convert source to string so we can use matches method for regex
        String srcStr = source.toString();
        // Numbers must start with 08 (Irish Number) and it must match one of the carrier prefixes
        // without source.length = 1 savedInstanceState is broken on ET (cause source is never "0")
        if (dstart == 2 && !srcStr.matches("[356789]") && source.length() == 1) {
            return "";
        }

        if (end > start) {
            String destTxt = dest.toString();
            String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);

            // Phone number must match 08xxxxxxxx
            if (!resultingTxt.matches("^\\d{1,1}(\\d{1,1}(\\d{1,1}(\\d{1,1}(\\d{1,1}(\\d{1,1}(\\d{1,1}(\\d{1,1}(\\d{1,1}(\\d{1,1}?)?)?)?)?)?)?)?)?)?")) {
                return "";
            }
        }
        return null;
    }
}
