package eu.wedgess.chitterandroid.utils;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import eu.wedgess.chitterandroid.db.ChitterContentProvider;

/**
 * This class is only for Nougat and above. After some research as to why
 * {@link NetworkChangeReceiver} BroadcastReceiver wasn't working. As per Google:
 * <p>
 * Apps targeting Android 7.0 (API level 24) do not receive CONNECTIVITY_ACTION
 * broadcasts if they register to receive them in their manifest, and processes
 * that depend on this broadcast will not start.
 * <p>
 * While this method is not desired it is a workaround for now!
 * For Lollipop and Marshmallow registering the receiver in AndroidManifest works
 * this methos is only used on Nougat.
 * <p>
 * Created by Gareth on 11/02/2017.
 */

public class NetworkChangeService extends Service {
    private boolean initialized = false;

    @Override
    public void onCreate() {
        super.onCreate();
        // register receiver to listen to network changes
        registerReceiver(mNetworkBroadcastReceiver,
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // make sure we are connected to mobile or wifi network
            if (Utils.isNetworkAvailable(context)) {
                if (initialized) {
                    // Sync local and remote DB
                    ContentResolver.requestSync(Utils.createDummyAccount(getApplicationContext()),
                            ChitterContentProvider.AUTHORITY, Bundle.EMPTY);
                }
            }
            initialized = true;
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // unregister receiver on destroy
        unregisterReceiver(mNetworkBroadcastReceiver);
    }
}
