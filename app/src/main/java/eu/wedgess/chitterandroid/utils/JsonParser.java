package eu.wedgess.chitterandroid.utils;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.model.UsersChats;

import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_CREATED_AT;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_CREATED_BY;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_MEMBERS;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_NAME;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CHAT_PROFILE_IMG_URL;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_CREATED_AT;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_IMG_URL;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_LAST_SEEN;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_NAME;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_PHONE_NUMBER;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_CONTACTS_STATUS;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_CHAT_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_CREATED_AT;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_IMG_URL;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_MSG;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_SENDER_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_STATUS;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_MESSAGE_TYPE;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USERS_CHATS_CID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USERS_CHATS_UID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_CREATED_AT;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_FCM_TOKEN;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_IMG_URL;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_LAST_SEEN;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_NAME;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_PASSWORD;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_PHONE_NUM;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_STATUS;

/**
 * Used for parsing servers JSON responses.
 * {@link User}
 * {@link Contact}
 * {@link ChatRoom}
 * {@link Message}
 * <p>
 * Created by Gareth on 22/12/2016.
 */

public class JsonParser {

    private JsonParser() {
        // non instantiable
    }

    /**
     * Converts User JSON object to Java User object
     *
     * @param userJson - User JSON object to convert
     * @return - Converted User Java Object {@link User}
     * @throws JSONException
     */
    public static User jsonToUser(JSONObject userJson) throws JSONException {
        long id = userJson.getLong(COLUMN_USER_ID);
        String name = userJson.getString(COLUMN_USER_NAME);
        String phoneNumber = userJson.getString(COLUMN_USER_PHONE_NUM);
        String password = userJson.getString(COLUMN_USER_PASSWORD);
        String fcmToken = userJson.getString(COLUMN_USER_FCM_TOKEN);
        String status = userJson.getString(COLUMN_USER_STATUS);
        String imgUrl = userJson.getString(COLUMN_USER_IMG_URL);
        String createdAt = userJson.getString(COLUMN_USER_CREATED_AT);
        String lastSeen = userJson.getString(COLUMN_USER_LAST_SEEN);

        return new User(id, name, phoneNumber, password, fcmToken, imgUrl, createdAt, lastSeen, status);
    }

    /**
     * Converts a java user object to a user JSON object which is sent to server
     *
     * @param user - User Java Object to convert to JSON object
     * @return - User Json Object {@link User}
     * @throws JSONException - If problem parsing data
     */
    public static JSONObject userToJson(User user) throws JSONException {
        JSONObject userJson = new JSONObject();
        userJson.put(COLUMN_USER_ID, user.getId());
        userJson.put(COLUMN_USER_NAME, user.getName());
        userJson.put(COLUMN_USER_PHONE_NUM, user.getPhoneNumber());
        userJson.put(COLUMN_USER_PASSWORD, user.getPassword());
        userJson.put(COLUMN_USER_FCM_TOKEN, user.getFcmToken());
        userJson.put(COLUMN_USER_IMG_URL, user.getImgUrl());
        userJson.put(COLUMN_USER_CREATED_AT, user.getCreatedAt());
        userJson.put(COLUMN_USER_LAST_SEEN, user.getLastSeen());
        userJson.put(COLUMN_USER_STATUS, user.getStatus());

        return userJson;
    }

    /**
     * Converts User JSON object to Java User object
     *
     * @param contactJson - User JSON object to convert
     * @return - Converted User Java Object
     * @throws JSONException
     */
    public static Contact jsonToContact(JSONObject contactJson) throws JSONException {
        long id = contactJson.getLong(COLUMN_USER_ID);
        String name = contactJson.getString(COLUMN_CONTACTS_NAME);
        String phoneNumber = contactJson.getString(COLUMN_CONTACTS_PHONE_NUMBER);
        String imgUrl = contactJson.getString(COLUMN_CONTACTS_IMG_URL);
        String createdAt = contactJson.getString(COLUMN_CONTACTS_CREATED_AT);
        String lastSeen = contactJson.getString(COLUMN_CONTACTS_LAST_SEEN);
        String status = contactJson.getString(COLUMN_CONTACTS_STATUS);

        return new Contact(id, name, phoneNumber, imgUrl, createdAt, lastSeen, status);
    }

    /**
     * Converts JSON message to Message POJO
     *
     * @param msgJson - Message JSON
     * @return {@link Message}
     * @throws JSONException
     */
    public static Message jsonToMessage(JSONObject msgJson) throws JSONException {
        String mid = msgJson.getString(COLUMN_MESSAGE_ID);
        String cid = msgJson.getString(COLUMN_MESSAGE_CHAT_ID);
        long senderId = msgJson.getLong(COLUMN_MESSAGE_SENDER_ID);
        String createdAt = msgJson.getString(COLUMN_MESSAGE_CREATED_AT);
        String message = msgJson.getString(COLUMN_MESSAGE_MSG);
        String imgUrl = msgJson.getString(COLUMN_MESSAGE_IMG_URL);
        int status = msgJson.getInt(COLUMN_MESSAGE_STATUS);
        int type = msgJson.getInt(COLUMN_MESSAGE_TYPE);
        Message message1 = new Message(mid, cid, senderId, message, createdAt, status, type);
        message1.setImgUrl(imgUrl);
        return message1;
    }

    /**
     * Converts Message POJO to Message JSON object
     *
     * @param message {@link Message}
     * @return
     * @throws JSONException
     */
    public static JSONObject messageToJson(Message message) throws JSONException {
        JSONObject msgJson = new JSONObject();
        // TODO: 12/01/2017 need to add in IMG_URL and add getter/setter and to constructor of message class
        // TODO: If implementing sending images & videos in chats
        msgJson.put(COLUMN_MESSAGE_ID, message.getId());
        msgJson.put(COLUMN_MESSAGE_CHAT_ID, message.getCid());
        msgJson.put(COLUMN_MESSAGE_SENDER_ID, message.getSenderId());
        msgJson.put(COLUMN_MESSAGE_MSG, message.getMessage());
        msgJson.put(COLUMN_MESSAGE_STATUS, message.getStatus());
        msgJson.put(COLUMN_MESSAGE_IMG_URL, message.getImgUrl());
        msgJson.put(COLUMN_MESSAGE_TYPE, message.getType());
        msgJson.put(COLUMN_MESSAGE_CREATED_AT, message.getCreatedAt());
        return msgJson;
    }

    /**
     * Converts a java chat object to a chat JSON object which is sent to server
     *
     * @param chatRoom - User Java Object to convert to JSON object
     * @return - Chat Json Object
     * @throws JSONException - If problem parsing data
     */
    public static JSONObject chatToJson(ChatRoom chatRoom) throws JSONException {
        long[] memberIds = new long[chatRoom.getMembers().size()];
        for (int i = 0; i < chatRoom.getMembers().size(); i++) {
            memberIds[i] = chatRoom.getMembers().get(i).getId();
        }
        JSONObject chatJson = new JSONObject();
        chatJson.put(COLUMN_CHAT_ID, chatRoom.getId());
        chatJson.put(COLUMN_CHAT_NAME, chatRoom.getTitle());
        chatJson.put(COLUMN_CHAT_MEMBERS, new JSONArray(memberIds));
        //TODO: Add image URL to ChatRoom and here
        chatJson.put(COLUMN_CHAT_PROFILE_IMG_URL, chatRoom.getGroupProfileImgUrl());
        chatJson.put(COLUMN_CHAT_CREATED_BY, chatRoom.getAdminId());
        return chatJson;
    }

    /**
     * Converts ChatRoom JSON Object to ChatRoom POJO
     * Used by {@link APIRequestUtility}
     *
     * @param jsonObject
     * @param context
     * @return {@link ChatRoom}
     */
    public static ChatRoom chatJsonToChat(JSONObject jsonObject, Context context) {
        try {
            ChitterModel model = ChitterModel.getInstance(context);
            String cid = jsonObject.getString(COLUMN_CHAT_ID);
            String name = jsonObject.getString(COLUMN_CHAT_NAME);
            String imgUrl = jsonObject.getString(COLUMN_CHAT_PROFILE_IMG_URL);
            long adminId = jsonObject.getLong(COLUMN_CHAT_CREATED_BY);
            String createdAt = jsonObject.getString(COLUMN_CHAT_CREATED_AT);
            ArrayList<Contact> members = new ArrayList<>();
            if (jsonObject.has(COLUMN_CHAT_MEMBERS)) {
                JSONArray membersArray = jsonObject.getJSONArray(COLUMN_CHAT_MEMBERS);
                for (int i = 0; i < membersArray.length(); i++) {
                    long memberId = membersArray.getLong(i);
                    Contact member = DBHandler.getInstance(context).getContactById(memberId);
                    if (member == null) {
                        if (model.userIsOwner(memberId, context)) {
                            members.add(model.getUser(context));
                        }
                    } else {
                        members.add(member);
                    }
                }
            }
            return new ChatRoom(cid, name, new ArrayList<Message>(), members, imgUrl, adminId, ChatRoom.USER_TO_USER_CHAT, createdAt);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Converts users_chats JSON to {@link UsersChats}
     *
     * @param usersChatsJson
     * @return {@link UsersChats}
     */
    public static UsersChats jsonToUsersChats(JSONObject usersChatsJson) {
        try {
            long uid = usersChatsJson.getLong(COLUMN_USERS_CHATS_UID);
            String cid = usersChatsJson.getString(COLUMN_USERS_CHATS_CID);
            return new UsersChats(uid, cid);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
