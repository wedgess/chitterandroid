package eu.wedgess.chitterandroid.utils;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.wedgess.chitterandroid.adapters.SyncAdapter;
import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.fragments.RegisterPersonalInfoFragment;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.model.UsersChats;

import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_FCM_TOKEN;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_ID;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_LAST_SEEN;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_PASSWORD;
import static eu.wedgess.chitterandroid.db.DBConstants.COLUMN_USER_PHONE_NUM;
import static eu.wedgess.chitterandroid.helpers.Constants.PREF_FCM_TOKEN;

/**
 * A utility class to Connect with the REST API.
 * All request methods are contained within this class.
 * <p>
 * Created by Gareth on 24/01/2017.
 */

public class APIRequestUtility {

    public static final String TAG = APIRequestUtility.class.getSimpleName();

    // Server JSON response keys
    private static final String RESPONSE_ERROR = "error";
    private static final String RESPONSE_MESSAGE = "message";
    private static final String RESPONSE_MESSAGES = "messages";
    private static final String RESPONSE_USER = "user";
    private static final String RESPONSE_CHAT = "chat";
    public static final String RESPONSE_NOTIF_TITLE = "title";

    public static final String FCM_RESPONSE_MSG = "message_response";
    public static final String FCM_RESPONSE_MSG_STATUS = "message_status";
    public static final String FCM_RESPONSE_UPDATE_LAST_SEEN = "update_user_last_seen";
    public static final String FCM_RESPONSE_UPDATE_USER_PROFILE = "update_user_profile";
    public static final String FCM_RESPONSE_UID = "uid";

    public static final String SERVER_UPLOADS_PATH = "media/uploads/";

    // Server login response error messages
    public static final String RESPONSE_INCORRECT_PASSWORD = "Incorrect password!";
    public static final String RESPONSE_NOT_REGISTERED = "Phone Number not registered!";

    // End points and base URL
    public static final String BASE_URL = "https://www.garethwilliams.host/chitterPHPBackend/";
    //public static final String BASE_URL = "http://192.168.1.12/chitterPHPBackend/";
    private static final String ENDPOINT_REGISTER = "user/register";
    private static final String ENDPOINT_LOGIN = "user/login";
    private static final String ENDPOINT_CONTACTS = "user/contacts";
    private static final String ENDPOINT_LAST_SEEN = "user/last_seen";
    private static final String ENDPOINT_UPDATE_FCM_TOKEN = "user/fcm_token";
    // phone_number is param
    private static final String ENDPOINT_GET_USER_EXIST = "user/exist/";
    // /user/info/{uid}
    private static final String ENDPOINT_GET_USER_BY_ID = "user/info/";
    private static final String ENDPOINT_UPDATE_USER_INFO = "user/public/info";
    // /user/messages/{uid}/{last_sync}
    private static final String ENDPOINT_GET_MSGS_SINCE_LAST_SYNC = "user/messages/";
    // /user/chats/{uid}
    private static final String ENDPOINT_GET_USERS_CHATS = "user/chats/";
    private static final String ENDPOINT_CREATE_MESSAGE = "message/create";
    private static final String ENDPOINT_CREATE_CHAT = "chats/create";
    // /message/status/{mid}/{status}
    private static final String ENDPOINT_MSG_STATUS = "message/status/";
    public static final String ENDPOINT_UPLOAD_MEDIA = "media/upload";
    // param cid
    private static final String ENDPOINT_GET_CHAT_INFO = "chats/";

    private APIRequestUtility() {
        // non instantiable
    }


    public interface CommonServerCallback {
        void onComplete(boolean error);

        void onError(String error);
    }

    public interface CommonListServerCallback {
        void onComplete(List items);
    }

    public interface GetContactCallback {
        void onComplete(Contact contact);
    }

    public interface ChatCreatedServerCallback {
        void onComplete(boolean error, ChatRoom chatRoom);
    }

    /**
     * Checks the users contacts on device to see which have registered for the application,
     * if a user already exists on the local DB check if FCM or last seen needs updating from
     * servers DB.
     *
     * @param contacts - list of contacts on users device
     * @param context
     */
    public static void checkContacts(final List<Contact> contacts, final Context context) {
        StringRequest contactsRequest = new StringRequest(Request.Method.POST, BASE_URL + ENDPOINT_CONTACTS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(final String response) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Log.i(TAG, "Response Contacts HERE: " + response);
                                    // server sends back JSONArray of contacts, so parse it
                                    JSONArray contacts = new JSONArray(response);
                                    int size = contacts.length();
                                    for (int i = 0; i < size; i++) {
                                        // get contact JSON from array
                                        JSONObject contactJson = contacts.getJSONObject(i);
                                        // convert contact JSOn to POJO
                                        Contact contact = JsonParser.jsonToContact(contactJson);
                                        if (contact != null) {
                                            String name = contact.getName();
                                            if (!TextUtils.isEmpty(name)) {
                                                // replace all underlines
                                                contact.setName(name.replaceAll("_", " "));
                                            }
                                            contact.setExistOnDevice(true);
                                            // add to model and DB only if not already exist, otherwise check for updating FCM or last seen
                                            DBHandler.getInstance(context).addUserToContacts(contact, context.getApplicationContext());
                                        }
                                    }
                                } catch (JSONException e) {
                                    // as parse error from the server
                                    Log.e(TAG, "Error parsing response from server: " + e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        }).start();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());

            }
        }) {
            protected Map<String, String> getParams() {
                // Data which will be sent to server as body parameters
                Map<String, String> contactRequestParams = new HashMap<String, String>();
                //int counter = 0;
                final String myNumber = ChitterModel.getInstance(context).getUser(context).getPhoneNumber();
                //contacts can be null when a user doesn't have any on their device????
                if (contacts != null) {
                    for (Contact contact : contacts) {
                        if (!TextUtils.isEmpty(contact.getPhoneNumber())) {
                            // convert numbers to format 0XXXXXXXXX Irish mobile numbers
                            // replace 00 353 00353 or +353 from phone number which is irish country codes
                            String cleanedNumber = contact.getPhoneNumber().replaceAll(" ", "") // remove all spaces
                                    .replaceAll("[\\(\\)\\s-]", "") // remove and place with brackets numbers can be (087) 1234
                                    .replaceAll("(^0*\\s?\\+?353)", "0"); // replace 0 or country code 00 353 +353 etc and replace with leading 0
                            // in case a user has their own number in contacts don't add their number to contacts
                            if (!cleanedNumber.equals(myNumber)) {
                                //Log.i(TAG, "Adding: " + cleaned);
                                contactRequestParams.put(COLUMN_USER_PHONE_NUM + "_" + contact.getName(), cleanedNumber);
                            }
                        }
                    }
                }
                return contactRequestParams;
            }
        };
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context.getApplicationContext()).addToRequestQueue(contactsRequest, context);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public static void updateUserProfile(final long uid, final String username, final String userStatus,
                                         final String userImgUrl, final Context context) {
        Log.i(TAG, "In updateUserProfile");
        new AsyncQueryHandler(context.getContentResolver()) {
            @Override
            protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
                super.onQueryComplete(token, cookie, cursor);
                final List<Contact> contactsList = new ArrayList<Contact>();
                if (cursor != null) {
                    while (cursor.moveToNext()) {
                        contactsList.add(new Contact(cursor));
                    }
                    cursor.close();

                    StringRequest contactsRequest = new StringRequest(Request.Method.PUT, BASE_URL + ENDPOINT_UPDATE_USER_INFO,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(final String response) {
                                    Log.i(TAG, "Update user profile Response: " + response);
                                    JSONObject responseJson = null;
                                    try {
                                        responseJson = new JSONObject(response);

                                        if (responseJson != null) {
                                            Log.d(TAG, responseJson.toString());
                                            // error flag within the response body
                                            boolean error = responseJson.getBoolean(RESPONSE_ERROR);
                                            Log.i(TAG, "Result has error: " + String.valueOf(error));
                                            if (error) {
                                                // if error set the error message on the TIL
                                                String responseMessage = responseJson.getString(RESPONSE_MESSAGE);
                                                Log.i(TAG, "Error message: " + responseMessage);
                                                //loginServerCallback.onError(responseMessage);
                                            } else {
                                                // successful result from server parse the user object
                                                JSONObject userJson = responseJson.getJSONObject(RESPONSE_USER);
                                                if (userJson != null) {
                                                    // update current user in preferences
                                                    Utils.setStringPref(Constants.PREF_USER_JSON, userJson.toString(), context);
                                                    Log.i(TAG, "user updated in prefs: " + userJson.toString());
                                                }
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e(TAG, "Error Response: " + error.getMessage());

                        }
                    }) {
                        protected Map<String, String> getParams() {
                            // Data which will be sent to server as body parameters
                            Map<String, String> contactRequestParams = new HashMap<String, String>();
                            contactRequestParams.put(DBConstants.COLUMN_USER_ID, String.valueOf(uid));
                            contactRequestParams.put(DBConstants.COLUMN_USER_NAME, username);
                            contactRequestParams.put(DBConstants.COLUMN_USER_STATUS, userStatus);
                            contactRequestParams.put(DBConstants.COLUMN_USER_IMG_URL, userImgUrl);

                            for (Contact contact : contactsList) {
                                contactRequestParams.put(contact.getName(), String.valueOf(contact.getId()));
                            }
                            return contactRequestParams;
                        }
                    };
                    // Access the RequestQueue through your singleton class.
                    ChitterModel.getInstance(context.getApplicationContext()).addToRequestQueue(contactsRequest, context);
                }
            }
        }.startQuery(121, null, ChitterContentProvider.CONTENT_URI_CONTACTS, null, null, null, null);
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////


    /**
     * User chat doesn;t exist in local DB so get the chat info from the servers DB,
     * this returns the chat and each member within the chat within an array.
     * This will create the chat in local DBs chat table and users_chats table on local DB.
     *
     * @param chatId  - chat id to check if exists
     * @param context
     */
    public static synchronized void createLocalChatFromRemote(final String chatId, final Context context, final ChatCreatedServerCallback serverCallback) {
        StringRequest getChatInfoRequest = new StringRequest(Request.Method.GET, BASE_URL + ENDPOINT_GET_CHAT_INFO + chatId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "Create local chat from remote response: " + response);
                            // server sends back JSON, so parse it
                            JSONObject responseJson = new JSONObject(response);
                            // error flag within the response body
                            final boolean error = responseJson.getBoolean(RESPONSE_ERROR);
                            String responseMessage = responseJson.getString(RESPONSE_MESSAGE);
                            if (error) {
                                Log.i(TAG, "Error message: " + responseMessage);
                                if (serverCallback != null) {
                                    serverCallback.onComplete(true, null);
                                }
                            } else {
                                Log.i(TAG, "Message: " + responseMessage);
                                // the response will be rows of same chat id with users in that chat
                                JSONArray chatArray = new JSONArray(responseMessage);
                                ChatRoom chatRoom = null;
                                int[] uids = new int[chatArray.length()];
                                boolean allUsersExistInContacts = true;
                                for (int i = 0; i < chatArray.length(); i++) {
                                    // get chat at pos i
                                    JSONObject chatInfo = chatArray.getJSONObject(i);
                                    if (chatRoom == null) {
                                        // only if chatRoom hasn't already been parsed (first time)
                                        chatRoom = JsonParser.chatJsonToChat(chatInfo, context);
                                    }
                                    // add the uid to uids array
                                    uids[i] = chatInfo.getInt(DBConstants.COLUMN_USERS_CHATS_UID);

                                    // if the UID is not this user
                                    if (!ChitterModel.getInstance(context).userIsOwner(uids[i], context)) {
                                        // get the contact from the DB
                                        Cursor cursor = context.getContentResolver().query(ChitterContentProvider.CONTENT_URI_CONTACT_ITEM,
                                                null, null, new String[]{String.valueOf(uids[i])}, null);
                                        if (cursor != null) {
                                            // if nothing is returned for the contact then they don't exist
                                            if (!cursor.moveToFirst()) {
                                                allUsersExistInContacts = false;
                                                Log.i(TAG, "User with id: " + uids[i] + " does not exist in contacts");
                                                // needed to access chatroom from within callback below
                                                final ChatRoom finalChatRoom = chatRoom;
                                                // get the contact from the server
                                                APIRequestUtility.getUserById(String.valueOf(uids[i]), context, new GetContactCallback() {
                                                    @Override
                                                    public void onComplete(Contact contact) {
                                                        // add the user to contacts
                                                        DBHandler.getInstance(context).addUserToContacts(contact, context);
                                                        // if group chat this is still ok, for user to user chat though this is needed
                                                        // to avoid race condition, where notification title would be user id
                                                        // or the chat wouldn;t show in main message screen due to user not being created
                                                        // in local DB and it is needed when using query to show chats
                                                        if (serverCallback != null) {
                                                            serverCallback.onComplete(error, finalChatRoom);
                                                        }
                                                    }
                                                });
                                            }
                                            cursor.close();
                                        }
                                    }
                                }
                                if (chatRoom != null) {
                                    // if uid length is greater than two its a group chat, otherwise user to user chat
                                    if (uids.length > 2) {
                                        chatRoom.setChatType(ChatRoom.GROUP_CHAT);
                                    } else {
                                        chatRoom.setChatType(ChatRoom.USER_TO_USER_CHAT);
                                    }
                                    // set the unread count as one as the user won;t have read it on ChatHomeScreen if
                                    // it has just been created.
                                    DBHandler.getInstance(context.getApplicationContext()).createChat(chatRoom, uids);
                                } else {
                                    Log.e(TAG, "ChatRoom is null something went wrong");
                                }
                                // don;t need to create contacts in local DB as they already exist, so send callback
                                if (allUsersExistInContacts) {
                                    if (serverCallback != null) {
                                        serverCallback.onComplete(error, chatRoom);
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            // as parse error from the server
                            Log.e(TAG, "Error parsing response from server: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());

            }
        });
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context.getApplicationContext()).addToRequestQueue(getChatInfoRequest, context);
    }

    /**
     * Update the menu_chat_home chat users FCM token on server, this usually changes if the user logs in from a new
     * device or for some other reason the FCM ID has changed.
     *
     * @param context
     */
    public static void updateFcmTokenOnServer(final Context context) {
        StringRequest updateFcmTokenRequest = new StringRequest(Request.Method.PUT, BASE_URL + ENDPOINT_UPDATE_FCM_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "FCM updated response: " + response);
                        try {
                            // server sends back JSON, so parse it
                            JSONObject responseJson = new JSONObject(response);
                            // error flag within the response body
                            boolean error = responseJson.getBoolean(RESPONSE_ERROR);
                            String responseMessage = responseJson.getString(RESPONSE_MESSAGE);
                            if (error) {
                                Log.i(TAG, "Error message: " + responseMessage);
                                // if error set the error message on the TIL
                            } else {
                                Log.i(TAG, "Message: " + responseMessage);
                                // no error then get the models user
                                ChitterModel model = ChitterModel.getInstance(context);
                                User user = model.getUser(context);
                                // get updated token from prefs and set as users token
                                String updatedToken = Utils.getStringPref(PREF_FCM_TOKEN, user.getFcmToken(), context);
                                user.setFcmToken(updatedToken);
                                // update models user and in DB
                                model.updateUser(user, context);
                            }
                        } catch (JSONException e) {
                            // as parse error from the server
                            Log.e(TAG, "Error parsing response from server: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());

            }
        }) {
            protected Map<String, String> getParams() {
                // Data which will be sent to server as body parameters
                Map<String, String> bodyParams = new HashMap<String, String>();
                bodyParams.put(COLUMN_USER_ID, String.valueOf(ChitterModel.getInstance(context).getUser(context).getId()));
                bodyParams.put(COLUMN_USER_ID, String.valueOf(ChitterModel.getInstance(context).getUser(context).getId()));
                bodyParams.put(COLUMN_USER_FCM_TOKEN, Utils.getStringPref(PREF_FCM_TOKEN, "", context.getApplicationContext()));
                return bodyParams;
            }
        };
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context.getApplicationContext()).addToRequestQueue(updateFcmTokenRequest, context);
    }

    /**
     * Creates a new chatroom on the server
     *
     * @param chatRoom
     * @param context
     */
    public static void createChat(final ChatRoom chatRoom, final Context context, final CommonServerCallback serverCallback) {
        JSONObject chatJson = null;
        try {
            chatJson = JsonParser.chatToJson(chatRoom);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (chatJson != null) {
            JsonObjectRequest createChatRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ENDPOINT_CREATE_CHAT, chatJson, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject responseJSON) {
                    // server sends back JSON, so parse it
                    Log.i(TAG, "Create chat Response: " + responseJSON.toString());
                    try {

                        final boolean error = responseJSON.getBoolean(RESPONSE_ERROR);

                        if (error) {
                            Log.e(TAG, responseJSON.getString(RESPONSE_MESSAGE));
                        } else {
                            JSONObject chat = responseJSON.getJSONObject(RESPONSE_CHAT);
                            final ChatRoom chatPojo = JsonParser.chatJsonToChat(chat, context);
                            if (chatPojo != null) {
                                Log.d(TAG, "Creating chat");
                                // set the chat type from the clients chatRoom object as the server doesn't store this data
                                chatPojo.setChatType(chatRoom.getChatType());
                                // create chat room on DB locally and users_chats
                                InsertChatQueryHandler insertChatQueryHandler = new InsertChatQueryHandler(context.getContentResolver(), new InsertChatQueryHandler.OnCompleteInsertListener() {
                                    @Override
                                    public void onComplete() {
                                        InsertChatQueryHandler insertUserChatsQuery = new InsertChatQueryHandler(context.getContentResolver(), null);
                                        for (Contact contact : chatPojo.getMembers()) {
                                            // add the users and chat id to users_chats table
                                            ContentValues cv = new ContentValues(2);
                                            cv.put(DBConstants.COLUMN_USERS_CHATS_UID, contact.getId());
                                            cv.put(DBConstants.COLUMN_USERS_CHATS_CID, chatPojo.getId());
                                            insertUserChatsQuery.startInsert(3, null, ChitterContentProvider.CONTENT_URI_USERS_CHATS, cv);
                                        }
                                        // set the current chat so we don;t get notifications for this chat
                                        ChitterModel.getInstance(context.getApplicationContext()).setCurrentChat(chatPojo.getId());
                                        context.getContentResolver().notifyChange(
                                                Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_USERS_CHATS_MEMBERS, chatPojo.getId()), null);
                                    }
                                });
                                insertChatQueryHandler.startInsert(5, null, ChitterContentProvider.CONTENT_URI_CHATS, chatPojo.getContentValues());
                            }
                        }
                        if (serverCallback != null) {
                            serverCallback.onComplete(error);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            // Access the RequestQueue through your singleton class.
            ChitterModel.getInstance(context.getApplicationContext()).addToRequestQueue(createChatRequest, context);
        }
    }

    /**
     * Creates a new message on the server, server then handles sending the msg to FCM server as well
     * as storing in Servers DB, this also updates the message in local DB, local DB saves message
     * when send button is pressed.
     *
     * @param message
     * @param context
     */
    public static void sendMessageRequest(final Message message, final Context context) {
        JSONObject messageJson = null;
        try {
            messageJson = JsonParser.messageToJson(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest sendMessageRequest = new JsonObjectRequest(Request.Method.POST,
                BASE_URL + ENDPOINT_CREATE_MESSAGE,
                messageJson, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i(TAG, "Response message: " + response.toString());
                try {
                    // error flag within the response body
                    boolean error = response.getBoolean(RESPONSE_ERROR);
                    if (error) {
                        String responseMessage = response.getString(RESPONSE_MESSAGE);
                    } else {
                        // get response message object
                        JSONObject responseMessage = response.getJSONObject(RESPONSE_MESSAGE);
                        // parse the response message to POJO
                        Message messagePojo = JsonParser.jsonToMessage(responseMessage);
                        // update message in DB
                        DBHandler.getInstance(context).updateMessage(messagePojo);
                    }
                } catch (JSONException e) {
                    // as parse error from the server
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());
            }
        });
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context).addToRequestQueue(sendMessageRequest, context);
    }


    /**
     * Send a POST request to the server to log the user in and then we get the user object as
     * a response from the server if the user is logged in, this user object is then parsed and
     * set as the {@link ChitterModel#mUser} object.
     * <p>
     * If logging in fails then the appropriate response will be received from the server on of:
     * {@link #RESPONSE_INCORRECT_PASSWORD}
     * OR
     * {@link #RESPONSE_NOT_REGISTERED}
     */
    public static void logUserIn(final String phoneNumber, final String password, final Context context,
                                 final CommonServerCallback loginServerCallback) {
        StringRequest loginPostRequest = new StringRequest(Request.Method.POST, BASE_URL + ENDPOINT_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "Log user in Response: " + response);
                            // server sends back JSON, so parse it
                            JSONObject responseJson = new JSONObject(response);
                            Log.d(TAG, responseJson.toString());
                            // error flag within the response body
                            boolean error = responseJson.getBoolean(RESPONSE_ERROR);
                            Log.i(TAG, "Result has error: " + String.valueOf(error));
                            if (error) {
                                // if error set the error message on the TIL
                                String responseMessage = responseJson.getString(RESPONSE_MESSAGE);
                                Log.i(TAG, "Error message: " + responseMessage);
                                loginServerCallback.onError(responseMessage);
                            } else {
                                // successful result from server parse the user object
                                JSONObject userJson = responseJson.getJSONObject(RESPONSE_USER);
                                if (userJson != null) {
                                    // parse to JSON object
                                    User user = JsonParser.jsonToUser(userJson);
                                    User savedUser = Utils.getUserFromPrefs(context);
                                    // if a user exists in DB
                                    if (savedUser != null && !user.equals(savedUser)) {
                                        Log.i(TAG, "User exists in DB but is not equal, updating...");
                                        // user exists but not equal, update local DB user
                                        ChitterModel.getInstance(context).updateUser(user, context);
                                    } else {
                                        Log.i(TAG, "User not in DB adding user to local DB");
                                        // user not in DB so add the user to DB
                                        ChitterModel.getInstance(context).updateUser(user, context);
                                    }
                                    loginServerCallback.onComplete(false);
                                }
                            }
                        } catch (JSONException e) {
                            // as parse error from the server
                            Log.e(TAG, "Error parsing response from server: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                loginServerCallback.onError(error.getMessage());
                Log.e(TAG, "Error Response: " + error.getMessage());

            }
        }) {
            protected Map<String, String> getParams() {
                // Data which will be sent to server as body parameters
                Map<String, String> bodyParams = new HashMap<String, String>();
                bodyParams.put(COLUMN_USER_PHONE_NUM, phoneNumber);
                // generateNotificationID it to see if it matches the one on the server for the user
                bodyParams.put(COLUMN_USER_PASSWORD, Utils.generateMD5(password));
                return bodyParams;
            }
        };
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context.getApplicationContext()).addToRequestQueue(loginPostRequest, context);
    }

    /**
     * Send a request to the server to see if the phone number is valid and that it has not
     * already been registered. If not already registered the user is brought to the next stage in
     * registering {@link RegisterPersonalInfoFragment}. Otherwise the appropriate error messages
     * are displayed.
     */
    public static void checkIfNumberIsRegistered(final String phoneNumber, final Context context,
                                                 final CommonServerCallback commonServerCallback) {
        String requestUrl = BASE_URL + ENDPOINT_GET_USER_EXIST + phoneNumber;

        StringRequest getUserExistRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Check if number is registered Response: " + response);
                        String strippedResponse = response.replaceAll("\n", "").trim();
                        Boolean userExists = Boolean.valueOf(strippedResponse);
                        Log.i(TAG, "User exists: " + String.valueOf(userExists));
                        commonServerCallback.onComplete(userExists);

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                commonServerCallback.onError(error.getMessage());
                Log.e(TAG, "Error Response: " + error.getMessage());
            }
        });
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context).addToRequestQueue(getUserExistRequest, context);
    }

    public static void getUserById(final String uid, final Context context,
                                   final GetContactCallback commonServerCallback) {
        String requestUrl = BASE_URL + ENDPOINT_GET_USER_BY_ID + uid;

        StringRequest getUserByIdRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Get user By ID Response: " + response);
                        try {
                            JSONObject responseJson = new JSONObject(response);
                            boolean error = false;
                            if (responseJson.has(RESPONSE_ERROR)) {
                                error = responseJson.getBoolean(RESPONSE_ERROR);
                            }
                            if (!error) {
                                JSONObject contactJSON = responseJson.getJSONObject(RESPONSE_USER);
                                Contact contact = JsonParser.jsonToContact(contactJSON);
                                commonServerCallback.onComplete(contact);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());
            }
        });
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context).addToRequestQueue(getUserByIdRequest, context);
    }

    /**
     * Gets a list of the chats that the user is in from the server.
     *
     * @param uid                      - uid of user to get chats
     * @param context
     * @param commonListServerCallback - callback for when request is complete
     */
    public static void getUsersChats(final long uid, final Context context, final CommonListServerCallback commonListServerCallback) {
        String requestUrl = BASE_URL + ENDPOINT_GET_USERS_CHATS + String.valueOf(uid);

        StringRequest getUserByIdRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Get Users Chats Response: " + response);
                        try {
                            JSONObject responseJson = new JSONObject(response);
                            boolean error = false;
                            if (responseJson.has(RESPONSE_ERROR)) {
                                error = responseJson.getBoolean(RESPONSE_ERROR);
                            }
                            if (!error) {
                                List<UsersChats> serverUsersChats = new ArrayList<>();
                                JSONArray users_chats = responseJson.getJSONArray("users_chats");
                                int size = users_chats.length();
                                for (int i = 0; i < size; i++) {
                                    // get contact JSON from array
                                    JSONObject usersChatsJson = users_chats.getJSONObject(i);
                                    // convert contact JSOn to POJO
                                    UsersChats usersChats = JsonParser.jsonToUsersChats(usersChatsJson);
                                    if (usersChats != null) {
                                        serverUsersChats.add(usersChats);
                                    }
                                }
                                commonListServerCallback.onComplete(serverUsersChats);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());
            }
        });
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context).addToRequestQueue(getUserByIdRequest, context);
    }

    /**
     * This method get called for {@link SyncAdapter}
     * to get all the messages since the date of the last received message on the users device.
     *
     * @param uid                      - uid of this user
     * @param lastReceivedMsgDate      - date of last received message from local DB
     * @param context
     * @param commonListServerCallback - callback for when request is complete
     */
    public static void getMessagesSinceLastMessageRecieved(final long uid, final String lastReceivedMsgDate,
                                                           final Context context, final CommonListServerCallback commonListServerCallback) {
        // build the url
        String requestUrl = BASE_URL + ENDPOINT_GET_MSGS_SINCE_LAST_SYNC + String.valueOf(uid) + "/" + lastReceivedMsgDate.replace(" ", "+");

        Log.i(TAG, "Sending request to: " + requestUrl);
        StringRequest getUserByIdRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Get Messages since last sync Response: " + response);
                        try {
                            JSONObject responseJson = new JSONObject(response);
                            boolean error = false;
                            if (responseJson.has(RESPONSE_ERROR)) {
                                error = responseJson.getBoolean(RESPONSE_ERROR);
                            }
                            if (!error) {
                                List<Message> serverMessages = new ArrayList<>();
                                JSONArray messages = responseJson.getJSONArray(RESPONSE_MESSAGES);
                                int size = messages.length();
                                for (int i = 0; i < size; i++) {
                                    // get message JSON from array
                                    JSONObject messageJson = messages.getJSONObject(i);
                                    // convert contact JSOn to POJO
                                    Message message = JsonParser.jsonToMessage(messageJson);
                                    if (message != null) {
                                        serverMessages.add(message);
                                    }
                                }
                                commonListServerCallback.onComplete(serverMessages);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());
            }
        });
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context).addToRequestQueue(getUserByIdRequest, context);
    }

    /**
     * Update message status on server, we don;t need to do anyhting with the reposne as it has already been updated locally.
     * TODO: Could change this so only updated when response is received.
     *
     * @param message
     * @param status
     * @param context
     */
    public static void updateMessageStatus(final Message message, final int status, final Context context) {
        String requestUrl = BASE_URL + ENDPOINT_MSG_STATUS + message.getId() + "/" + status;

        StringRequest getUserByIdRequest = new StringRequest(Request.Method.GET, requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.i(TAG, "Update message status Response: " + response);
                        try {
                            JSONObject responseJson = new JSONObject(response);
                            boolean error = false;
                            if (responseJson.has(RESPONSE_ERROR)) {
                                error = responseJson.getBoolean(RESPONSE_ERROR);
                            }
                            // no error in response so update message locally in DB
                            if (!error) {
                                message.setStatus(status);
                                DBHandler.getInstance(context).updateMessage(message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());
            }
        });
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context).addToRequestQueue(getUserByIdRequest, context);
    }

    /**
     * Send a POST request to server as a JSON object of the user.
     * The response receives back JSON which is parsed to check for an error and if no error receives
     * a USer object which is saved within the ChitterModel.
     */
    public static void registerUser(User user, final Context context, final CommonServerCallback commonServerCallback) {
        JSONObject userJson = null;
        try {
            // set empty created at, otherwise wont be found when parsing
            user.setCreatedAt(Utils.getSQLTimestampFormat().format(new Date()));
            user.setStatus(user.getStatus());
            userJson = JsonParser.userToJson(user);
            Log.i(TAG, "JSON: " + userJson.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (userJson != null) {
            JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL + ENDPOINT_REGISTER, userJson,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            Log.i(TAG, "Register User Response: " + response.toString());
                            try {
                                // check the response for an error
                                boolean error = response.getBoolean(RESPONSE_ERROR);
                                if (error) {
                                    // if an error there will be a message
                                    String message = response.getString(RESPONSE_MESSAGE);
                                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                                } else {
                                    // if no error we will have a user object within the response
                                    JSONObject userJson = response.getJSONObject(RESPONSE_USER);
                                    User user = JsonParser.jsonToUser(userJson);
                                    if (user != null) {
                                        Log.i(TAG, "User response: " + user.toString());
                                        // set the returned user as the current user in the Model
                                        ChitterModel.getInstance(context).updateUser(user, context);
                                    }
                                }
                                commonServerCallback.onComplete(error);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "Error Response: " + error.getMessage());
                }
            });
            // Access the RequestQueue through your singleton class.
            ChitterModel.getInstance(context).addToRequestQueue(postRequest, context);
        }
    }

    /**
     * Update users last seen date on server DB
     *
     * @param lastSeen
     * @param context
     */
    public static void updateLastSeenRequest(final String lastSeen, final Context context) {
        StringRequest contactsRequest = new StringRequest(Request.Method.PUT, BASE_URL + ENDPOINT_LAST_SEEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            Log.i(TAG, "Update user last seen Response: " + response);
                            //server sends back JSON, so parse it
                            JSONObject responseJson = new JSONObject(response);
                            Log.d(TAG, responseJson.toString());
                            //error flag within the response body
                            boolean error = responseJson.getBoolean(RESPONSE_ERROR);
                            Log.i(TAG, "Result has error: " + String.valueOf(error));
                            if (error) {
                                Log.i(TAG, "Response ERROR Message: " + responseJson.getString(RESPONSE_MESSAGE));
                            } else {
                                // get user JSON from response and save it to model and prefs
                                JSONObject userResponse = responseJson.getJSONObject(RESPONSE_USER);
                                if (userResponse.getBoolean(RESPONSE_ERROR)) {
                                    JSONObject userJson = userResponse.getJSONObject(RESPONSE_USER);
                                    User user = JsonParser.jsonToUser(userJson);
                                    ChitterModel.getInstance(context).setUser(user);
                                    Utils.setStringPref(Constants.PREF_USER_JSON, userJson.toString(), context);
                                }
                            }
                        } catch (JSONException e) {
                            // as parse error from the server
                            Log.e(TAG, "Error parsing response from server: " + e.getMessage());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error Response: " + error.getMessage());
            }
        }) {
            protected Map<String, String> getParams() {
                // Data which will be sent to server as body parameters
                Map<String, String> requestBodyMap = new HashMap<String, String>();
                requestBodyMap.put(COLUMN_USER_ID, String.valueOf(ChitterModel.getInstance(context).getUser(context).getId()));
                requestBodyMap.put(COLUMN_USER_LAST_SEEN, lastSeen);
                return requestBodyMap;
            }
        };
        // Access the RequestQueue through your singleton class.
        ChitterModel.getInstance(context).addToRequestQueue(contactsRequest, context);
    }

    private static class InsertChatQueryHandler extends AsyncQueryHandler {

        public interface OnCompleteInsertListener {
            void onComplete();
        }

        private WeakReference<OnCompleteInsertListener> onCompleteInserWeakReference;

        public InsertChatQueryHandler(ContentResolver cr, OnCompleteInsertListener onCompleteInsertListener) {
            super(cr);
            this.onCompleteInserWeakReference = new WeakReference<OnCompleteInsertListener>(onCompleteInsertListener);
        }

        @Override
        protected void onInsertComplete(int token, Object cookie, Uri uri) {
            super.onInsertComplete(token, cookie, uri);

            OnCompleteInsertListener onCompleteInsertListener = onCompleteInserWeakReference.get();

            if (onCompleteInsertListener != null) {
                onCompleteInsertListener.onComplete();
            }
        }
    }
}
