package eu.wedgess.chitterandroid.utils;

import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import eu.wedgess.chitterandroid.db.ChitterContentProvider;

/**
 * Android doesn;t allow to listen to Network changes in BG anymore.
 * So from Nougat and future android releases this won't work. But for
 * Lollipop and Marshamallow devices this works fine.
 * <p>
 * Listens to Network changes on device. Registered from within AndroidManifest.xml
 * <p>
 * Created by Gareth on 11/02/2017.
 */

public class NetworkChangeReceiver extends BroadcastReceiver {

    public static final String TAG = NetworkChangeReceiver.class.getSimpleName();

    @Override
    public void onReceive(final Context context, final Intent intent) {

        // check that mobile or wifi netwrok is available
        if (Utils.isNetworkAvailable(context)) {
            Log.i(TAG, "YES Internet connection");
            // use sync adapter to sync local and remote DB
            ContentResolver.requestSync(Utils.createDummyAccount(context),
                    ChitterContentProvider.AUTHORITY, Bundle.EMPTY);
        } else {
            Log.i(TAG, "NO Internet connection");
        }
    }
}
