package eu.wedgess.chitterandroid.utils;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.User;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * Basic utility methods used throughout application.
 * <p>
 * Created by Gareth on 05/12/2016.
 */

public class Utils {

    private static final String TAG = Utils.class.getSimpleName();

    private Utils() {
        // non instantiable
    }

    /**
     * Generate an MD5 from a string
     *
     * @param input - password to generateNotificationID
     * @return - hashed password
     */
    public static String generateMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            return number.toString(16);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    // handle shared preferences
    public static void setBooleanPref(String name, boolean value, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(name, value).commit();
    }

    public static boolean getBooleanPref(String name, boolean def, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(name, def);
    }

    public static void setIntPref(String name, int value, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(name, value).commit();
    }

    public static int getIntPref(String name, int def, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(name, def);
    }

    public static String getStringPref(String name, String def, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(name, def);
    }

    public static void setStringPref(String name, String value, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(name, value).commit();
    }

    public static boolean exists(String name, Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).contains(name);
    }

    public static void removePref(String name, Context context) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove(name).apply();
    }

    /**
     * Format last seen date
     *
     * @param date - date string to format
     * @return - formatted date string
     * @throws ParseException
     */
    public static String formatLastSeenDate(String date) throws ParseException {
        String formattedDate = "";
        Date past;
        past = getSQLTimestampFormat().parse(date);

        Calendar itemDate = Calendar.getInstance();
        itemDate.setTime(past);
        Calendar today = Calendar.getInstance();
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);

        String timeInHrsMins = getHourMinTimestampFormat().format(past);
        if (DateUtils.isToday(past.getTime())) {
            formattedDate = "Today at " + timeInHrsMins;
        } else if (itemDate.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && itemDate.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            formattedDate = "Yesterday at " + timeInHrsMins;
        } else if (itemDate.get(itemDate.YEAR) == today.get(today.YEAR)
                && itemDate.get(itemDate.WEEK_OF_YEAR) == today.get(today.WEEK_OF_YEAR)) {
            formattedDate = getDayOfWeek(itemDate.get(Calendar.DAY_OF_WEEK), false) + " at " + timeInHrsMins;
        } else if (itemDate.get(itemDate.YEAR) == today.get(today.YEAR)) {
            formattedDate = abbreviateDateEndings(itemDate.get(Calendar.DAY_OF_MONTH))
                    + "  " + getMonth(itemDate.get(Calendar.MONTH));
        } else {
            formattedDate = abbreviateDateEndings(itemDate.get(Calendar.DAY_OF_MONTH))
                    + "  " + getMonth(itemDate.get(Calendar.MONTH))
                    + "  " + itemDate.get(itemDate.YEAR);
        }

        return formattedDate;
    }

    /**
     * Check if internet connection is available
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        try {
            // check for both mobile and wifi connection
            int[] networkTypes = {ConnectivityManager.TYPE_MOBILE, ConnectivityManager.TYPE_WIFI};
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (activeNetwork != null) { // connected to the internet
                for (int networkType : networkTypes) {
                    if (networkType == activeNetwork.getType()) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            // returning false now anyway
            Log.e(TAG, e.getMessage());
        }
        return false;
    }

    /**
     * Format chat date string
     *
     * @param date - date string to format
     * @return - formatted string
     * @throws ParseException
     */
    public static String getFormattedDate(String date, boolean showTime) throws ParseException {
        String formattedDate = "";
        Date past;
        Date timeNow = new Date();
        past = getSQLTimestampFormat().parse(date);

        // set the passed date
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(past);
        // todays date
        Calendar today = Calendar.getInstance();
        // yesterdays date
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);

        String timeInHrsMins = getHourMinTimestampFormat().format(past);
        // if today
        if (DateUtils.isToday(past.getTime())) {
            formattedDate = timeInHrsMins;
        } else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && calendar.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR)) {
            // if yesterday
            if (showTime) {
                formattedDate = timeInHrsMins + " Yesterday";
            } else {
                formattedDate = "Yesterday";
            }
        } else if (calendar.get(calendar.YEAR) == today.get(today.YEAR)
                && calendar.get(calendar.WEEK_OF_YEAR) == today.get(today.WEEK_OF_YEAR)) {
            // this week
            if (showTime) {
                formattedDate = timeInHrsMins + " " + getDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK), true);
            } else {
                formattedDate = getDayOfWeek(calendar.get(Calendar.DAY_OF_WEEK), true);
            }
        } else if (calendar.get(calendar.YEAR) == today.get(today.YEAR)) {
            // this year
            formattedDate = abbreviateDateEndings(calendar.get(Calendar.DAY_OF_MONTH))
                    + "  " + getMonth(calendar.get(Calendar.MONTH));
        } else {
            // previous year
            formattedDate = abbreviateDateEndings(calendar.get(Calendar.DAY_OF_MONTH))
                    + "  " + getMonth(calendar.get(Calendar.MONTH))
                    + "  " + calendar.get(calendar.YEAR);
        }

        return formattedDate;
    }

    /**
     * Used for getting the date that a contact registered on Chitter
     *
     * @param date
     * @return
     * @throws ParseException
     */
    public static String getJoinDate(String date) throws ParseException {
        String formattedDate = "";
        Date past;
        Date timeNow = new Date();
        past = getSQLTimestampFormat().parse(date);

        // set the passed date
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(past);
        // todays date
        Calendar today = Calendar.getInstance();
        // yesterdays date
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DATE, -1);

        // full date abbreviated
        formattedDate = abbreviateDateEndings(calendar.get(Calendar.DAY_OF_MONTH))
                + " " + getMonth(calendar.get(Calendar.MONTH))
                + " " + calendar.get(calendar.YEAR);

        return formattedDate;
    }

    /**
     * Get abbreviated month from month
     *
     * @param month
     * @return
     */
    private static String getMonth(int month) {
        switch (month) {
            case Calendar.JANUARY:
                return "Jan";
            case Calendar.FEBRUARY:
                return "Feb";
            case Calendar.MARCH:
                return "Mar";
            case Calendar.APRIL:
                return "Apr";
            case Calendar.MAY:
                return "May";
            case Calendar.JUNE:
                return "June";
            case Calendar.JULY:
                return "July";
            case Calendar.AUGUST:
                return "Aug";
            case Calendar.SEPTEMBER:
                return "Sept";
            case Calendar.OCTOBER:
                return "Oct";
            case Calendar.NOVEMBER:
                return "Nov";
            case Calendar.DECEMBER:
                return "Dec";
            default:
                return "";

        }
    }

    /**
     * Get abbreviated date ending
     *
     * @param day
     * @return
     */
    private static String abbreviateDateEndings(final int day) {
        String date = "";
        switch (day) {
            case 1:
            case 21:
            case 31:
                date = "st";
                break;
            case 2:
            case 22:
                date = "nd";
                break;
            case 3:
            case 23:
                date = "rd";
                break;
            default:
                date = "th";
        }
        return day + date;
    }

    /**
     * Get day of week
     *
     * @param day
     * @return
     */
    private static String getDayOfWeek(int day, boolean abbreviated) {
        switch (day) {
            case Calendar.SUNDAY:
                return abbreviated ? "Sun" : "Sunday";
            case Calendar.MONDAY:
                return abbreviated ? "Mon" : "Monday";
            case Calendar.TUESDAY:
                return abbreviated ? "Tues" : "Tuesday";
            case Calendar.WEDNESDAY:
                return abbreviated ? "Wed" : "Wednesday";
            case Calendar.THURSDAY:
                return abbreviated ? "Thurs" : "Thursday";
            case Calendar.FRIDAY:
                return abbreviated ? "Fri" : "Friday";
            case Calendar.SATURDAY:
                return abbreviated ? "Sat" : "Saturday";
            default:
                return "";
        }
    }

    // default Current Timestamp SQL format
    public static SimpleDateFormat getSQLTimestampFormat() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    // date formatted as hours and mins for ChatRoom messages
    public static SimpleDateFormat getHourMinTimestampFormat() {
        return new SimpleDateFormat("HH:mm");
    }

    /**
     * Create a dummy account needed for sync adapter
     *
     * @param context
     * @return
     */
    public static Account createDummyAccount(Context context) {
        Account dummyAccount = new Account("dummyaccount", "eu.wedgess.chitterandroid");
        AccountManager accountManager = (AccountManager) context.getSystemService(ACCOUNT_SERVICE);
        accountManager.addAccountExplicitly(dummyAccount, null, null);
        ContentResolver.setSyncAutomatically(dummyAccount, ChitterContentProvider.AUTHORITY, true);
        return dummyAccount;
    }

    /**
     * Gets a list of conatcts from the users device alont with their number.
     *
     * @param cr - ContentResolver
     * @return - List of contacts
     */
    public static ArrayList<Contact> getAllDeviceContacts(ContentResolver cr) {

        Cursor pCur = cr.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                        ContactsContract.Contacts.DISPLAY_NAME},
                null,
                null,
                null
        );
        if (pCur != null) {
            if (pCur.getCount() > 0) {
                // 2 HashMaps which we will store the id of contact as the key
                // we will only keep one number of the user, and overwrite existing ones
                HashMap<Integer, String> phones = new HashMap<>();
                HashMap<Integer, String> names = new HashMap<>();
                while (pCur.moveToNext()) {
                    Integer contactId = pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
                    String name = pCur.getString(pCur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    String number = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    if (!names.containsKey(contactId)) {
                        names.put(contactId, name);
                    }
                    // save the last phone number as sometimes a user may have multiple numbers saved on device for one contact
                    phones.put(contactId, number);
                }
                // query and sort the result alphabetically by name
                Cursor cur = cr.query(
                        ContactsContract.Contacts.CONTENT_URI,
                        new String[]{ContactsContract.Contacts._ID, ContactsContract.Contacts.HAS_PHONE_NUMBER},
                        ContactsContract.Contacts.HAS_PHONE_NUMBER + " > 0",
                        null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
                if (cur != null) {
                    if (cur.getCount() > 0) {
                        ArrayList<Contact> contacts = new ArrayList<>();
                        while (cur.moveToNext()) {
                            int id = cur.getInt(cur.getColumnIndex(ContactsContract.Contacts._ID));
                            if (names.containsKey(id) && phones.containsKey(id)) {
                                // get the name and number by key and add contact to list of contacts
                                Contact contact = new Contact();
                                contact.setName(names.get(id));
                                contact.setPhoneNumber(phones.get(id));
                                contacts.add(contact);
                            }
                        }
                        return contacts;
                    }
                    cur.close();
                }
            }
            pCur.close();
        }
        return null;
    }

    // check if a string is numeric using REGEX
    public static boolean isNumeric(String str) {
        return str.matches("[+-]?\\d*(\\.\\d+)?");
    }

    /**
     * Returns user POJO from shared preferences
     *
     * @param context
     * @return - {@link User}
     */
    public static User getUserFromPrefs(Context context) {
        String userJsonString = Utils.getStringPref(Constants.PREF_USER_JSON, "", context);
        if (!TextUtils.isEmpty(userJsonString)) {
            try {
                JSONObject userJson = new JSONObject(userJsonString);
                User user = JsonParser.jsonToUser(userJson);
                if (user != null) {
                    return user;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}