package eu.wedgess.chitterandroid.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.activities.LoginActivity;
import eu.wedgess.chitterandroid.activities.RegisterActivity;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.PhoneNumberFilter;
import eu.wedgess.chitterandroid.utils.Utils;


/**
 * A {@link Fragment} subclass. Which is part of {@link RegisterActivity} ViewPager
 * Used for a new user to register their phone number and password info.
 * <p>
 */
public class RegisterAccountInfoFragment extends Fragment implements View.OnClickListener {

    private final String TAG = RegisterAccountInfoFragment.class.getSimpleName();
    private final String KEY_PHONE_NUMBER = "savedInstancePhoneNumber";
    private final String KEY_PASSWORD = "savedInstancePassword";
    private final String KEY_CONFIRM_PASSWORD = "savedInstanceConfirmPassword";

    private TextInputLayout mPhoneNumberTIL, mPasswordTIL, mConfirmPasswordTIL;
    private ProgressBar mProgressbar;

    public RegisterAccountInfoFragment() {
        // Required empty public constructor
    }

    /**
     * Save the values of mNameInput fields for orientation change, so that they can be restored.
     *
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_PHONE_NUMBER, mPhoneNumberTIL.getEditText().getText().toString());
        outState.putString(KEY_PASSWORD, mPasswordTIL.getEditText().getText().toString());
        outState.putString(KEY_CONFIRM_PASSWORD, mConfirmPasswordTIL.getEditText().getText().toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register_account_info, container, false);

        bindFragment(rootView);

        // if not null restore the values saved in savedInstanceState to the inputs
        if (savedInstanceState != null) {
            mPhoneNumberTIL.getEditText().setText(savedInstanceState.getString(KEY_PHONE_NUMBER));
            mPasswordTIL.getEditText().setText(savedInstanceState.getString(KEY_PASSWORD));
            mConfirmPasswordTIL.getEditText().setText(savedInstanceState.getString(KEY_CONFIRM_PASSWORD));
        }

        return rootView;
    }

    /**
     * Bind the fragments views and listeners
     *
     * @param rootView
     */
    private void bindFragment(View rootView) {
        mProgressbar = (ProgressBar) rootView.findViewById(R.id.pb_register_account);
        mPhoneNumberTIL = (TextInputLayout) rootView.findViewById(R.id.til_reg_phone_number);
        mPasswordTIL = (TextInputLayout) rootView.findViewById(R.id.til_reg_password);
        mConfirmPasswordTIL = (TextInputLayout) rootView.findViewById(R.id.til_confirm_reg_password);
        Button mBackToLoginBtn = (Button) rootView.findViewById(R.id.btn_back_login);
        Button mNextFragmentBtn = (Button) rootView.findViewById(R.id.btn_next_register);
        mBackToLoginBtn.setOnClickListener(this);
        mNextFragmentBtn.setOnClickListener(this);

        final String defaultNumberTxt = getString(R.string.irish_number_start);
        mPhoneNumberTIL.getEditText().setText(defaultNumberTxt);
        mPhoneNumberTIL.getEditText().setSelection(defaultNumberTxt.length());
        mPhoneNumberTIL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!editable.toString().contains(defaultNumberTxt)) {
                    mPhoneNumberTIL.getEditText().setText(defaultNumberTxt);
                    Selection.setSelection(mPhoneNumberTIL.getEditText().getText(), mPhoneNumberTIL.getEditText().getText().length());

                }
            }
        });
        mPhoneNumberTIL.getEditText().setFilters(new InputFilter[]{new PhoneNumberFilter(), new InputFilter.LengthFilter(11)});
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_back_login) {
            // on back button to login pressed start LoginActivity and finish this one
            startActivity(new Intent(getActivity(), LoginActivity.class));
            getActivity().finish();
        } else if (view.getId() == R.id.btn_next_register) {
            if (Utils.isNetworkAvailable(getActivity())) {
                mProgressbar.setVisibility(View.VISIBLE);
                // valid irish number is 10 digits long
                boolean validNumber = mPhoneNumberTIL.getEditText().getText().length() == 10;
                if (!validNumber) {
                    // not valid length set error messages
                    mPhoneNumberTIL.setErrorEnabled(true);
                    mPhoneNumberTIL.setError(getString(R.string.til_error_invalid_number_length));
                } else {
                    mPhoneNumberTIL.setErrorEnabled(false);
                }
                // make sure all TILs aren't empty
                boolean isValid = validNumber
                        && UIHelper.tilIsNotEmpty(mPasswordTIL, getString(R.string.til_error_empty_password))
                        && UIHelper.tilIsNotEmpty(mConfirmPasswordTIL, getString(R.string.til_error_empty_confirm_password));

                // TILs all have mNameInput
                if (isValid) {
                    // TODO: Make password at least 6 characters long
                    // make sure password and confirmation password are equal
                    if (!mPasswordTIL.getEditText().getText().toString()
                            .equals(mConfirmPasswordTIL.getEditText().getText().toString())) {
                        mConfirmPasswordTIL.setErrorEnabled(true);
                        mConfirmPasswordTIL.setError(getString(R.string.til_error_passwords_dont_match));
                        isValid = false;
                    } else if (mConfirmPasswordTIL.isErrorEnabled()) {
                        // disable error
                        mConfirmPasswordTIL.setErrorEnabled(false);
                    }

                    // if still valid make sure the phone number is not laready registered on server
                    if (isValid) {
                        APIRequestUtility.checkIfNumberIsRegistered(mPhoneNumberTIL.getEditText().getText().toString(), getActivity(),
                                new APIRequestUtility.CommonServerCallback() {
                                    @Override
                                    public void onComplete(boolean error) {
                                        if (error) {
                                            mPhoneNumberTIL.setError(getString(R.string.til_error_phone_num_already_registered));
                                        } else {
                                            // NOT REGISTERED  - new user, set the user in model so we can add final bits
                                            // to user object in next fragment (PersonalInfo)
                                            User user = new User();
                                            user.setPhoneNumber(mPhoneNumberTIL.getEditText().getText().toString());
                                            // generateNotificationID the password so that it is not visible on the servers DB
                                            user.setPassword(Utils.generateMD5(mConfirmPasswordTIL.getEditText().getText().toString()));
                                            // set the new user as the user in the model - not in DB until user details are complete
                                            ChitterModel.getInstance(getActivity()).setUser(user);
                                            Fragment fragment = new RegisterPersonalInfoFragment();
                                            // set the new fragment as the current fragment in activity, needed for orientation change
                                            ((RegisterActivity) getActivity()).setCurrentFragment(fragment);
                                            // go to next fragment
                                            getActivity().getFragmentManager()
                                                    .beginTransaction()
                                                    .setCustomAnimations(
                                                            R.animator.slide_fragment_right_in,
                                                            R.animator.slide_fragment_left_out,
                                                            R.animator.slide_fragment_left_in,
                                                            R.animator.slide_fragment_right_out)
                                                    .replace(R.id.flayout_fragment_container, fragment,
                                                            RegisterPersonalInfoFragment.class.getSimpleName())
                                                    .commit();
                                        }
                                        mProgressbar.setVisibility(View.GONE);
                                    }

                                    @Override
                                    public void onError(String error) {
                                        mProgressbar.setVisibility(View.GONE);
                                    }
                                });
                    } else {
                        mProgressbar.setVisibility(View.GONE);
                    }
                }
            } else {
                // show no internet snackbar from RegisterActivity
                ((RegisterActivity) getActivity()).toggleNoInternetSnackbar();
            }
        }
    }
}
