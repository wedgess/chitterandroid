package eu.wedgess.chitterandroid.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Context;
import android.content.DialogInterface;
import android.content.OperationApplicationException;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Data;
import android.provider.ContactsContract.RawContacts;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.activities.ContactInfoActivity;
import eu.wedgess.chitterandroid.activities.ContactsActivity;
import eu.wedgess.chitterandroid.activities.GroupInfoActivity;
import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.Contact;

/**
 * A AlertDialog wrapped in a dialog fragment as AlertDialogs get hidden on orientation change
 * if showing. Wrapping in DialogFragment solves this.
 * <p>
 * Created by Gareth on 30/01/2017.
 */

public class CreateContactDialogFragment extends DialogFragment {

    public static final String TAG = CreateContactDialogFragment.class.getSimpleName();
    public static final String DISPLAY_NAME_BUNDLE = "displayNameKey";
    public static final String CONTACT_BUNDLE = "phoneNumKey";

    // contact which we are saving to device contacts
    private Contact mContact;
    private String mDisplayName;
    private OnCreateDialogCloseListener dialogCloseListener;

    public interface OnCreateDialogCloseListener {
        void onDialogComplete();
    }

    public static CreateContactDialogFragment newInstance(Contact contact) {
        Bundle args = new Bundle();
        args.putParcelable(CONTACT_BUNDLE, contact);

        CreateContactDialogFragment fragment = new CreateContactDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mContact = bundle.getParcelable(CONTACT_BUNDLE);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DISPLAY_NAME_BUNDLE, mDisplayName);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // inflate custom layout
        View customView = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_create_contact, null);

        final TextInputLayout displayNameTIL = (TextInputLayout) customView.findViewById(R.id.til_contact_name);
        final TextView phoneNumberTV = (TextView) customView.findViewById(R.id.tv_contact_number);

        displayNameTIL.getEditText().setText(mContact.getName());
        // set cursor at end of string
        displayNameTIL.getEditText().setSelection(mContact.getName().length());
        phoneNumberTV.setText(mContact.getPhoneNumber());

        // restore instance states on orientation change
        if (savedInstanceState != null) {
            mDisplayName = savedInstanceState.getString(DISPLAY_NAME_BUNDLE);
            displayNameTIL.getEditText().setText(mDisplayName);
        }

        // use an alert dialog and set the customView as its view
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert_title_create_contact)
                .setView(customView)
                .setCancelable(true)
                .setPositiveButton(R.string.btn_create, null)
                .setNegativeButton(R.string.btn_close,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .create();

        // this makes sure the dialog doesn;t close when positive button is pressed and TIL is empty
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // make sure mNameInput for name is not empty
                        if (UIHelper.tilIsNotEmpty(displayNameTIL, getString(R.string.tile_error_empty_name))) {
                            // write contact to device contacts
                            writePhoneContact(displayNameTIL.getEditText().getText().toString(),
                                    phoneNumberTV.getText().toString(), getActivity());
                            // use whatever contact name the contact was saved under on the device
                            mContact.setName(displayNameTIL.getEditText().getText().toString());
                            // contact now exist on device
                            mContact.setExistOnDevice(true);
                            // update contact
                            DBHandler.getInstance(getActivity()).updateContact(mContact);
                            if (dialogCloseListener != null) {
                                // send result back to listener
                                dialogCloseListener.onDialogComplete();
                            }
                            //Dismiss once everything is OK.
                            alertDialog.dismiss();
                        }
                    }
                });
            }
        });

        return alertDialog;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // if the calling activity is any one of the following set the listener as they implement it
        if (activity instanceof ContactsActivity ||
                activity instanceof GroupInfoActivity ||
                activity instanceof ContactInfoActivity) {
            dialogCloseListener = (OnCreateDialogCloseListener) activity;
        }
    }


    private void writePhoneContact(String displayName, String number, Context context) {

        // list of operations
        ArrayList<ContentProviderOperation> contentProviderOperations = new ArrayList<ContentProviderOperation>();
        int contactIndex = contentProviderOperations.size();//ContactSize

        //Newly Inserted contact
        // A raw contact will be inserted ContactsContract.RawContacts table in contacts database.
        contentProviderOperations.add(ContentProviderOperation.newInsert(RawContacts.CONTENT_URI)//Step1
                .withValue(RawContacts.ACCOUNT_TYPE, null)
                .withValue(RawContacts.ACCOUNT_NAME, null).build());

        //Display name will be inserted in ContactsContract.Data table
        contentProviderOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)//Step2
                .withValueBackReference(Data.RAW_CONTACT_ID, contactIndex)
                .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
                .withValue(StructuredName.DISPLAY_NAME, displayName) // Name of the contact
                .build());
        //Mobile number will be inserted in ContactsContract.Data table
        contentProviderOperations.add(ContentProviderOperation.newInsert(Data.CONTENT_URI)//Step 3
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, contactIndex)
                .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
                .withValue(Phone.NUMBER, number) // Number to be added
                .withValue(Phone.TYPE, Phone.TYPE_MOBILE).build()); //Type like HOME, MOBILE etc
        try {
            // We will do batch operation to insert all above data
            //Contains the output of the app of a ContentProviderOperation.
            //It is sure to have exactly one of uri or count set
            ContentProviderResult[] contentProresult = null;
            contentProresult = context.getContentResolver().applyBatch(ContactsContract.AUTHORITY, contentProviderOperations); //apply above data insertion into contacts list
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }
}
