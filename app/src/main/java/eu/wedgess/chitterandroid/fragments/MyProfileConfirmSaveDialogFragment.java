package eu.wedgess.chitterandroid.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import eu.wedgess.chitterandroid.R;

/**
 * Created by Gareth on 10/04/2017.
 */

public class MyProfileConfirmSaveDialogFragment extends DialogFragment {

    public static final String TAG = MyProfileConfirmSaveDialogFragment.class.getSimpleName();

    private ConfirmSaveListener mCompleteListener;

    public interface ConfirmSaveListener {
        void sendSaveResult(boolean save);
    }

    public static MyProfileConfirmSaveDialogFragment newInstance() {
        MyProfileConfirmSaveDialogFragment fragment = new MyProfileConfirmSaveDialogFragment();
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCompleteListener = (ConfirmSaveListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // use an alert dialog and set the customView as its view

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert_title_save_profile_changes)
                .setMessage(R.string.alert_msg_save_profile)
                .setCancelable(true)
                .setPositiveButton(R.string.btn_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCompleteListener.sendSaveResult(true);
                    }
                })
                .setNegativeButton(R.string.btn_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mCompleteListener.sendSaveResult(false);
                                dialog.cancel();
                            }
                        })
                .create();
    }
}
