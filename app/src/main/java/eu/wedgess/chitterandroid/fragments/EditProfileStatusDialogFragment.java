package eu.wedgess.chitterandroid.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import eu.wedgess.chitterandroid.R;

/**
 * Created by Gareth on 10/04/2017.
 */

public class EditProfileStatusDialogFragment extends DialogFragment {

    public static final String TAG = EditProfileStatusDialogFragment.class.getSimpleName();
    private Spinner mStatusSpinner;
    private StatusDialogCompleteListener mCompleteListener;

    public interface StatusDialogCompleteListener {
        void sendStatusDialogResult(String result);
    }

    public static EditProfileStatusDialogFragment newInstance(String name) {
        Bundle args = new Bundle();
        args.putString(TAG, name);
        EditProfileStatusDialogFragment fragment = new EditProfileStatusDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCompleteListener = (StatusDialogCompleteListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG, mStatusSpinner.getSelectedItem().toString());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View root = inflater.inflate(R.layout.fragment_dialog_edit_profilestatus, null, false);

        mStatusSpinner = (Spinner) root.findViewById(R.id.spinner_my_profile_status);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.user_status, android.R.layout.simple_spinner_item);
        // set custom drop down list
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStatusSpinner.setAdapter(spinnerAdapter);

        // get current selection from arguments or savedInstance depending
        if (savedInstanceState == null) {
            int curPos = spinnerAdapter.getPosition(getArguments().getString(TAG));
            mStatusSpinner.setSelection(curPos);
        } else {
            int curPos = spinnerAdapter.getPosition(savedInstanceState.getString(TAG));
            mStatusSpinner.setSelection(curPos);
        }

        //mStatusSpinner.setSelection(mStatusSpinner.getText().length());

        // use an alert dialog and set the customView as its view
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert_title_change_status)
                .setView(root)
                .setCancelable(true)
                .setPositiveButton(R.string.btn_ok, null)
                .setNegativeButton(R.string.btn_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .create();

        // this makes sure the dialog doesn;t close when positive button is pressed and TIL is empty
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(final DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        mCompleteListener.sendStatusDialogResult(mStatusSpinner.getSelectedItem().toString());
                        dialog.dismiss();
                    }
                });
            }
        });

        return alertDialog;
    }
}
