package eu.wedgess.chitterandroid.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.model.Message;

/**
 * A AlertDialog wrapped in a dialog fragment as AlertDialogs get hidden on orientation change
 * if showing. Wrapping in DialogFragment solves this.
 * <p>
 * Created by Gareth on 30/01/2017.
 */

public class MessageInfoDialogFragment extends DialogFragment {

    private static final String TAG = MessageInfoDialogFragment.class.getSimpleName();
    private Message mMessage;
    private String mContactName;

    // build the fragments bundle
    public static MessageInfoDialogFragment newInstance(Message message, String contactName) {
        Bundle args = new Bundle();
        args.putParcelable(TAG, message);
        args.putString(Constants.KEY_CONTACT, contactName);
        MessageInfoDialogFragment fragment = new MessageInfoDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        mMessage = bundle.getParcelable(TAG);
        mContactName = bundle.getString(Constants.KEY_CONTACT);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // inflate custom layout
        View customView = getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_msg_info, null);

        // reference the widgets within the layout
        TextView dateValue = (TextView) customView.findViewById(R.id.tv_info_date_value);
        TextView senderValue = (TextView) customView.findViewById(R.id.tv_info_sender_value);
        TextView statusValue = (TextView) customView.findViewById(R.id.tv_info_status_value);

        dateValue.setText(mMessage.getCreatedAt());
        String name;
        // if contact name is not null or empty set it
        if (mContactName != null && !TextUtils.isEmpty(mContactName)) {
            name = mContactName;
        } else {
            // make sure sender id is not this users, if it is set current users name
            if (ChitterModel.getInstance(getActivity()).userIsOwner(mMessage.getSenderId(), getActivity())) {
                name = ChitterModel.getInstance(getActivity()).getUser(getActivity()).getName()
                        + " " + getActivity().getString(R.string.current_user_me_postfix);
            } else {
                // no contact name and not current user set name as ID
                name = String.valueOf(mMessage.getSenderId());
            }
        }
        senderValue.setText(name);
        String status;
        int color = 0;
        // get status string based on status flag and get color based on the status
        switch (mMessage.getStatus()) {
            case Message.STATUS_PENDING:
                status = getString(R.string.message_status_failed);
                color = R.color.colorError;
                break;
            case Message.STATUS_DELIVERED_TO_SERVER:
                status = getString(R.string.message_status_delivered_to_server);
                color = R.color.material_orange;
                break;
            case Message.STATUS_DELIVERED_TO_USER:
                status = getString(R.string.message_status_delivered);
                color = R.color.material_green;
                break;
            case Message.STATUS_READ_BY_USER:
                status = getString(R.string.message_status_read);
                color = R.color.blue_read;
                break;
            default:
                status = getString(R.string.message_status_unknown);
                color = R.color.material_brown;
                break;
        }
        statusValue.setText(status);
        statusValue.setTextColor(ContextCompat.getColor(getActivity(), color));

        // use an alert dialog and set the customView as its view
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert_title_message_information)
                .setView(customView)
                .setCancelable(true)
                .setNegativeButton(R.string.btn_close,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .create();

        return alertDialog;
    }
}
