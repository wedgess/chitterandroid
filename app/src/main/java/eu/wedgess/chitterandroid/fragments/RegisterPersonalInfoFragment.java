package eu.wedgess.chitterandroid.fragments;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.IOException;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.activities.ChatHomeActivity;
import eu.wedgess.chitterandroid.activities.RegisterActivity;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.tasks.MediaUploadTask;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.RoundedImageView;

import static android.app.Activity.RESULT_OK;


/**
 * A {@link Fragment} subclass. Which is part of {@link RegisterActivity} ViewPager
 * Used for a new user to add their profile image, name and status.
 * <p>
 */
public class RegisterPersonalInfoFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = RegisterPersonalInfoFragment.class.getSimpleName();
    private static final int IMAGE_REQUEST_CODE = 101;

    private final String KEY_NAME = "savedInstanceNameKey";

    private TextInputLayout mNameTIL;
    private RoundedImageView mUserProfileImg;
    private Spinner mStatusSpinner;
    private Button mRegistrationDoneBtn;
    private ProgressBar mImageUploadPb;
    private User user;
    private String mProfileImageLocation;
    private ProgressBar mRegisterProgressbar;

    public RegisterPersonalInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // same the name tils vsalue on orientation change
        outState.putString(KEY_NAME, mNameTIL.getEditText().getText().toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        user = ChitterModel.getInstance(getActivity()).getUser(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register_personal_info, container, false);
        bindFragment(rootView);

        if (savedInstanceState != null) {
            // restore the name tils value after orientation change
            mNameTIL.getEditText().setText(savedInstanceState.getString(KEY_NAME));
        }

        return rootView;
    }

    /**
     * get a reference to all of the fragments views
     *
     * @param rootView - the layout
     */
    private void bindFragment(View rootView) {
        mRegisterProgressbar = (ProgressBar) rootView.findViewById(R.id.pb_register_personal);
        mImageUploadPb = (ProgressBar) rootView.findViewById(R.id.pb_img_upload);
        mRegistrationDoneBtn = (Button) rootView.findViewById(R.id.btn_done_register);
        final FloatingActionButton fab = (FloatingActionButton) rootView.findViewById(R.id.fab_profile_img);
        mNameTIL = (TextInputLayout) rootView.findViewById(R.id.til_reg_name);
        mUserProfileImg = (RoundedImageView) rootView.findViewById(R.id.riv_user_profile_img);

        // set up custom spinner look and feel
        mStatusSpinner = (Spinner) rootView.findViewById(R.id.spinner_status);
        mStatusSpinner.getBackground().setColorFilter(ContextCompat.getColor(getActivity(), R.color.text_secondary_dark), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.user_status, R.layout.login_simple_spinner_item);
        // set custom drop down list
        spinnerAdapter.setDropDownViewResource(R.layout.login_simple_spinner_dropdown_item);
        mStatusSpinner.setAdapter(spinnerAdapter);
        mRegistrationDoneBtn.setOnClickListener(this);
        fab.setOnClickListener(this);
    }

    // open media picker chooser to select image
    private void openMediaPicker() {
        Intent intent = new Intent();
        // the type which we will be picking to upload is image of any format
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_profile_img_chooser)), IMAGE_REQUEST_CODE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_done_register:
                // make sure we have an internet connection before trying to register
                if (Utils.isNetworkAvailable(getActivity())) {
                    mRegisterProgressbar.setVisibility(View.VISIBLE);
                    // make sure name is not empty
                    if (UIHelper.tilIsNotEmpty(mNameTIL, getString(R.string.tile_error_empty_name))) {
                        // set user name, status, FCM token and image location if selected
                        user.setName(mNameTIL.getEditText().getText().toString());
                        user.setStatus((String) mStatusSpinner.getSelectedItem());
                        user.setFcmToken(Utils.getStringPref(Constants.PREF_FCM_TOKEN, "", getActivity()));
                        if (mProfileImageLocation != null) {
                            user.setImgUrl(mProfileImageLocation);
                        }
                        Log.i(TAG, "Sending user to server as: " + user.toString());
                        // add new user to server
                        APIRequestUtility.registerUser(user, getActivity(), new APIRequestUtility.
                                CommonServerCallback() {
                            @Override
                            public void onComplete(boolean error) {
                                if (!error) {
                                    mRegisterProgressbar.setVisibility(View.GONE);
                                    // start the chat home activity
                                    startActivity(new Intent(getActivity(), ChatHomeActivity.class));
                                    getActivity().finish();
                                }
                            }

                            @Override
                            public void onError(String error) {
                                mRegisterProgressbar.setVisibility(View.GONE);
                            }
                        });
                    } else {
                        // empty name, hide progressbar
                        mRegisterProgressbar.setVisibility(View.GONE);
                    }
                } else {
                    // show no internet snackbar from actvity
                    ((RegisterActivity) getActivity()).toggleNoInternetSnackbar();
                }
                break;
            case R.id.fab_profile_img:
                // make sure we have an internet connection
                if (Utils.isNetworkAvailable(getActivity())) {
                    // make sure we have WRITE/READ EXTERNAL storage permission
                    if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 911);
                    } else {
                        // we have permission so open picker
                        openMediaPicker();
                    }
                } else {
                    // no internet so show toast as image can;t be uploaded
                    Toast.makeText(getActivity(), getString(R.string.snackbar_msg_no_internet), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "Permission callback called in fragment");
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openMediaPicker();
        } else {
            Toast.makeText(getActivity(), R.string.toast_msg_no_permission, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            // hide register button and show progressbar while uploading image
            mRegistrationDoneBtn.setVisibility(View.GONE);
            mImageUploadPb.setVisibility(View.VISIBLE);
            Uri filePath = data.getData();
            Bitmap bitmap = null;
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                //Setting the Bitmap to RoundedImageView
                mUserProfileImg.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            new MediaUploadTask(getActivity(), new MediaUploadTask.UploadProgressListener() {
                @Override
                public void onUploadComplete(int responseCode, String fileLocation) {
                    // if successful upload
                    if (responseCode == 200) {
                        // replace the spaces with encoded spaces
                        String decodedFileLocation = fileLocation.replaceAll(" ", "%20");
                        mProfileImageLocation = decodedFileLocation;
                        Toast.makeText(getActivity(), R.string.toast_msg_img_upload_success, Toast.LENGTH_LONG).show();
                    }
                    // show register button and hide progressbar upload finished due to error
                    mRegistrationDoneBtn.setVisibility(View.VISIBLE);
                    mImageUploadPb.setVisibility(View.GONE);
                }

                @Override
                public void onUploadError(final String error) {
                    // show register button and hide progressbar upload finished due to error
                    mRegistrationDoneBtn.setVisibility(View.VISIBLE);
                    mImageUploadPb.setVisibility(View.GONE);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }).execute(filePath);
        }
    }
}
