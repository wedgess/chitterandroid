package eu.wedgess.chitterandroid.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import eu.wedgess.chitterandroid.R;

/**
 * Created by Gareth on 10/04/2017.
 */

public class EditProfileNameDialogFragment extends DialogFragment {

    public static final String TAG = EditProfileNameDialogFragment.class.getSimpleName();
    private EditText mNameInput;

    private UsernameDialogCompleteListener mCompleteListener;

    public interface UsernameDialogCompleteListener {
        void sendUsernameDialogResult(String result);
    }

    public static EditProfileNameDialogFragment newInstance(String name) {

        Bundle args = new Bundle();
        args.putString(TAG, name);
        EditProfileNameDialogFragment fragment = new EditProfileNameDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCompleteListener = (UsernameDialogCompleteListener) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG, mNameInput.getText().toString());
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View root = inflater.inflate(R.layout.fragment_dialog_edit_profilename, null, false);

        mNameInput = (EditText) root.findViewById(R.id.et_my_profile_name);

        if (savedInstanceState == null) {
            mNameInput.setText(getArguments().getString(TAG));
        } else {
            mNameInput.setText(savedInstanceState.getString(TAG));
        }

        mNameInput.setSelection(mNameInput.getText().length());

        // use an alert dialog and set the customView as its view
        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.alert_title_change_username)
                .setView(root)
                .setCancelable(true)
                .setPositiveButton(R.string.btn_ok, null)
                .setNegativeButton(R.string.btn_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .create();

        // this makes sure the dialog doesn;t close when positive button is pressed and TIL is empty
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // make sure mNameInput for name is not empty
                        if (!TextUtils.isEmpty(mNameInput.getText())) {
                            mCompleteListener.sendUsernameDialogResult(mNameInput.getText().toString());
                            //Dismiss once everything is OK.
                            alertDialog.dismiss();
                        } else {
                            mNameInput.setError(getString(R.string.tile_error_empty_name));
                        }
                    }
                });
            }
        });

        return alertDialog;
    }
}
