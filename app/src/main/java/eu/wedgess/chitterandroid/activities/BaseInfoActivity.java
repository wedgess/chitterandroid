package eu.wedgess.chitterandroid.activities;

import android.animation.Animator;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.views.RoundedImageView;

/**
 * Superclass of both {@link ContactInfoActivity} and {@link GroupInfoActivity}
 * Both activities are the same besides use shared element transitons, and contain the
 * same toolbar.
 */
public abstract class BaseInfoActivity extends AppCompatActivity {

    public static final String TAG = BaseInfoActivity.class.getSimpleName();

    private RoundedImageView mToolbarLogo;
    private TextView mToolbarTitle, mToolbarSubTitle;
    private Toolbar mToolbar;
    private FloatingActionButton mFab;

    /**
     * Subclasses will set up their own stub layout. Both {@link GroupInfoActivity}
     * and {@link ContactInfoActivity}
     */
    public abstract void bindStubLayout();

    public RoundedImageView getToolbarLogoIV() {
        return this.mToolbarLogo;
    }

    public TextView getToolbarSubTitleTV() {
        return this.mToolbarSubTitle;
    }

    public TextView getToolbarTitleTV() {
        return this.mToolbarTitle;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_info);

        bindActivity();

        // Postpone the transition until the window's decor view has
        // finished its layout.
        postponeEnterTransition();

        final View decor = getWindow().getDecorView();
        decor.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                decor.getViewTreeObserver().removeOnPreDrawListener(this);
                startPostponedEnterTransition();
                return true;
            }
        });

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        ViewStub stub = (ViewStub) findViewById(R.id.layout_stub);
        if (this instanceof GroupInfoActivity) {
            stub.setLayoutResource(R.layout.content_group_info);
            //Log.i(TAG, "Is group info");
        } else {
            stub.setLayoutResource(R.layout.content_contact_info);
            //Log.i(TAG, "Is user to user info");
        }

        setFabOriginalOnClickLister();

        stub.inflate();
        bindStubLayout();

        //mToolbar.inflateMenu(R.menu.menu_main);
        setupWindowAnimations();
    }

    // get the floating actionbutton used mainly in GroupInfoActvity for adding user to devices contacts
    public FloatingActionButton getFAB() {
        return mFab;
    }

    // set the FABs click listener as going back to previous chat for now
    public void setFabOriginalOnClickLister() {
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishAfterTransition();
            }
        });
    }

    // slide out window animation
    private void setupWindowAnimations() {
        // Using gravity compat otherwise it crashed on older devices
        Slide slide = new Slide(GravityCompat.getAbsoluteGravity(GravityCompat.END, getResources().getConfiguration().getLayoutDirection()));
        slide.setDuration(200);
        //slide.setSlideEdge(Gravity.RIGHT);
        getWindow().setEnterTransition(slide);
    }

    /**
     * Bind this activities views
     */
    private void bindActivity() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_info);
        mToolbarTitle = (TextView) findViewById(R.id.cat_title);
        mToolbarLogo = (RoundedImageView) findViewById(R.id.cat_avatar);
        mToolbarSubTitle = (TextView) findViewById(R.id.cat_subtitle);
        mFab = (FloatingActionButton) findViewById(R.id.fab_info);
    }

    public void setCollapseToolbar(final Drawable drawable, String title, String subTitle) {
        // drawable will be null here if the user has a profile img
        if (drawable != null) {
            mToolbarLogo.setBackground(drawable);
        }
        mToolbarTitle.setText(title);
        mToolbarSubTitle.setText(subTitle);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // we want to play the shared element transition then finish
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // on back press finish activity after transition has played
        Animator anim = UIHelper.getScaleAnimatorSet(mFab, 1, 0, 50);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

                finishAfterTransition();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        anim.start();
        //super.onBackPressed();
    }

    public void startViewImageActivity(String imgUrl) {
        // start ViewImageActivity on Toolbar image click
        Intent intent = new Intent(this, ViewImageActivity.class);
        // set the image url within the intents extras for loading image
        intent.putExtra(ViewImageActivity.IMAGE_URL_EXTRA, imgUrl);

        // set up shared elements between actvities
        final ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                this,
                Pair.create((View) mToolbarLogo, mToolbarLogo.getTransitionName()));
        startActivity(intent, options.toBundle());
    }
}
