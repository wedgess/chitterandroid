package eu.wedgess.chitterandroid.activities;

import android.Manifest;
import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.adapters.ContactsAdapter;
import eu.wedgess.chitterandroid.callbacks.CreateContactListener;
import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.fragments.CreateContactDialogFragment;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.ChatMainItemDivider;

/**
 * This class is used to create new chats, user to user chats or group chats with multiple members.
 * It extends {@link BaseSelectableActivity} as when creating group chats the user must select multiple
 * members to add to the chat group.
 * <p>
 * Created by Gareth on 20/12/2016.
 */

public class ContactsActivity extends BaseSelectableActivity implements CreateContactDialogFragment.OnCreateDialogCloseListener {

    private final String TAG = ContactsActivity.class.getSimpleName();

    private RelativeLayout mGroupChatSelectionLayout;
    private TextView mEmptyTV;
    private RecyclerView mRecyclerView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        bindActivity();
        getLoaderManager().initLoader(Constants.LOADER_ID_CONTACTS, null, this);
        // check registered chitter users with device contacts, this is already done in HomeActivity,
        // however more contacts could have joined since we last opened the app so refresh
        APIRequestUtility.checkContacts(Utils.getAllDeviceContacts(getContentResolver()), this);
    }

    private void bindActivity() {
        // setup the action bar & title
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_contact);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.activity_title_new_chat));
        }


        mEmptyTV = (TextView) findViewById(R.id.tv_no_contacts);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_contacts);
        // because recycler is in a NestedScrollView, the scroll was not smooth so disable
        // nested scrolling which gives back the smooth scroll effect
        mRecyclerView.setNestedScrollingEnabled(false);
        mRecyclerView.addItemDecoration(new ChatMainItemDivider(UIHelper.dpToPx(80, this), this));

        // set the recyclerviews adapter with a callback to create the contact (if not exist in device contacts)
        // if contact does not exist in device contacts then we show an add contact button so that
        // the user can add the contact to their device contacts
        setAdapter(new ContactsAdapter(null, this, new CreateContactListener() {
            @Override
            public void onCreateContact(int position) {
                if (ContextCompat.checkSelfPermission(ContactsActivity.this, Manifest.permission.WRITE_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ContactsActivity.this,
                            new String[]{Manifest.permission.WRITE_CONTACTS}, Constants.WRITE_CONTACT_START);
                } else {
                    CreateContactDialogFragment.newInstance(
                            ((ContactsAdapter) mRecyclerView.getAdapter()).getItemAtPosition(position))
                            .show(getFragmentManager(), TAG);
                }
            }
        }, this));
        mRecyclerView.setAdapter(getAdapter());


        mGroupChatSelectionLayout = (RelativeLayout) findViewById(R.id.rlayout_group_chat);
        mGroupChatSelectionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getAdapter().getItemCount() > 1) {
                    mGroupChatSelectionLayout.setVisibility(View.GONE);
                    startActionMode();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // finish this activity on home button press
            ContactsActivity.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
        // use custom animation on activity finish
        overridePendingTransition(R.anim.do_not_move, R.anim.activity_exit_to_right);
    }

    @Override
    public void onItemClicked(int position) {
        // if action mode is started then toggle selection, otherwise start chat room activity
        if (getActionMode() != null) {
            // must be before setting done button visibility
            getAdapter().toggleSelection(position);
            MenuItem doneItem = getActionMode().getMenu().getItem(0);
            doneItem.setVisible(getAdapter().getSelectedItemCount() > 1);
            setActionModeTitle();
        } else {
            startChatRoomActivity((Contact) getAdapter().getItemAtPosition(position));
        }
    }

    /**
     * If there are no contacts hide the recylerview and display a TextView with drawable
     * otherwise show RecyclerView and hide textview
     */
    private void toggleEmptyLayout() {
        //Log.d(TAG, "ToggleEmptyLayout called!");
        if (getAdapter().getItemCount() > 0) {
            if (mRecyclerView.getVisibility() == View.GONE) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mEmptyTV.setVisibility(View.GONE);
            }
            // if its hidden and their is now enough contacts for a group(2) then show the option
            // for group chats
            if (mGroupChatSelectionLayout.getVisibility() == View.GONE
                    && getAdapter().getItemCount() > 1) {
                mGroupChatSelectionLayout.setVisibility(View.VISIBLE);
            }
        } else {
            if (mEmptyTV.getVisibility() == View.GONE) {
                mEmptyTV.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        }
        // if its visible and their is now NOT enough contacts for a group(2) then hide the option
        // for group chats
        if (mGroupChatSelectionLayout.getVisibility() == View.VISIBLE
                && getAdapter().getItemCount() < 2) {
            mGroupChatSelectionLayout.setVisibility(View.GONE);

        }
    }

    /**
     * Start the create chat group activity setting extras as the list of contacts
     *
     * @param participants - list of new chat groups members
     */
    private void startCreateGroupChatActivity(ArrayList<Contact> participants) {
        // new bundle to store the values of the extras
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ChatRoomActivity.KEY_CONTACTS_LIST, participants);
        Intent intent = new Intent(this, CreateGroupChatActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.activity_enter_from_right, R.anim.do_not_move);
    }

    /**
     * Start chat room activity for user to user chat
     *
     * @param contact - recipient
     */
    private void startChatRoomActivity(final Contact contact) {
        CheckChatExistsQueryHandler chatExistsQueryHandler = new CheckChatExistsQueryHandler(getContentResolver(),
                new CheckChatExistsQueryHandler.QueryCompleteListener() {
                    @Override
                    public void onComplete(boolean chatExist, String cid) {
                        // create new intent for the ChatRoomActivity
                        // set the bundle as the extras and start the activity while finishing this one
                        Intent intent = new Intent(ContactsActivity.this, ChatRoomActivity.class);
                        // new bundle to store the values of the extras
                        Bundle bundle = new Bundle();
                        bundle.putParcelable(Constants.KEY_CONTACT, contact);
                        if (chatExist) {
                            Cursor cursor1 = getContentResolver().query(ChitterContentProvider.CONTENT_URI_CHAT_ITEM,
                                    null, null, new String[]{cid}, null, null);
                            if (cursor1 != null && cursor1.moveToFirst()) {
                                bundle.putParcelable(Constants.KEY_CHAT, new ChatRoom(cursor1));
                                cursor1.close();
                            }
                            bundle.putInt(ChatRoomActivity.KEY_NEW_CHAT, ChatRoomActivity.EXISTING_CHAT);
                        } else {
                            bundle.putInt(ChatRoomActivity.KEY_NEW_CHAT, ChatRoomActivity.NEW_USER_TO_USER_CHAT);
                        }
                        intent.putExtras(bundle);
                        startActivity(intent);
                        overridePendingTransition(R.anim.do_not_move, R.anim.activity_exit_to_right);
                        finish();
                    }
                });

        chatExistsQueryHandler.startQuery(Constants.QUERY_ID_CHAT_EXIST, null, ChitterContentProvider.CONTENT_URI_USERS_CHATS_EXIST,
                null, null, new String[]{String.valueOf(contact.getId()),
                        String.valueOf(ChitterModel.getInstance(ContactsActivity.this).getUser(getApplicationContext()).getId())}, null);
    }

    @Override
    public void onItemLongClicked(int position) {
        // do nothing
    }

    @Override
    public int getActionModeMenuId() {
        return R.menu.cab_contacts;
    }

    @Override
    public String getEmptySelectedTitle() {
        return getString(R.string.title_cab_select_participants);
    }

    @Override
    public int getSelectedTitleId() {
        return R.string.title_x_participants;
    }

    @Override
    public Uri getContentProviderUri() {
        return ChitterContentProvider.CONTENT_URI_CONTACTS;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        // actionmodes done button is pressed
        if (menuItem.getItemId() == R.id.action_group_chat_done) {
            // if more than one contact is selected
            if (getAdapter().getSelectedItemCount() > 1) {
                // get selected items list
                List<Integer> selectedItems = getAdapter().getSelectedItems();
                ArrayList<Contact> participants = new ArrayList<>();
                // for each selected position
                for (Integer position : selectedItems) {
                    // get the contact from the adapter
                    Contact contact = (Contact) getAdapter().getItemAtPosition(position);
                    // add contact to participants
                    participants.add(contact);
                }
                startCreateGroupChatActivity(participants);
            } else {
                // not enough members selected for group chat
                Toast.makeText(this, R.string.toast_msg_group_chat_more_members, Toast.LENGTH_LONG).show();
            }
            return true;
        }
        return false;
    }

    /**
     * Callback for when user is added to device contacts, update the contacts URI to remove add contact ImageButton
     * from {@link CreateContactDialogFragment}.
     */
    @Override
    public void onDialogComplete() {
        // when dialog is complete notify chat members UI of change
        getContentResolver().notifyChange(getContentProviderUri(), null);
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        super.onDestroyActionMode(actionMode);
        mGroupChatSelectionLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        super.onLoadFinished(loader, data);
        toggleEmptyLayout();
    }

    // extend query handler so that the class can be static to avoid memory leaks
    private static class CheckChatExistsQueryHandler extends AsyncQueryHandler {

        interface QueryCompleteListener {
            void onComplete(boolean chatExist, String cid);
        }

        private WeakReference<QueryCompleteListener> listenerWeakReference;

        CheckChatExistsQueryHandler(ContentResolver cr, QueryCompleteListener queryCompleteListener) {
            super(cr);
            listenerWeakReference = new WeakReference<>(queryCompleteListener);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            super.onQueryComplete(token, cookie, cursor);

            QueryCompleteListener listener = listenerWeakReference.get();

            boolean chatExists = false;
            String currentChatId = null;
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    // user to user chat should have only 2 members, if no chat exists or member count is not 2 then chat doesn;t exist
                    String cid = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_USERS_CHATS_CID));
                    int member_count = cursor.getInt(cursor.getColumnIndex(DBConstants.COLUMN_USERS_CHATS_MEMBERS_COUNT));
                    if (member_count == 2) {
                        chatExists = true;
                        currentChatId = cid;
                        break;
                    }
                }
                cursor.close();
            }

            if (listener != null) {
                listener.onComplete(chatExists, currentChatId);
            }
        }
    }
}
