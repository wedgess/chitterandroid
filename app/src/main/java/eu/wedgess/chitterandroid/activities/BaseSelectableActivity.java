package eu.wedgess.chitterandroid.activities;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.adapters.ContactsAdapter;
import eu.wedgess.chitterandroid.adapters.BaseCursorAdapter;
import eu.wedgess.chitterandroid.adapters.SelectableAdapter;
import eu.wedgess.chitterandroid.callbacks.RecyclerClickListener;

/**
 * Superclass of {@link ChatHomeActivity}, {@link ChatRoomActivity}, {@link ContactsActivity}
 * contains an actionmode when an item is long clicked. All subclasses have their own content URI
 * which are loaded from within this class. Subclasses will be able to multi-select items from their list.
 * <p>
 * Created by Gareth on 30/01/2017.
 */

public abstract class BaseSelectableActivity extends AppCompatActivity
        implements ActionMode.Callback, RecyclerClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    private ActionMode mActionMode;
    private BaseCursorAdapter mAdapter;

    // get action modes resource id
    public abstract int getActionModeMenuId();

    // get the action mode title when no selection is made
    public abstract String getEmptySelectedTitle();

    // set actionmodes title for when items are selected
    public abstract int getSelectedTitleId();

    // get content provider URIs for the recyclerviews loaders
    public abstract Uri getContentProviderUri();

    public BaseCursorAdapter getAdapter() {
        return this.mAdapter;
    }

    public void setAdapter(BaseCursorAdapter recylerViewAdapter) {
        this.mAdapter = recylerViewAdapter;
    }

    public ActionMode getActionMode() {
        return this.mActionMode;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save adapters state on orientation change
        if (mAdapter != null) {
            mAdapter.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (mAdapter != null) {
            // restore adapters state on orientation change
            mAdapter.onRestoreInstanceState(savedInstanceState);
            if (mAdapter.getMode() == SelectableAdapter.MODE_MULTI) {
                // start action mode if adapter was in multi-select mode
                mActionMode = startActionMode(this);
                // reset the action modes title
                setActionModeTitle();
                if (this instanceof ContactsActivity) {
                    MenuItem doneItem = getActionMode().getMenu().getItem(0);
                    doneItem.setVisible(getAdapter().getSelectedItemCount() > 1);
                }
            }
        }
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        // inflate menu for subclass
        actionMode.getMenuInflater().inflate(getActionModeMenuId(), menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        // clear selection and finish action mode after item is pressed
        getAdapter().clearSelection();
        getAdapter().setMode(SelectableAdapter.MODE_SINGLE);
        getActionMode().finish();
        return true;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        // reset adapter and actionmode
        mActionMode = null;
        mAdapter.clearSelection();
        mAdapter.setMode(SelectableAdapter.MODE_SINGLE);
    }

    @Override
    public void onItemLongClicked(int position) {
        // start action mode on recycler items long click
        startActionMode();
    }

    public void startActionMode() {
        // start actionmode if it is not already started
        if (mActionMode == null) {
            // change adapters selection mode
            mAdapter.setMode(ContactsAdapter.MODE_MULTI);
            mActionMode = startActionMode(this);
            mActionMode.setTitle(getEmptySelectedTitle());
            if (this instanceof ContactsActivity) {
                mActionMode.getMenu().findItem(R.id.action_group_chat_done).setVisible(false);
            }
        }
    }

    // set the action modes title
    public void setActionModeTitle() {
        // if selected item count greater than 0 show number of selected items and show done icon
        if (mAdapter.getSelectedItemCount() > 0) {
            mActionMode.setTitle(getString(getSelectedTitleId(), mAdapter.getSelectedItemCount()));
        } else {
            mActionMode.setTitle(getEmptySelectedTitle());
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // use subclasses content provider
        return new CursorLoader(this, getContentProviderUri(), null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}
