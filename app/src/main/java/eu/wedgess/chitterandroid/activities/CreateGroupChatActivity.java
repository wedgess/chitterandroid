package eu.wedgess.chitterandroid.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.adapters.GroupParticipantsAdapter;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.tasks.MediaUploadTask;
import eu.wedgess.chitterandroid.views.ChatMainItemDivider;
import eu.wedgess.chitterandroid.views.RoundedImageView;

/**
 * The user is brought to this activity after then have selected multiple members to add
 * to their group chat. Within this activity the group profile image can be set along with
 * the chats name. A list of the groups members is also displayed. This activity is called
 * from {@link ContactsActivity}
 */
public class CreateGroupChatActivity extends AppCompatActivity {

    public static final String TAG = CreateGroupChatActivity.class.getSimpleName();

    private static final String SAVED_INSTANCE_CHAT_NAME = "savedInstanceChatName";
    private static final int IMAGE_REQUEST_CODE = 11;

    private TextInputLayout mChatNameTIL;
    private RoundedImageView mGroupImage;
    private String mGroupImageLocation;
    private ArrayList<Contact> mParticipants;
    private Uri mGroupImageURI;
    private MenuItem mDoneMenuItem;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // save states and orientation change
        outState.putString(SAVED_INSTANCE_CHAT_NAME, mChatNameTIL.getEditText().getText().toString());
        outState.putString(Constants.KEY_GROUP_IMG_LOCATION, mGroupImageLocation);
        outState.putParcelable(TAG, mGroupImageURI);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group_chat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        bindActivity();
        // restore the chat title, group image location and uri on orientation change
        if (savedInstanceState != null) {
            mChatNameTIL.getEditText().setText(savedInstanceState.getString(SAVED_INSTANCE_CHAT_NAME));
            if (mChatNameTIL.getEditText().getText().length() > 1) {
                // set cursor at end of string
                mChatNameTIL.getEditText().setSelection(mChatNameTIL.getEditText().getText().length());
            }
            mGroupImageLocation = savedInstanceState.getString(Constants.KEY_GROUP_IMG_LOCATION);
            mGroupImageURI = savedInstanceState.getParcelable(TAG);
            if (mGroupImageURI != null) {
                try {
                    //Getting the Bitmap from Gallery
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), mGroupImageURI);
                    //Setting the Bitmap to RoundedImageView
                    mGroupImage.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    // bind activities views and listeners
    public void bindActivity() {
        Bundle bundle = getIntent().getExtras();
        /*
         * if participants key is in the bundle get the participants. We cannot use
         * a loader here as the chat is not yet created. This is why a list is used.
         */
        if (bundle != null && bundle.containsKey(ChatRoomActivity.KEY_CONTACTS_LIST)) {
            mParticipants = bundle.getParcelableArrayList(ChatRoomActivity.KEY_CONTACTS_LIST);
            // set recyclerviews adapter
            RecyclerView participantsRecycler = (RecyclerView) findViewById(R.id.recycler_group_participants);
            participantsRecycler.setAdapter(new GroupParticipantsAdapter(mParticipants, this));
            participantsRecycler.addItemDecoration(new ChatMainItemDivider(UIHelper.dpToPx(80, this), this));
        }
        mChatNameTIL = (TextInputLayout) findViewById(R.id.til_group_name);
        mGroupImage = (RoundedImageView) findViewById(R.id.iv_group_chat_img);

        // group image button click listener
        mGroupImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // request permission to WRITE_EXTERNAL_STORAGE, which includes READ permission
                if (ContextCompat.checkSelfPermission(CreateGroupChatActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CreateGroupChatActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 911);
                } else {
                    openMediaPicker();
                }
            }
        });

        mChatNameTIL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // set menu items visibility, show only when text is entered
                if (mDoneMenuItem != null) {
                    mDoneMenuItem.setVisible(!TextUtils.isEmpty(editable));
                }
            }
        });
    }

    private void openMediaPicker() {
        Intent intent = new Intent();
        // the type which we will be picking to upload is image of any format
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        // start chooser intent to pick app to find file with
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_profile_img_chooser)), IMAGE_REQUEST_CODE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_create_group, menu);
        mDoneMenuItem = menu.findItem(R.id.action_create_group);
        if (mChatNameTIL != null) {
            mDoneMenuItem.setVisible(!TextUtils.isEmpty(mChatNameTIL.getEditText().getText()));
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        } else if (item.getItemId() == R.id.action_create_group) {
            // make sure chat name is not empty
            if (TextUtils.isEmpty(mChatNameTIL.getEditText().getText())) {
                mChatNameTIL.setErrorEnabled(true);
                mChatNameTIL.setError(getString(R.string.tile_error_empty_name));
            } else {
                // chat name not empty so create bundle and start ChatRoomActivity
                Intent intent = new Intent(this, ChatRoomActivity.class);
                Bundle bundle = new Bundle();
                // add the owner to the chat group as well
                mParticipants.add(ChitterModel.getInstance(CreateGroupChatActivity.this).getUser(getApplicationContext()));
                bundle.putParcelableArrayList(ChatRoomActivity.KEY_CONTACTS_LIST, mParticipants);
                bundle.putInt(ChatRoomActivity.KEY_NEW_CHAT, ChatRoomActivity.NEW_GROUP_CHAT);
                bundle.putString(ChatRoomActivity.KEY_CHAT_NAME, mChatNameTIL.getEditText().getText().toString());
                // set the image location only if its set
                if (mGroupImageLocation != null && !TextUtils.isEmpty(mGroupImageLocation)) {
                    bundle.putString(Constants.KEY_GROUP_IMG_LOCATION, mGroupImageLocation);
                }
                intent.putExtras(bundle);
                startActivity(intent);
                overridePendingTransition(R.anim.do_not_move, R.anim.activity_exit_to_right);
                finish();
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "Permission callback called in fragment");
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openMediaPicker();
        } else {
            Toast.makeText(this, R.string.toast_msg_no_permission, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {

        // if request code is the image request code and all data is received
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            // get the image URI from the data
            mGroupImageURI = data.getData();
            //Getting the Bitmap from Gallery
            //Setting the Bitmap inside the RoundedImageView
            final int imgDimens = (int) getResources().getDimension(R.dimen.chat_profile_img_size);
            Picasso.with(this)
                    .load(mGroupImageURI)
                    .resize(imgDimens, imgDimens)
                    .placeholder(R.drawable.ic_avatar_placeholder)
                    .centerCrop()
                    .into(mGroupImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            try {
                                mGroupImage.setImageBitmap(MediaStore.Images.Media.getBitmap(getContentResolver(), mGroupImageURI));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    });
            // start media upload task
            new MediaUploadTask(this, new MediaUploadTask.UploadProgressListener() {
                @Override
                public void onUploadComplete(int responseCode, String fileLocation) {
                    // upload successful
                    if (responseCode == 200) {
                        // replace the spaces with encoded spaces
                        mGroupImageLocation = fileLocation.replaceAll(" ", "%20");
                        Toast.makeText(CreateGroupChatActivity.this, R.string.toast_msg_img_upload_success, Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onUploadError(final String error) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(CreateGroupChatActivity.this, error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }).execute(mGroupImageURI);
        }
    }
}
