package eu.wedgess.chitterandroid.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.AsyncQueryHandler;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.util.Linkify;
import android.util.Log;
import android.util.Pair;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import eu.wedgess.chitterandroid.BuildConfig;
import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.adapters.ChatRoomAdapter;
import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.fcm.ChitterFirebaseMessagingService;
import eu.wedgess.chitterandroid.fragments.MessageInfoDialogFragment;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.IncomingMessageHelper;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.model.Message;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.tasks.MediaUploadTask;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;
import eu.wedgess.chitterandroid.views.RoundedImageView;

/**
 * This is the activity which displays the chats messages. It is a subclass of {@link BaseSelectableActivity}
 * when multiple items are selected they can be deleted. When a message is clicked a dialog will show, allowing
 * the user to view the messages info, delete the single message and if the message is not delivered to server
 * (the device was offline when sending the message) the message can be resent. If this is a new chat the chat
 * is not created in the DB locally or sever until a message is sent. This is because if the recipient of the
 * new chat message was to create a chat at the same tim to this user then there would be 2 chats from the same
 * user.
 * <p>
 * Created by Gareth on 20/12/2016.
 */

public class ChatRoomActivity extends BaseSelectableActivity implements View.OnClickListener {

    private final String TAG = ChatRoomActivity.class.getSimpleName();

    private static final int OPEN_CAMERA_REQUEST = 552;
    private static final int IMAGE_REQUEST_CODE = 101;

    public static final String KEY_CHAT_NAME = "keyChatName";
    public static final String KEY_FROM_NOTIF = "keyFromNotif";
    public static final String KEY_CONTACTS_LIST = "keyContactsList";
    public static final String KEY_NEW_CHAT = "keyNewChat";

    public static final int EXISTING_CHAT = 0;
    public static final int NEW_USER_TO_USER_CHAT = 1;
    public static final int NEW_GROUP_CHAT = 2;

    // appbar layout referenced for shared element transitions
    private AppBarLayout mAppBarLayout;
    private ChatRoom mChatRoom;
    private Contact mContact;
    private LinearLayout mRevealLayout;
    private EditText mMessageET;
    private RecyclerView mMessageRecyclerView;
    private TextView mToolbarTitle, mToolbarSubtitle;
    private RoundedImageView mToolbarImage;
    private BroadcastReceiver mNetworkBroadcastReceiver;
    // room type one of the constants above: existing, new_single, new_group
    private int mRoomType = 0;
    private Snackbar mSnackbar;
    private CoordinatorLayout mCoordinatorLayout;
    private String mCapturedImagePath = null;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TAG, mToolbarSubtitle.getText().toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);


        // if coming from clicking on a notification the user might be null. So if it is get it from shared prefs
        if (ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()) == null) {
            // get the user in a BG thread from shared preferences
            new Thread(new Runnable() {
                @Override
                public void run() {
                    User user = Utils.getUserFromPrefs(getApplicationContext());
                    if (user != null) {
                        ChitterModel.getInstance(ChatRoomActivity.this).setUser(user);
                    }
                }
            }).start();
        }

        // initialize all views
        bindActivity();

        if (getSupportActionBar() != null) {
            // disable title as we use our own custom toolbar
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            // show back arrow (home) in toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        // get the bundle from the intent (if any - should be)
        Bundle extras = getIntent().getExtras();
        // if its present
        if (extras != null) {
            bindExtrasToActivity(extras);
        }
        // make sure chat room is not null and it is existing chat before trying to update unread
        // as new chats have no messages to update
        if (mChatRoom != null && mRoomType == EXISTING_CHAT) {
            // set messages in this chat as read on the DB
            setChatMessagesAsRead();
        }
    }

    private void setChatMessagesAsRead() {
        Log.i(TAG, "-------> Running message update as read method");
        CommonQueryHandler queryHandler = new CommonQueryHandler(getContentResolver(), new CommonQueryHandler.QueryCompleteListener() {
            @Override
            public void onComplete(Cursor cursor) {
                if (cursor != null) {
                    Log.i(TAG, "--------->        Cursor not null: " + cursor.getCount());
                    while (cursor.moveToNext()) {
                        Message message = new Message(cursor);
                        Log.i(TAG, "--------->        Updating message status on server: " + message.getMessage());
                        // set the messages in this chat as read on servers DB, if successful local DB is also updated
                        APIRequestUtility.updateMessageStatus(message, Message.STATUS_READ_BY_USER, getApplicationContext());
                    }
                    cursor.close();
                }
            }
        });
        // get users id
        long myId = ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()).getId();
        // run the query to fetch all messages who are delivered in this chat and set them as read
        queryHandler.startQuery(Constants.QUERY_ID_UPDATE_MESSAGES_STATUS, null,
                ChitterContentProvider.CONTENT_URI_CHATS_MESSAGES_STATUS, null,
                null,
                new String[]{String.valueOf(myId)}, null);
    }

    /**
     * Bind the extras that wehere sent with this activity
     *
     * @param extras
     */
    private void bindExtrasToActivity(Bundle extras) {
        // get the values from the bundle
        if (extras.containsKey(KEY_NEW_CHAT)) {
            mRoomType = extras.getInt(KEY_NEW_CHAT);
        } else {
            mRoomType = EXISTING_CHAT;
        }

        String cid;
        ArrayList<Contact> participants = null;
        // room type can be New group, user to user or existing chat
        switch (mRoomType) {
            case NEW_GROUP_CHAT:
                //Log.d(TAG, ">>>>>>>>>>>>>>>>>>> New group chat");
                // generate random ID
                cid = UUID.randomUUID().toString();
                String groupImgUrl = null;
                // get the group image URL form the bundle if it exists
                if (extras.containsKey(Constants.KEY_GROUP_IMG_LOCATION)) {
                    groupImgUrl = extras.getString(Constants.KEY_GROUP_IMG_LOCATION);
                }
                // new groups will have list of participants(Contacts) in the group
                participants = extras.getParcelableArrayList(KEY_CONTACTS_LIST);
                // also will have group chat name in bundle
                String chatName = extras.getString(KEY_CHAT_NAME);
                // set the chatroom object, ready to send to server, don't send initially as what if no messages
                // are sent and then the other users starts a chat, then we have 2 different chats between the
                // same users, unless we delete the chat if no messages are inserted when activity is closed
                // But that way would be messy
                mChatRoom = new ChatRoom(cid, chatName, new ArrayList<Message>(), participants, groupImgUrl,
                        ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()).getId(),
                        ChatRoom.GROUP_CHAT, Utils.getSQLTimestampFormat().format(new Date()));
                // set the toolbar data for the chat room
                setGroupChatToolbarData();
                break;
            case NEW_USER_TO_USER_CHAT:
                //Log.d(TAG, ">>>>>>>>>>>>>>>>>>>>> New user to user chat");
                // generate random  ID
                cid = UUID.randomUUID().toString();
                // its a new user to user chat selected from contacts so get the contact from bundle
                mContact = extras.getParcelable(Constants.KEY_CONTACT);
                if (mContact != null) { // shouldn;t ever be null
                    // set the user to user chat toolbar data
                    setUserToUserToolbarData();
                }
                // init list of partcipants (the contact and the user)
                participants = new ArrayList<>();
                participants.add(ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()));
                participants.add(mContact);
                mChatRoom = new ChatRoom(cid, "", new ArrayList<Message>(), participants, null,
                        ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()).getId(), ChatRoom.USER_TO_USER_CHAT, Utils.getSQLTimestampFormat().format(new Date()));
                //APIRequestUtility.createUserChatIfNotExist(new int[]{1, 2}, this);
                break;
            default:
                // if existing chat
                //Log.d(TAG, ">>>>>>>>>>>>>>>>> Existing chat");
                // get chat room form the extras sent to activity
                mChatRoom = extras.getParcelable(Constants.KEY_CHAT);
                if (mChatRoom.getMembers() == null) {
                    // initialize empty memebers list
                    mChatRoom.setMembers(new ArrayList<Contact>());
                }
                // the message list is null if existing chat so set it
                mChatRoom.setMessages(new ArrayList<Message>());
                if (mChatRoom != null) {
                    // remove any active notifications for this chat
                    IncomingMessageHelper.removeNotificationByChatId(mChatRoom.getId(), getApplicationContext());
                    //Log.d(TAG, ">>>>>>>>>>>>>>>>> Unread count: " + mChatRoom.getUnreadCount());
                    // if message unread count is not 0, set it to 0 in local DB
                    if (mChatRoom.getUnreadCount() != 0) {
                        //Log.d(TAG, "Unread count " + mChatRoom.getUnreadCount() + " NOT 0 so resetting counter");
                        DBHandler.getInstance(this).updateChatUnreadCount(mChatRoom.getId(), true);
                    }
                    // initialize loader
                    getLoaderManager().initLoader(Constants.LOADER_ID_MESSAGES, null, this);
                    getLoaderManager().initLoader(Constants.LOADER_ID_CHAT_CONTACTS, null, this);
                    // if user to user chat, set up toolbar with contact data, otherwise set up group info
                    if (mChatRoom.getChatType() == ChatRoom.USER_TO_USER_CHAT) {
                        mContact = extras.getParcelable(Constants.KEY_CONTACT);
                        if (mContact != null) {
                            setUserToUserToolbarData();
                        }
                    } else {
                        // group chat set up group chat toolbar
                        setGroupChatToolbarData();
                    }
                }
                break;
        }

        //Log.d(TAG, "Chatroom null: " + String.valueOf(mChatRoom == null));
        // set the current chat as this one as we don;t want notifications while inside chat
        ChitterModel.getInstance(ChatRoomActivity.this).setCurrentChat(mChatRoom.getId());
    }

    /**
     * Sets the user to user chat toolbar
     */
    private void setUserToUserToolbarData() {
        // use contact name as chat name
        String name = mContact.getName();
        if (!TextUtils.isEmpty(name)) {
            mToolbarTitle.setText(name);
            if (TextUtils.isEmpty(mContact.getImgUrl())) {
                // get the first character from the name and set as circle drawable
                // set background as set drawable/imageDrawable breaks the shared element transition
                mToolbarImage.setBackground(new CharacterDrawable(name, this));
            } else {
                final int imgDimens = (int) getResources().getDimension(R.dimen.chat_profile_img_size);
                // use picasso to load the images
                // try fetch image from cache first
                Picasso.with(this)
                        .load(mContact.getImgUrl())
                        .resize(imgDimens, imgDimens)
                        .placeholder(R.drawable.ic_avatar_placeholder)
                        .centerCrop()
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(mToolbarImage, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                // on error fetch the image from the server
                                Picasso.with(ChatRoomActivity.this)
                                        .load(mContact.getImgUrl())
                                        .resize(imgDimens, imgDimens)
                                        .placeholder(R.drawable.ic_avatar_placeholder)
                                        .centerCrop()
                                        .into(mToolbarImage);
                            }
                        });
            }
        }
        // set contacts last seen as toolbar title if its not null
        if (mContact.getLastSeen() != null) {
            // set the users status
            if (mContact.getLastSeen().equals(Constants.STATUS_ONLINE)) {
                mToolbarSubtitle.setText(Constants.STATUS_ONLINE);
            } else {
                try {
                    mToolbarSubtitle.setText(Utils.formatLastSeenDate(mContact.getLastSeen()));
                } catch (ParseException e) {
                    Log.e(TAG, "Error parsing last seen date to string: ", e);
                    e.printStackTrace();
                    // error parsing so fall back to un-formatted date
                    mToolbarSubtitle.setText(mContact.getLastSeen());
                }
            }
        }
    }

    /**
     * Set group chat toolbar
     */
    private void setGroupChatToolbarData() {
        String chatTitle = mChatRoom.getTitle();
        if (!TextUtils.isEmpty(chatTitle)) {
            mToolbarTitle.setText(chatTitle);
        }
        // if the profile url is not empty and null then load image from url
        if (mChatRoom.getGroupProfileImgUrl() != null && !TextUtils.isEmpty(mChatRoom.getGroupProfileImgUrl())) {
            // try fetch image from cache first
            final int imgDimens = (int) getResources().getDimension(R.dimen.chat_profile_img_size);
            Picasso.with(this)
                    .load(mChatRoom.getGroupProfileImgUrl())
                    .resize(imgDimens, imgDimens)
                    .placeholder(R.drawable.ic_avatar_placeholder)
                    .centerCrop()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(mToolbarImage, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            // on error fetch the image from the server
                            Picasso.with(ChatRoomActivity.this)
                                    .load(mChatRoom.getGroupProfileImgUrl())
                                    .resize(imgDimens, imgDimens)
                                    .placeholder(R.drawable.ic_avatar_placeholder)
                                    .centerCrop()
                                    .into(mToolbarImage);
                        }
                    });
        } else {
            // else url is empty so use character drawable so long as chat name is not empty
            if (!TextUtils.isEmpty(chatTitle)) {
                mToolbarImage.setImageDrawable(new CharacterDrawable(chatTitle, this));
            }
        }
        // if chat members is empty then get the members from the local DB
        if (mChatRoom.getMembers().isEmpty()) {
            CommonQueryHandler getGroupChatMembersQueryHandler = new CommonQueryHandler(getContentResolver(), new CommonQueryHandler.QueryCompleteListener() {
                @Override
                public void onComplete(Cursor cursor) {
                    if (cursor != null) {
                        ArrayList<Contact> participants = new ArrayList<>();
                        // while cursor has next add contact to participant list
                        while (cursor.moveToNext()) {
                            Contact contact = new Contact(cursor);
                            Log.i(TAG, "Contact: " + contact.toString());
                            // if id is unset (0) then its the owner, so show them first in list
                            if (contact.getId() == 0) {
                                participants.add(0, ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()));
                            } else {
                                // else is a contact so add the contact
                                participants.add(contact);
                            }
                        }
                        // set members in chat room and set the subtitle with the member count
                        mChatRoom.setMembers(participants);
                        int onlineCount = 0;
                        for (Contact contact : participants) {
                            if (contact.getLastSeen() != null && contact.getLastSeen().equals(Constants.STATUS_ONLINE)) {
                                onlineCount++;
                            }
                        }
                        // group chat so set participant count as toolbar sub title
                        if (onlineCount > 1) {
                            mToolbarSubtitle.setText(getString(R.string.title_x_participants_online, participants.size(), onlineCount));
                        } else {
                            mToolbarSubtitle.setText(getString(R.string.title_x_participants, participants.size()));
                        }
                        // close cursor
                        cursor.close();
                    }
                }
            });
            Uri uri = Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_USERS_CHATS_MEMBERS, mChatRoom.getId());
            getGroupChatMembersQueryHandler.startQuery(Constants.QUERY_ID_GET_CHAT_MEMBERS, null, uri, null, null, null, null);
        } else {
            mToolbarSubtitle.setText(getString(R.string.title_x_participants, mChatRoom.getMembers().size()));
        }
    }

    /**
     * A method to bind the activities views and their listeners
     */
    private void bindActivity() {
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_chat_room);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout_chat_room);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar_chat_room);
        setSupportActionBar(mToolbar);
        mToolbarTitle = (TextView) findViewById(R.id.tv_toolbar_title);
        mToolbarSubtitle = (TextView) findViewById(R.id.tv_toolbar_subtitle);
        mToolbarImage = (RoundedImageView) findViewById(R.id.iv_toolbar_chat_icon);
        mMessageRecyclerView = (RecyclerView) findViewById(R.id.recycler_chat_room);
        int chatType = 0;
        // bindActivity is called before we get intents extras due to setting up toolbar
        // so we have to check for the chatroom  extra within the intent this way
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(KEY_NEW_CHAT)) {
                mRoomType = extras.getInt(KEY_NEW_CHAT);
            } else {
                mRoomType = EXISTING_CHAT;
            }
            if (getIntent().getExtras().containsKey(Constants.KEY_CHAT)) {
                ChatRoom chatRoom = getIntent().getExtras().getParcelable(Constants.KEY_CHAT);
                if (chatRoom != null) {
                    // set chat type if the chat is not null
                    chatType = chatRoom.getChatType();
                }
            } else {
                chatType = mRoomType;
            }
        }
        setAdapter(new ChatRoomAdapter(null, chatType, this, this));
        mMessageRecyclerView.setAdapter(getAdapter());
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        llm.setSmoothScrollbarEnabled(true);
        // allows the list to start from the end
        llm.setStackFromEnd(true);
        mMessageRecyclerView.setLayoutManager(llm);
        mRevealLayout = (LinearLayout) findViewById(R.id.reveal_items);
        LinearLayout attachGalleryImg = (LinearLayout) findViewById(R.id.ll_attach_img_gallery);
        attachGalleryImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(ChatRoomActivity.this)) {
                    // make sure we have WRITE/READ EXTERNAL storage permission
                    if (ContextCompat.checkSelfPermission(ChatRoomActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ChatRoomActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 911);
                    } else {
                        // we have permission so open picker
                        openMediaPicker();
                    }
                } else {
                    Toast.makeText(ChatRoomActivity.this, R.string.mm_msg_no_internet, Toast.LENGTH_LONG).show();
                }
            }
        });

        LinearLayout attachCameraImg = (LinearLayout) findViewById(R.id.ll_attach_img_camera);
        attachCameraImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utils.isNetworkAvailable(ChatRoomActivity.this)) {
                    if (ContextCompat.checkSelfPermission(ChatRoomActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(ChatRoomActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 912);
                    } else {
                        dispatchTakePictureIntent();
                    }
                } else {
                    Toast.makeText(ChatRoomActivity.this, R.string.mm_msg_no_internet, Toast.LENGTH_LONG).show();
                }
            }
        });

        LinearLayout attachmentsClose = (LinearLayout) mRevealLayout.findViewById(R.id.llayout_close_attachments);
        final ImageButton sendMsgBTN = (ImageButton) findViewById(R.id.ib_send_msg);
        final ImageButton attachFileBTN = (ImageButton) findViewById(R.id.ib_attach_file);
        mMessageET = (EditText) findViewById(R.id.et_message);


        // if send on enter button from settings pref is checked
        if (Utils.getBooleanPref(Constants.PREF_SEND_ON_ENTER, false, ChatRoomActivity.this)) {
            // has to be input type text
            mMessageET.setInputType(InputType.TYPE_CLASS_TEXT);
            mMessageET.setMaxLines(1);
            mMessageET.setImeOptions(EditorInfo.IME_ACTION_SEND);
            mMessageET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    String str = v.getText().toString();
                    // check string is not null and first character is not enter key, this avoid sending
                    // blank messages
                    if (str.length() != 0 && str.charAt(0) != '\n' && (actionId == EditorInfo.IME_ACTION_SEND)) {
                        Log.i("TST", "String length: " + str.length() + " string is: " + str);
                        sendMessage();
                    } else {

                        Log.i("TST", "NOT DONE ");
                    }
                    return true;
                }
            });
        }

        // set click listeners
        sendMsgBTN.setOnClickListener(this);
        attachmentsClose.setOnClickListener(this);
        attachFileBTN.setOnClickListener(this);
        mToolbarImage.setOnClickListener(this);

        // on text changed listener to hide/show send button and attach button depending on if text is empty or not
        mMessageET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                // hide/show the send button depending on if text is empty or not
                if (TextUtils.isEmpty(charSequence)) {
                    // showing send button and hiding attachment button
                    // we don;t want to keep animating the buttons on each text change only if the state has changed
                    if (attachFileBTN.getScaleX() == 0.3f && sendMsgBTN.getScaleX() == 1f) {
                        // get attch button animator set
                        AnimatorSet setAttach = UIHelper.getSendAnimatorSet(attachFileBTN, 0.3f, 1f);
                        setAttach.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                super.onAnimationStart(animation);
                                // when animation ends hide the attach button
                                attachFileBTN.setVisibility(View.VISIBLE);
                            }
                        });

                        // animator set for send button so animations play at same time
                        AnimatorSet setSend = UIHelper.getSendAnimatorSet(sendMsgBTN, 1f, 0.3f);
                        setSend.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                // when animation ends hide the send button or we still get its clicks
                                sendMsgBTN.setVisibility(View.INVISIBLE);
                            }
                        });
                        // play the send button animation first then the attch animation
                        AnimatorSet set = new AnimatorSet();
                        set.playSequentially(setSend, setAttach);
                        set.start();
                    }
                } else {
                    // showing attach button and hiding send button
                    // we don;t want to keep animating the buttons on each text change only if the state has changed
                    if (attachFileBTN.getScaleX() == 1f && sendMsgBTN.getScaleX() == 0.3f) {
                        // animate x and y scale of attach button
                        AnimatorSet setAttach = UIHelper.getSendAnimatorSet(attachFileBTN, 1f, 0.3f);
                        setAttach.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                super.onAnimationEnd(animation);
                                // when animation ends hide the attach button
                                attachFileBTN.setVisibility(View.INVISIBLE);
                            }
                        });

                        // animator set for send button so animations play at same time
                        AnimatorSet setSend = UIHelper.getSendAnimatorSet(sendMsgBTN, 0.3f, 1f);
                        setSend.addListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                super.onAnimationStart(animation);
                                // at the start of the animation set the button to visible
                                sendMsgBTN.setVisibility(View.VISIBLE);
                            }
                        });
                        // play animations one after another
                        AnimatorSet set = new AnimatorSet();
                        set.playSequentially(setAttach, setSend);
                        set.start();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                // if the text contains links then highlight them
                Linkify.addLinks(editable, Linkify.WEB_URLS);
            }
        });

        // set up network change receiver to display snackbar if internet is not available
        mNetworkBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Utils.isNetworkAvailable(context)) {
                    if (mSnackbar != null) {
                        if (mSnackbar.isShown()) {
                            mSnackbar.dismiss();
                        }
                        mSnackbar = null;
                    }
                } else {
                    mSnackbar = Snackbar.make(mCoordinatorLayout,
                            R.string.snackbar_msg_no_internet, Snackbar.LENGTH_LONG);
                    mSnackbar.show();
                }
            }
        };
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // on home button press finish this activity
        if (item.getItemId() == android.R.id.home) {
            ChatRoomActivity.this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
        // use my own animations on finish
        overridePendingTransition(R.anim.do_not_move, R.anim.activity_exit_to_right);
    }

    @Override
    public void onItemClicked(int position) {
        // if aitem is clicked and action mode is enabled toggle the items selection
        if (getActionMode() != null) {
            getAdapter().toggleSelection(position);
            setActionModeTitle();
        } else {
            // no actionmode so show the bottom sheet dialog
            showBottomsheetDialog(position);
        }
    }

    /**
     * When messages are pressed and the adapters mode is set to Single then a dialog
     * pops up allowing users to view message info, resend(if unsent) and delete the message
     *
     * @param postion
     */
    private void showBottomsheetDialog(int postion) {
        // get message from adapters position
        final Message message = (Message) getAdapter().getItemAtPosition(postion);
        final Contact contact = ((ChatRoomAdapter) getAdapter()).getContactAtPosition(postion);
        // initialize bottom sheets views
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
        View sheetView = getLayoutInflater().inflate(R.layout.bottomsheet_message, null);
        LinearLayout infoOption = (LinearLayout) sheetView.findViewById(R.id.bottomsheet_info_item_ll);
        LinearLayout resendMsgOption = (LinearLayout) sheetView.findViewById(R.id.bottomsheet_resend_ll);
        LinearLayout copyMsgTxtOption = (LinearLayout) sheetView.findViewById(R.id.bottomsheet_copy_item_ll);
        LinearLayout deleteOption = (LinearLayout) sheetView.findViewById(R.id.bottomsheet_delete_item_ll);

        // ONLY if message is pending show the resend option
        if (message.getStatus() == Message.STATUS_PENDING) {
            resendMsgOption.setVisibility(View.VISIBLE);
            ImageView icon = (ImageView) resendMsgOption.findViewById(R.id.iv_resend_msg);
            TextView text = (TextView) resendMsgOption.findViewById(R.id.tv_resend_msg);
            if (icon != null) {
                icon.setVisibility(View.VISIBLE);
            }
            if (text != null) {
                text.setVisibility(View.VISIBLE);
            }
        }

        // resend message click listener
        resendMsgOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // send message request
                APIRequestUtility.sendMessageRequest(message, ChatRoomActivity.this);
                bottomSheetDialog.dismiss();
            }
        });

        // delete click listener
        deleteOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // set the message as deleted and update in DB
                message.setDeleted(true);
                DBHandler.getInstance(ChatRoomActivity.this).updateMessage(message);
                bottomSheetDialog.dismiss();
            }
        });

        // info click listener
        infoOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MessageInfoDialogFragment.newInstance(message, contact.getName()).show(getFragmentManager(), TAG);
                bottomSheetDialog.dismiss();
            }
        });

        // copy message text click listener
        copyMsgTxtOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // copy message content to clipboard
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(getString(R.string.app_name), message.getMessage());
                clipboard.setPrimaryClip(clip);
                // display toast telling user text has been copied
                Toast.makeText(ChatRoomActivity.this, R.string.toast_msg_text_copied_to_clipboard,
                        Toast.LENGTH_SHORT).show();
                // close bottomsheet dialog
                bottomSheetDialog.dismiss();
            }
        });

        // use custom view for bottom sheet then show it
        bottomSheetDialog.setContentView(sheetView);
        bottomSheetDialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_toolbar_chat_icon:
                // only allow the user to view chat info when chat has been created locally (a message sent or received)
                if (mRoomType == EXISTING_CHAT) {
                    // were going to open contact info activity
                    Intent intent = null;
                    if (mChatRoom != null) {
                        Bundle bundle = new Bundle();
                        View statusBar = findViewById(android.R.id.statusBarBackground);
                        View navigationBar = findViewById(android.R.id.navigationBarBackground);

                        List<Pair<View, String>> pairs = new ArrayList<>();
                        // navbar and status bar can be null on some samsung and One+ devices so,
                        // only add them if they exist (not null)
                        if (navigationBar != null) {
                            Pair.create(navigationBar, Window.NAVIGATION_BAR_BACKGROUND_TRANSITION_NAME);
                        }
                        if (statusBar != null) {
                            pairs.add(Pair.create(statusBar, Window.STATUS_BAR_BACKGROUND_TRANSITION_NAME));
                        }
                        pairs.add(Pair.create((View) mAppBarLayout, mAppBarLayout.getTransitionName()));
                        pairs.add(Pair.create((View) mToolbarTitle, mToolbarTitle.getTransitionName()));
                        pairs.add(Pair.create((View) mToolbarSubtitle, mToolbarSubtitle.getTransitionName()));
                        pairs.add(Pair.create((View) mToolbarImage, mToolbarImage.getTransitionName()));

                        // activity options to make the shared element transition with appbar and items within the toolbar
                        final ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                                this,
                                pairs.toArray(new Pair[pairs.size()]));
                        // if user to user chat get the recipient
                        if (mChatRoom.getChatType() == ChatRoom.USER_TO_USER_CHAT) {
                            // if recipient not null set the recipient in bundle
                            if (mContact != null) {
                                intent = new Intent(this, ContactInfoActivity.class);
                                // pass contact and chatroom to next activity
                                bundle.putParcelable(Constants.KEY_CONTACT, mContact);
                                bundle.putParcelable(Constants.KEY_CHAT, mChatRoom);
                            }
                        } else {
                            // group chat jsut pass the chatroom object
                            bundle.putParcelable(Constants.KEY_CHAT, mChatRoom);
                            intent = new Intent(this, GroupInfoActivity.class);
                        }
                        if (intent != null) {
                            intent.putExtras(bundle);
                            startActivityForResult(intent, Constants.CHAT_INFO_REQ_CODE, options.toBundle());
                        }
                    }
                }
                break;
            case R.id.llayout_close_attachments:
                closeAttachmentsLayout();
                break;
            case R.id.ib_attach_file:
                // set up circular reveal animation for displaying attachments layout
                int cx2 = (mRevealLayout.getLeft() + mRevealLayout.getRight());
                int cy2 = mRevealLayout.getBottom();
                float finalRadius = (float) Math.hypot(cx2, cy2);
                Animator animOpen = ViewAnimationUtils.createCircularReveal(mRevealLayout, cx2, cy2, 0, finalRadius);
                // show reveal layout
                mRevealLayout.setVisibility(View.VISIBLE);
                animOpen.start();
                break;
            case R.id.ib_send_msg:
                // so long as message text is not empty
                if (!TextUtils.isEmpty(mMessageET.getText())) {
                    sendMessage();
                } else {
                    // user is offline, we cannot create chats while offline so show toast message and don't display message
                    Toast.makeText(getApplicationContext(), R.string.toast_msg_cannot_create_while_offline, Toast.LENGTH_LONG).show();
                }
        }
    }

    private void closeAttachmentsLayout() {
        // set up circular reveal animation for closing attachments layout
        int cx = (mRevealLayout.getLeft() + mRevealLayout.getRight());
        int cy = mRevealLayout.getBottom();
        // get the initial radius for the clipping circle
        float initialRadius = (float) Math.hypot(cx, cy);
        Animator animClose = ViewAnimationUtils.createCircularReveal(mRevealLayout, cx, cy, initialRadius, 0);
        animClose.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                // hide the reveal layout
                mRevealLayout.setVisibility(View.INVISIBLE);
            }
        });
        animClose.start();
    }

    private void sendMessage() {
        // get the date time, this doesn;t matter as server will set the timestamp but this
        // is so that if the device is offline and the message is pending the date is still correct
        String dateTime = Utils.getSQLTimestampFormat().format(new Date());
        // create the message with a random String id
        final Message message = new Message(UUID.randomUUID().toString(), mChatRoom.getId(),
                ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()).getId(),
                mMessageET.getText().toString(), dateTime, Message.STATUS_PENDING, Message.TYPE_TEXT);
        mChatRoom.addMessage(message);
        // if existing chat then just send the message normally no chats need to be created
        if (mRoomType == EXISTING_CHAT) {
            DBHandler.getInstance(this).addMessage(message, ChatRoomActivity.this);
            getContentResolver().notifyChange(getContentProviderUri(), null);
            APIRequestUtility.sendMessageRequest(message, this);
            mMessageET.setText("");
            MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.soundout);
            mp.start();
        } else {
            // make sure user is online before sending if a new chat, as chats cannot be created
            // if user is offline
            if (Utils.isNetworkAvailable(getApplicationContext())) {
                // if a new chat then create either user to user chat locally and on server
                if (mRoomType == NEW_USER_TO_USER_CHAT || mRoomType == NEW_GROUP_CHAT) {
                    // if user to user chat
                    if (mRoomType == NEW_USER_TO_USER_CHAT) {
                        checkChatExistOrCreate(message);
                    } else {
                        APIRequestUtility.createChat(mChatRoom, ChatRoomActivity.this, new APIRequestUtility.CommonServerCallback() {
                            @Override
                            public void onComplete(boolean error) {
                                if (!error) {
                                    // send message after the chat has been created
                                    APIRequestUtility.sendMessageRequest(message, ChatRoomActivity.this);
                                }
                            }

                            @Override
                            public void onError(String error) {

                            }
                        });
                        // add message to local DB
                        DBHandler.getInstance(this).addMessage(message, ChatRoomActivity.this);
                    }
                    // chat ID was empty so only initialize loader here or chatId is NULL
                    getLoaderManager().initLoader(Constants.LOADER_ID_MESSAGES, null, this);
                    // reset room type so we don't create another chat
                    mRoomType = EXISTING_CHAT;
                }
                mMessageET.setText("");
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.soundout);
                mp.start();
            }
        }
    }

    private void checkChatExistOrCreate(final Message message) {
        // check if chat exists locally, if not create it on the server
        CommonQueryHandler chatExistQueryHandler = new CommonQueryHandler(ChatRoomActivity.this.getApplicationContext()
                .getContentResolver(), new CommonQueryHandler.QueryCompleteListener() {
            @Override
            public void onComplete(Cursor cursor) {
                if (cursor != null) {
                    boolean chatExists = false;
                    while (cursor.moveToNext()) {
                        //String cid = cursor.getString(cursor.getColumnIndex(DBConstants.COLUMN_USERS_CHATS_CID));
                        int member_count = cursor.getInt(cursor.getColumnIndex(DBConstants.COLUMN_USERS_CHATS_MEMBERS_COUNT));
                        // user to user will have 2 members
                        if (member_count == 2) {
                            chatExists = true;
                            break;
                        }
                    }
                    if (!chatExists) {
                        //Log.i(TAG, "Creating user chats as it doesn;t exist");
                        APIRequestUtility.createChat(mChatRoom, ChatRoomActivity.this, new APIRequestUtility.CommonServerCallback() {
                            @Override
                            public void onComplete(boolean error) {
                                // callback for when createChat has been completed
                                if (!error) {
                                    // if no error then create message locally
                                    DBHandler.getInstance(ChatRoomActivity.this).addMessage(message, ChatRoomActivity.this);
                                    // send message to server
                                    APIRequestUtility.sendMessageRequest(message, ChatRoomActivity.this);
                                    // then clear message text in ET
                                    mMessageET.setText("");
                                }
                            }

                            @Override
                            public void onError(String error) {

                            }
                        });
                    } else {
                        // chat exists just send message as normal
                        DBHandler.getInstance(ChatRoomActivity.this).addMessage(message, ChatRoomActivity.this);
                        APIRequestUtility.sendMessageRequest(message, ChatRoomActivity.this);
                        mMessageET.setText("");
                    }
                    //else {
                    //Log.i(TAG, "user chat already exists");
                    // }
                    cursor.close();
                }
            }
        });
        chatExistQueryHandler.startQuery(Constants.QUERY_ID_CREATE_CHAT, null, ChitterContentProvider.CONTENT_URI_USERS_CHATS_EXIST, null, null,
                new String[]{String.valueOf(mContact.getId()), String.valueOf(ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()).getId())}, null);
    }

    @Override
    protected void onResume() {
        // if contact is saved we need to update the contact name in DB
        if (mContact != null) {
            UpdateContactNameQueryHandler updateContactNameInToolbarTitleQuery =
                    new UpdateContactNameQueryHandler(getContentResolver(), mContact, mToolbarTitle);
            updateContactNameInToolbarTitleQuery.startQuery(
                    Constants.QUERY_ID_UPDATE_CONTACT_NAME, null, ChitterContentProvider.CONTENT_URI_CONTACT_ITEM, null, null,
                    new String[]{String.valueOf(mContact.getId())}, null);
        }
        super.onResume();
        Log.i(TAG, "ON RESUMED --------->");
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(ChitterFirebaseMessagingService.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(messageBroadcastReceiver, filter);
        // register network change receiver
        registerReceiver(mNetworkBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageBroadcastReceiver);
        unregisterReceiver(mNetworkBroadcastReceiver);
    }

    /**
     * Callback for when message is received from FCM server
     */
    private BroadcastReceiver messageBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Log.i(TAG, "Received result in receiver: ChatRoomActivity");
            if (intent.hasExtra(ChitterFirebaseMessagingService.BROADCAST_MESSAGE)) {
                Message message = intent.getParcelableExtra(ChitterFirebaseMessagingService.BROADCAST_MESSAGE);
                //Log.i(TAG, "Receiver received message: " + message.toString());
                // play sound for new message
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.soundin);
                mp.start();
                // notify of item change
                getContentResolver().notifyChange(getContentProviderUri(), null);
                //if (mChatRoom != null) {
                //Log.i(TAG, "Setting (Receiver) message:" + message.getId() + " status as: " + message.getStatus());
                //setChatMessagesAsRead();
                Log.i(TAG, "Received update of message status");
                APIRequestUtility.updateMessageStatus(message, Message.STATUS_READ_BY_USER, getApplicationContext());
                // ensure no notification doesn't shows up while in this chat -- shouldn't anyway
                IncomingMessageHelper.removeNotificationByChatId(message.getCid(), getApplicationContext());
                //}
            }
            // broadcast message is to update a users last seen status
            else if (intent.hasExtra(ChitterFirebaseMessagingService.BROADCAST_BUNDLE)) {
                // update a users last seen
                Bundle bundle = intent.getBundleExtra(ChitterFirebaseMessagingService.BROADCAST_BUNDLE);
                if (bundle != null) {
                    // get the status from the bundle
                    String status = bundle.getString(ChitterFirebaseMessagingService.BROADCAST_STATUS);
                    // get the uid of the users whos status has changed
                    long uid = bundle.getLong(ChitterFirebaseMessagingService.BROADCAST_UID);
                    if (mChatRoom.getMembers() != null && !mChatRoom.getMembers().isEmpty()) {
                        // loop through each chat member and if its the member sending the broadcast, update their last seen
                        // this is needed to show correct number of online users in GroupInfoActivity after orientation change
                        for (int i = 0; i < mChatRoom.getMembers().size(); i++) {
                            if (mChatRoom.getMembers().get(i).getId() == uid) {
                                //Log.i(TAG, "Received update in receiver: updating user: "
                                //        + mChatRoom.getMembers().get(i).getName() + " status to: " + status);
                                mChatRoom.getMembers().get(i).setLastSeen(status);
                            }
                        }
                        // notify the loader of the changes, which will update toolbar
                        Uri chatMembersURI = Uri.withAppendedPath(
                                ChitterContentProvider.CONTENT_URI_USERS_CHATS_MEMBERS, mChatRoom.getId());
                        getContentResolver().notifyChange(chatMembersURI, null);
                    }
                    // if the contacts not null and it is the user who send the broadcast update their last seen
                    else if (mContact != null && mContact.getId() == uid) {
                        mContact.setLastSeen(status);
                        if (status != null && status.equals(Constants.STATUS_ONLINE)) {
                            mToolbarSubtitle.setText(mContact.getLastSeen());
                        } else {
                            try {
                                mToolbarSubtitle.setText(Utils.formatLastSeenDate(mContact.getLastSeen()));
                            } catch (ParseException e) {
                                Log.e(TAG, "Error parsing last seen date to string: ", e);
                                e.printStackTrace();
                                mToolbarSubtitle.setText(mContact.getLastSeen());
                            }
                        }
                    }
                }
            }
        }
    };

    /**
     * Smooth scroll to bottom of RecyclerView
     */
    private void smoothScrollToBottom() {
        Log.i(TAG, "Smooth scroll called");
        // scroll to bottom of RecyclerView
        if (getAdapter() != null && getAdapter().getItemCount() > 0) {
            mMessageRecyclerView.smoothScrollToPosition(getAdapter().getItemCount());
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        // messages loader let BaseInfoActivity handle it
        if (loader.getId() == Constants.LOADER_ID_MESSAGES) {
            super.onLoadFinished(loader, data);
            // when the cursor is loaded scroll to bottom of recycler, caused by new message
            smoothScrollToBottom();
        } else {
            // if user to user chat we want to display the last seen status of the person we are sending
            // the message to, so for each contact within the chat ...
            if (mChatRoom != null) {
                if (mChatRoom.getChatType() == ChatRoom.USER_TO_USER_CHAT) {
                    // get the owner id as there is only going to be two users in the chat (user to user)
                    long ownerId = ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()).getId();
                    // while there is a next
                    while (data.moveToNext()) {
                        // get the contact from the cursor data
                        Contact contact = new Contact(data);
                        Log.i(TAG, "Looping contact: " + contact.toString());
                        // if its not the user(owner) then seb the toolbars sub title
                        if (contact.getId() != ownerId) {
                            try {
                                if (contact.getLastSeen() != null) {
                                    if (contact.getLastSeen().equals(Constants.STATUS_ONLINE)) {
                                        mToolbarSubtitle.setText(Constants.STATUS_ONLINE);
                                    } else {
                                        mToolbarSubtitle.setText(Utils.formatLastSeenDate(contact.getLastSeen()));
                                    }
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                                // parse exception so just display the string unformatted
                                mToolbarSubtitle.setText(contact.getLastSeen());
                            }
                        }
                    }
                } else {
                    // this member is online
                    int onlineCount = 1;
                    while (data.moveToNext()) {
                        String lastSeen = (data.getString(data.getColumnIndex(DBConstants.COLUMN_CONTACTS_LAST_SEEN)));
                        if (lastSeen != null && lastSeen.equals(Constants.STATUS_ONLINE)) {
                            onlineCount++;
                        }
                    }
                    // group chat so set participant count as toolbar sub title
                    if (onlineCount > 1) {
                        mToolbarSubtitle.setText(getString(R.string.title_x_participants_online, data.getCount(), onlineCount));
                    } else {
                        mToolbarSubtitle.setText(getString(R.string.title_x_participants, data.getCount()));
                    }
                }
            }
        }
    }

    @Override
    public int getActionModeMenuId() {
        return R.menu.cab_chat_room;
    }

    @Override
    public String getEmptySelectedTitle() {
        return getString(R.string.title_cab_select_messages);
    }

    @Override
    public int getSelectedTitleId() {
        return R.string.title_x_messages;
    }

    @Override
    public Uri getContentProviderUri() {
        return Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_CHATS_MESSAGES, mChatRoom.getId());
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.action_delete_messages) {
            // if items are selected
            if (getAdapter().getSelectedItemCount() > 0) {
                // get all the selected items position in adapter
                List<Integer> selectedItems = getAdapter().getSelectedItems();
                // for each of the positions
                for (Integer pos : selectedItems) {
                    // get the message
                    Message message = (Message) getAdapter().getItemAtPosition(pos);
                    if (message != null) {
                        // set the message as deleted
                        message.setDeleted(true);
                        // update message in DB
                        DBHandler.getInstance(ChatRoomActivity.this).updateMessage(message);
                    }
                }
                // call super to close multi-selection mode and actionmode
                super.onActionItemClicked(actionMode, menuItem);
            } else {
                Toast.makeText(this, R.string.toast_msg_not_items_selected, Toast.LENGTH_SHORT).show();
            }
            return true;
        }
        return false;
    }

    private static class CommonQueryHandler extends AsyncQueryHandler {

        interface QueryCompleteListener {
            void onComplete(Cursor cursor);
        }

        // hold a weak reference, to the listener
        private WeakReference<QueryCompleteListener> queryCompleteListener;

        CommonQueryHandler(ContentResolver cr, QueryCompleteListener queryCompleteListener) {
            super(cr);
            this.queryCompleteListener = new WeakReference<>(queryCompleteListener);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            super.onQueryComplete(token, cookie, cursor);
            QueryCompleteListener listener = queryCompleteListener.get();
            if (listener != null) {
                listener.onComplete(cursor);
            } else {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }

    private static class UpdateContactNameQueryHandler extends AsyncQueryHandler {

        private WeakReference<Contact> contactWeakReference;
        private WeakReference<TextView> toolbarTitleWeakReference;

        UpdateContactNameQueryHandler(ContentResolver cr, Contact contact, TextView toolbarTitle) {
            super(cr);
            this.contactWeakReference = new WeakReference<>(contact);
            this.toolbarTitleWeakReference = new WeakReference<>(toolbarTitle);
        }

        @Override
        protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
            super.onQueryComplete(token, cookie, cursor);

            Contact contact = contactWeakReference.get();
            TextView toolbarTitleTV = toolbarTitleWeakReference.get();

            if (cursor != null) {
                if (contact != null && toolbarTitleTV != null) {
                    // if contact exist (always should)
                    if (cursor.moveToFirst()) {
                        contact = new Contact(cursor);
                        // if previous contact name and new one are not the same update title
                        if (!toolbarTitleTV.getText().equals(contact.getName())) {
                            toolbarTitleTV.setText(contact.getName());
                        }
                    }
                }
                cursor.close();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // if loader id is the messages let the superclass deal with it
        if (id == Constants.LOADER_ID_MESSAGES) {
            return super.onCreateLoader(id, args);
        } else {
            // otherwise its an update to the chat members so reload them
            Uri providerUri = Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_USERS_CHATS_MEMBERS, mChatRoom.getId());
            return new CursorLoader(this, providerUri, null, null, null, null);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        // let superclass deal with resetting loader messages, do not reset members though
        if (loader.getId() == Constants.LOADER_ID_MESSAGES) {
            super.onLoaderReset(loader);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "Permission callback called");
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == 911) {
                openMediaPicker();
            } else if (requestCode == 912) {
                dispatchTakePictureIntent();
            }
        } else {
            Toast.makeText(ChatRoomActivity.this, R.string.toast_msg_no_permission, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.CHAT_INFO_REQ_CODE && resultCode == Activity.RESULT_OK) {
            mChatRoom = data.getParcelableExtra(Constants.KEY_CHAT);
        } else if (requestCode == OPEN_CAMERA_REQUEST) {
            if (resultCode == Activity.RESULT_OK && mCapturedImagePath != null) {
                Uri filePath = Uri.fromFile(new File(mCapturedImagePath));
                uploadImageTask(filePath);
                mCapturedImagePath = null; // reset the image path as its now deleted
            }
        } else if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            // hide register button and show progressbar while uploading image
            //mUploadImgProgress.setVisibility(View.VISIBLE);
            Uri filePath = data.getData();
            uploadImageTask(filePath);
        }
    }

    private void uploadImageTask(Uri filePath) {
        new MediaUploadTask(ChatRoomActivity.this, new MediaUploadTask.UploadProgressListener() {
            @Override
            public void onUploadComplete(int responseCode, String fileLocation) {
                // if successful upload
                if (responseCode == 200) {
                    // replace the spaces with encoded spaces
                    String decodedFileLocation = fileLocation.replaceAll(" ", "%20");
                    String dateTime = Utils.getSQLTimestampFormat().format(new Date());
                    final Message message = new Message(UUID.randomUUID().toString(), mChatRoom.getId(),
                            ChitterModel.getInstance(ChatRoomActivity.this).getUser(getApplicationContext()).getId(),
                            "", dateTime, Message.STATUS_PENDING, Message.TYPE_IMG);
                    message.setImgUrl(decodedFileLocation);
                    mChatRoom.addMessage(message);
                    // if existing chat then just send the message normally no chats need to be created
                    //if (mRoomType == EXISTING_CHAT) {
                    DBHandler.getInstance(ChatRoomActivity.this).addMessage(message, ChatRoomActivity.this);
                    getContentResolver().notifyChange(getContentProviderUri(), null);
                    APIRequestUtility.sendMessageRequest(message, ChatRoomActivity.this);
                    mMessageET.setText("");
                    MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.soundout);
                    mp.start();
                    closeAttachmentsLayout();
                    //mNewProfileImgUrl = decodedFileLocation;
                    //Toast.makeText(ChatRoomActivity.this, R.string.toast_msg_img_upload_success, Toast.LENGTH_LONG).show();
                }
                // show register button and hide progressbar upload finished due to error
                //mUploadImgProgress.setVisibility(View.GONE);
            }

            @Override
            public void onUploadError(final String error) {
                // show register button and hide progressbar upload finished due to error
                //mUploadImgProgress.setVisibility(View.GONE);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ChatRoomActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                });
            }
        }).execute(filePath);
    }

    // open media picker chooser to select image
    private void openMediaPicker() {
        Intent intent = new Intent();
        // the type which we will be picking to upload is image of any format
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_profile_img_chooser)), IMAGE_REQUEST_CODE);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.d(TAG, "Error creating image mCapturedImagePath: " + ex.getMessage());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                mCapturedImagePath = photoFile.getAbsolutePath();
//                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
//                        Uri.fromFile(photoFile));
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        FileProvider.getUriForFile(ChatRoomActivity.this, BuildConfig.APPLICATION_ID + ".provider", photoFile));
                //uriphoto = Uri.fromFile(photoFile);
                startActivityForResult(takePictureIntent, OPEN_CAMERA_REQUEST);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image mCapturedImagePath name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a mCapturedImagePath: path for use with ACTION_VIEW intents
        Log.d(TAG, "File created:" + image.getAbsolutePath());
        return image;
    }
}