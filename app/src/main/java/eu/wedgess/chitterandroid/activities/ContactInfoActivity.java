package eu.wedgess.chitterandroid.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.fragments.CreateContactDialogFragment;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;

/**
 * Subclass of {@link BaseInfoActivity}, used for displaying a contacts details from within
 * a user to user chat by clicking on the contacts image from within the toolbar.
 */
public class ContactInfoActivity extends BaseInfoActivity implements CreateContactDialogFragment.OnCreateDialogCloseListener {

    public static final String TAG = ContactInfoActivity.class.getSimpleName();

    private Contact mContact;
    private ChatRoom mChatRoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        // get the contact and chatroom from the bundle
        if (bundle != null) {
            if (bundle.containsKey(Constants.KEY_CONTACT)) {
                mContact = bundle.getParcelable(Constants.KEY_CONTACT);
            }
            if (bundle.containsKey(Constants.KEY_CHAT)) {
                mChatRoom = bundle.getParcelable(Constants.KEY_CHAT);
            }
        }
        // IMPORTANT - don't call super classes onCreate before getting values from bundle
        super.onCreate(savedInstanceState);
    }

    /**
     * Both {@link ContactInfoActivity} & {@link GroupInfoActivity} use the same base layout
     * which is the Toolbar containing avatar, title and subtitle, so bind the stub layout
     */
    @Override
    public void bindStubLayout() {
        TextView contactStatus = (TextView) findViewById(R.id.tv_contact_status);
        TextView contactNumber = (TextView) findViewById(R.id.tv_contact_phone_num);
        TextView contactJoinDate = (TextView) findViewById(R.id.tv_contact_join_date);

        contactStatus.setText(mContact.getStatus());

        if (mContact != null) {
            //Log.i(TAG, mContact.toString());
            // default as unknown
            String createdAt = "Unknown";
            // if contact vreated at is not null
            if (mContact.getCreatedAt() != null) {
                try {
                    // try format the created date
                    createdAt = Utils.getJoinDate(mContact.getCreatedAt());
                } catch (ParseException e) {
                    e.printStackTrace();
                    // if parse exception just set default format
                    createdAt = mContact.getCreatedAt();
                }
            }
            // set contacts join date as there created at
            contactJoinDate.setText(createdAt);

            // set the contacts phone number
            contactNumber.setText(mContact.getPhoneNumber());

            // get contacts last seen
            String lastSeen = mContact.getLastSeen();
            try {
                // if last seen is not Online then format the last seen date
                if (!lastSeen.equals(Constants.STATUS_ONLINE)) {
                    lastSeen = Utils.formatLastSeenDate(mContact.getLastSeen());
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            CharacterDrawable characterDrawable = null;
            // if img url is empty use the contacts name for a drawable
            if (TextUtils.isEmpty(mContact.getImgUrl())) {
                characterDrawable = new CharacterDrawable(mContact.getName(), this);
            } else {
                // otherwise load image with picasso form URL
                final int imgDimens = (int) getResources().getDimension(R.dimen.chat_profile_img_size);
                // try fetch image from cache first
                Picasso.with(this)
                        .load(mContact.getImgUrl())
                        .resize(imgDimens, imgDimens)
                        .placeholder(R.drawable.ic_avatar_placeholder)
                        .centerCrop()
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(getToolbarLogoIV(), new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                // fetch image from server as its not cached
                                Picasso.with(ContactInfoActivity.this)
                                        .load(mContact.getImgUrl())
                                        .resize(imgDimens, imgDimens)
                                        .placeholder(R.drawable.ic_avatar_placeholder)
                                        .centerCrop()
                                        .into(getToolbarLogoIV());
                            }
                        });
            }
            // set the toolbar data
            setCollapseToolbar(characterDrawable, mContact.getName(), lastSeen);

            //Log.i(TAG, "Contact is not null");

            // if contact doesn;t exist on the decive
            if (!mContact.existOnDevice()) {
                // get fab set on click listener and set icon, this allows the user to add contact to device contacts
                // if the contact does not exist in device contacts
                getFAB().setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_contact_add_white));
                getFAB().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (ContextCompat.checkSelfPermission(ContactInfoActivity.this, Manifest.permission.WRITE_CONTACTS)
                                != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(ContactInfoActivity.this,
                                    new String[]{Manifest.permission.WRITE_CONTACTS}, Constants.WRITE_CONTACT_START);
                        } else {
                            CreateContactDialogFragment.newInstance(mContact).show(getFragmentManager(), TAG);
                        }
                    }
                });
            }

            // character drawables are set as the BG while images are set as drawables
            // so asume that if the background is null then allow the user to view the img full size
            if (getToolbarLogoIV().getBackground() == null) {
                getToolbarLogoIV().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startViewImageActivity(mContact.getImgUrl());
                    }
                });
            }

            // allow for the chat between this contacts to be muted or not
            Switch mutedSwitch = (Switch) findViewById(R.id.switch_contact_muted);
            mutedSwitch.setChecked(mChatRoom.isMuted());
            mutedSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (mChatRoom != null) {
                        mChatRoom.setMuted(isChecked);
                        // update chat in DB
                        DBHandler.getInstance(ContactInfoActivity.this).updateChatRoom(mChatRoom);
                    }
                }
            });
        }
    }

    /**
     * Callback for when a new contact has been inserted into user device contacts
     */
    @Override
    public void onDialogComplete() {
        // the contact has been added so change FAb back to return to chat action
        getFAB().setImageDrawable(ContextCompat.getDrawable(ContactInfoActivity.this, R.drawable.ic_chat));
        setFabOriginalOnClickLister();
        getContentResolver().notifyChange(ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN, null);
        // set the contactsinfo toolbar title to new name
        getToolbarTitleTV().setText(mContact.getName());
        if (mChatRoom != null) {
            // update the chats messages to now contain new name (if changed)
            getContentResolver().notifyChange(Uri.withAppendedPath(
                    ChitterContentProvider.CONTENT_URI_CHATS_MESSAGES, mChatRoom.getId()), null);
            // also update chat members uri which changes the toolbar info within chat room (if changed)
            Uri chatMembersUri = Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_USERS_CHATS_MEMBERS, mChatRoom.getId());
            getContentResolver().notifyChange(chatMembersUri, null);
        }
    }

    @Override
    public void finishAfterTransition() {
        // send back the chatroom object t ChatRoomActivity, this is because the chat could be muted
        // and we want to update the current mChatRoom in CHatRoomActivity, so send result back activity
        Intent data = new Intent();
        data.putExtra(Constants.KEY_CHAT, mChatRoom);
        setResult(Activity.RESULT_OK, data);
        super.finishAfterTransition();
    }

    @Override
    public void onBackPressed() {
        // send back the chatroom object t ChatRoomActivity, this is because the chat could be muted
        // and we want to update the current mChatRoom in CHatRoomActivity, so send result back activity
        Intent data = new Intent();
        data.putExtra(Constants.KEY_CHAT, mChatRoom);
        setResult(Activity.RESULT_OK, data);
        super.onBackPressed();
    }
}
