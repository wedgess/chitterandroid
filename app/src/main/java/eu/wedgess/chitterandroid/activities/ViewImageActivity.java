package eu.wedgess.chitterandroid.activities;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import eu.wedgess.chitterandroid.R;

/**
 * An activity which allows the user to view the contacts image when clicked from
 * {@link ContactInfoActivity} or {@link GroupInfoActivity}, there is a share button
 * however I have not yet implemented its action yet.
 * Note that only images can be viewed, {@link eu.wedgess.chitterandroid.views.CharacterDrawable} will not.
 * <p>
 * Created by Gareth on 05/02/2017.
 */

public class ViewImageActivity extends AppCompatActivity {

    public static final String TAG = ViewImageActivity.class.getSimpleName();

    // key of image url passed with bundle
    public static final String IMAGE_URL_EXTRA = "img_url_key";
    // image URL
    private String mImageUrl;
    private ImageView mMainImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);

        // postpone the enter animation, we start postponed after image is loaded with picasso
        ActivityCompat.postponeEnterTransition(this);
        bindActivity();

        // load image
        Picasso.with(this)
                .load(mImageUrl)
                .noFade()
                .placeholder(R.drawable.ic_avatar_placeholder)
                .error(R.drawable.ic_avatar_placeholder)
                .networkPolicy(NetworkPolicy.OFFLINE)
                .into(mMainImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        // image loaded start postponed transition
                        ActivityCompat.startPostponedEnterTransition(ViewImageActivity.this);
                    }

                    @Override
                    public void onError() {
                        // fetch from server as image is not cached
                        Log.e(TAG, "Loading image error picasso");
                        Picasso.with(ViewImageActivity.this)
                                .load(mImageUrl)
                                .noFade()
                                .placeholder(R.drawable.ic_avatar_placeholder)
                                .into(mMainImageView);
                        // error start postponed transition
                        ActivityCompat.startPostponedEnterTransition(ViewImageActivity.this);
                    }
                });
    }

    private void bindActivity() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_view_image);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        mMainImageView = (ImageView) findViewById(R.id.iv_full_size);
        mImageUrl = getIntent().getExtras().getString(IMAGE_URL_EXTRA);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finishAfterTransition();
        }
        return super.onOptionsItemSelected(item);
    }
}
