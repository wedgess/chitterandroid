package eu.wedgess.chitterandroid.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Pair;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;
import eu.wedgess.chitterandroid.views.RoundedImageView;

/**
 * Chitter settings and profile settings
 * <p>
 * Created by Gareth on 04/04/2017.
 */

public class SettingsActivity extends PreferenceActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();
    private static final int MY_PROFILE_RESULT_CODE = 11;

    private RoundedImageView mProfileImageRIV;
    private TextView mUserNameTV;
    private TextView mUserStatusTV;

    @Override
    public void finish() {
        super.finish();
        // use custom animation on activity finish
        overridePendingTransition(R.anim.do_not_move, R.anim.activity_exit_to_right);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        final User user = Utils.getUserFromPrefs(SettingsActivity.this);
        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        bindActivity(user);

        getFragmentManager().beginTransaction()
                .replace(R.id.content, new SettingsPrefFragment())
                .commit();
    }

    private void bindActivity(final User user) {
        RelativeLayout root = (RelativeLayout) findViewById(R.id.rl_profile_base);
        mProfileImageRIV = (RoundedImageView) findViewById(R.id.riv_my_profile_img);
        mUserNameTV = (TextView) findViewById(R.id.tv_user_name);
        mUserStatusTV = (TextView) findViewById(R.id.tv_user_status);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(
                        SettingsActivity.this,
                        Pair.create((View) mProfileImageRIV, mProfileImageRIV.getTransitionName()));
                startActivityForResult(new Intent(SettingsActivity.this, MyProfileActivity.class),
                        MY_PROFILE_RESULT_CODE, options.toBundle());
            }
        });

        mUserNameTV.setText(user.getName());
        mUserStatusTV.setText(user.getStatus());

        if (user.getImgUrl() != null && !user.getImgUrl().isEmpty()) {
            final int imgDimens = (int) getResources().getDimension(R.dimen.chat_profile_img_size);
            Picasso.with(SettingsActivity.this)
                    .load(user.getImgUrl())
                    .resize(imgDimens, imgDimens)
                    .placeholder(R.drawable.ic_avatar_placeholder)
                    .centerCrop()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(mProfileImageRIV, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            // on error fetch the image from the server
                            Picasso.with(SettingsActivity.this)
                                    .load(user.getImgUrl())
                                    .resize(imgDimens, imgDimens)
                                    .placeholder(R.drawable.ic_avatar_placeholder)
                                    .centerCrop()
                                    .into(mProfileImageRIV);
                        }
                    });
        } else {
            mProfileImageRIV.setImageDrawable(new CharacterDrawable(user.getName(), SettingsActivity.this));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getActionBar() != null) {
            getActionBar().setTitle(getString(R.string.action_settings));
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_PROFILE_RESULT_CODE) {
            if (resultCode == RESULT_OK) {
                String imgUrl = data.getStringExtra(MyProfileActivity.MY_PROFILE_IMG_URL_KEY);
                final int imgDimens = (int) getResources().getDimension(R.dimen.chat_profile_img_size);
                Drawable original = mProfileImageRIV.getDrawable();
                if (original == null) {
                    original = ContextCompat.getDrawable(getApplicationContext(), R.drawable.profile_img_placeholder);
                }
                Picasso.with(SettingsActivity.this)
                        .load(imgUrl)
                        .resize(imgDimens, imgDimens)
                        .placeholder(original)
                        .centerCrop()
                        .into(mProfileImageRIV, new Callback() {
                            @Override
                            public void onSuccess() {
                                Log.i(TAG, "Success loading img");
                            }

                            @Override
                            public void onError() {
                                Log.i(TAG, "Error loading img");
                            }
                        });

                String name = data.getStringExtra(MyProfileActivity.MY_PROFILE_NAME_KEY);
                String status = data.getStringExtra(MyProfileActivity.MY_PROFILE_STATUS_KEY);
                mUserNameTV.setText(name);
                mUserStatusTV.setText(status);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public static class SettingsPrefFragment extends PreferenceFragment implements Preference.OnPreferenceChangeListener {

        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            Preference fontSizePref = findPreference(Constants.PREF_CHAT_FONT_SIZE);
            fontSizePref.setOnPreferenceChangeListener(this);
            setFontSizeTitle(fontSizePref, Utils.getStringPref(Constants.PREF_CHAT_FONT_SIZE, "16", getActivity()));

            Preference sendMsgOnEnterPref = findPreference(Constants.PREF_SEND_ON_ENTER);
            sendMsgOnEnterPref.setOnPreferenceChangeListener(this);
            setEnterSummary(sendMsgOnEnterPref, Utils.getBooleanPref(Constants.PREF_SEND_ON_ENTER, false, getActivity()));

            Preference notifVibrationIntensityPref = findPreference(Constants.PREF_NOTIF_VIBRATION);
            notifVibrationIntensityPref.setOnPreferenceChangeListener(this);
            setNotifVibrationSummary(notifVibrationIntensityPref,
                    Utils.getStringPref(Constants.PREF_NOTIF_VIBRATION, "1", getActivity()));

            Preference notifLightsPref = findPreference(Constants.PREF_NOTIF_LIGHTS);
            notifLightsPref.setOnPreferenceChangeListener(this);
            setNotifLightsSummary(notifLightsPref, Utils.getStringPref(Constants.PREF_NOTIF_LIGHTS, "5", getActivity()));
        }

        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if (preference.getKey().equals(Constants.PREF_SEND_ON_ENTER)) {
                setEnterSummary(preference, Boolean.valueOf(newValue.toString()));
                return true;
            } else if (preference.getKey().equals(Constants.PREF_CHAT_FONT_SIZE)) {
                setFontSizeTitle(preference, newValue);
                return true;
            } else if (preference.getKey().equals(Constants.PREF_NOTIF_VIBRATION)) {
                setNotifVibrationSummary(preference, newValue);
                return true;
            } else if (preference.getKey().equals(Constants.PREF_NOTIF_LIGHTS)) {
                setNotifLightsSummary(preference, newValue);
                return true;
            }
            return false;
        }

        private void setEnterSummary(Preference preference, boolean newValue) {
            if (newValue) {
                preference.setSummary(R.string.pref_summary_send_enter_enabled);
            } else {
                preference.setSummary(R.string.pref_summary_send_enter_disabled);
            }
        }

        private void setFontSizeTitle(Preference preference, Object newValue) {
            String valueRep = "";
            Log.i(TAG, "New Value toString: " + newValue.toString());
            switch (newValue.toString()) {
                case "14":
                    valueRep = getString(R.string.txt_small);
                    break;
                case "16":
                    valueRep = getString(R.string.txt_medium);
                    break;
                case "20":
                    valueRep = getString(R.string.txt_large);
                    break;
            }
            preference.setTitle(getString(R.string.font_size_placeholder, valueRep));
        }

        private void setNotifVibrationSummary(Preference preference, Object newValue) {
            String valueRep = "";
            switch (newValue.toString()) {
                case "0":
                    valueRep = getString(R.string.vib_none);
                    break;
                case "1":
                    valueRep = getString(R.string.vib_default);
                    break;
                case "2":
                    valueRep = getString(R.string.vib_short);
                    break;
                case "3":
                    valueRep = getString(R.string.vib_long);
                    break;
            }
            preference.setSummary(valueRep);
        }

        private void setNotifLightsSummary(Preference preference, Object newValue) {
            String valueRep = "";
            switch (newValue.toString()) {
                case "0":
                    valueRep = getString(R.string.notif_lights_none);
                    break;
                case "1":
                    valueRep = getString(R.string.notif_lights_red);
                    break;
                case "2":
                    valueRep = getString(R.string.notif_lights_green);
                    break;
                case "3":
                    valueRep = getString(R.string.notif_lights_blue);
                    break;
                case "4":
                    valueRep = getString(R.string.notif_lights_white);
                    break;
                case "5":
                    valueRep = getString(R.string.notif_lights_cyan);
                    break;
                case "6":
                    valueRep = getString(R.string.notif_lights_yellow);
                    break;
                case "7":
                    valueRep = getString(R.string.notif_lights_magenta);
                    break;
            }
            preference.setSummary(valueRep);
        }
    }

}
