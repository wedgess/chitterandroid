package eu.wedgess.chitterandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.utils.Utils;

/**
 * An activity which display the applications splash screen, which this activity is showing we load
 * the user object from shared preferences (if it exists) and set the models user. This is to avoid
 * any white screen when the application first opens.
 * The activities BG is set through a theme within {@link /res/values/styles.xml}
 * <p>
 * Created by Gareth on 13/12/2016.
 */

public class SplashActivity extends AppCompatActivity {

    private final String TAG = SplashActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // get user JSON string from prefs
        User user = Utils.getUserFromPrefs(getApplicationContext());
        if (user != null) {
            Log.i(TAG, "User String: " + user.toString());
            // user is logged in go to chat home activity
            startActivity(new Intent(SplashActivity.this, ChatHomeActivity.class));
        } else {
            // no prefs stored for user, not logged in so start the activity
            startActivity(new Intent(SplashActivity.this, LoginActivity.class));
        }
        finish();
    }
}
