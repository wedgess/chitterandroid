package eu.wedgess.chitterandroid.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.PhoneNumberFilter;
import eu.wedgess.chitterandroid.utils.Utils;

import static eu.wedgess.chitterandroid.utils.APIRequestUtility.RESPONSE_INCORRECT_PASSWORD;
import static eu.wedgess.chitterandroid.utils.APIRequestUtility.RESPONSE_NOT_REGISTERED;

/**
 * This activity is for the member to login to the application. If the user is already logged in
 * (User JSON string is present in shared preferences) then we go straight to the {@link ChatHomeActivity}
 * <p>
 * Created by Gareth on 05/12/2016.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final String TAG = LoginActivity.class.getSimpleName();
    private final String KEY_PHONE_NUM = "savedInstanceKeyPhoneNum";
    private final String KEY_PASSWORD = "savedInstanceKeyPassword";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 911;

    private TextInputLayout mPhoneNumberTIL, mPasswordTIL;
    private TextView mLoginStatusTV;
    private ProgressBar mLoginPB;

    /**
     * Save the values within the inputs on orientation change
     *
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_PHONE_NUM, mPhoneNumberTIL.getEditText().getText().toString());
        outState.putString(KEY_PASSWORD, mPasswordTIL.getEditText().getText().toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (checkPlayServices()) {
            Log.i(TAG, "Google PlayServices installed");
        }
        // if user is set in chitter model then user is logged in so start ChatHomeActivity
        if (ChitterModel.getInstance(this).getUser(getApplicationContext()) != null) {
            Intent intent = new Intent(this, ChatHomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish(); // finish this activity
        } else {
            // otherwise show log in screen
            setContentView(R.layout.activity_login);
            bindActivity();
        }
    }

    /**
     * Bind views and listeners to the activity
     */
    private void bindActivity() {
        final RelativeLayout mRegisterNewAccountBTN = (RelativeLayout) findViewById(R.id.rlayout_register);
        mPhoneNumberTIL = (TextInputLayout) findViewById(R.id.til_phone_number);
        mPasswordTIL = (TextInputLayout) findViewById(R.id.til_password);
        final Button mLoginButton = (Button) findViewById(R.id.btn_login);
        mLoginPB = (ProgressBar) findViewById(R.id.pb_logging_in);
        mLoginStatusTV = (TextView) findViewById(R.id.tv_logging_in);
        TextView registerTV = (TextView) findViewById(R.id.tv_register_value);

        // set listeners
        mRegisterNewAccountBTN.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);
        // underline the register TV
        registerTV.setPaintFlags(registerTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        final String defaultNumberTxt = getString(R.string.irish_number_start);
        mPhoneNumberTIL.getEditText().setText(defaultNumberTxt);
        mPhoneNumberTIL.getEditText().setSelection(defaultNumberTxt.length());
        // add text changed lsistener to ensure its a phone number along with a filter
        mPhoneNumberTIL.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                // always show the 08 prefix in the EditText
                if (!editable.toString().contains(getString(R.string.irish_number_start))) {
                    mPhoneNumberTIL.getEditText().setText(defaultNumberTxt);
                    Selection.setSelection(mPhoneNumberTIL.getEditText().getText(), mPhoneNumberTIL.getEditText().getText().length());

                }
            }
        });

        // add filter so phone number is 08(3,5,6,7,8,9) XXXXXXX
        mPhoneNumberTIL.getEditText().setFilters(
                new InputFilter[]{new PhoneNumberFilter(), new InputFilter.LengthFilter(10)});
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // restore the values entered into inputs from savedInstanceState Bundle
        String phoneNumber = savedInstanceState.getString(KEY_PHONE_NUM);
        String password = savedInstanceState.getString(KEY_PASSWORD);

        mPhoneNumberTIL.getEditText().setText(phoneNumber);
        if (!TextUtils.isEmpty(phoneNumber)) {
            // set cursor to the end
            mPhoneNumberTIL.getEditText().setSelection(phoneNumber.length());
        }
        mPasswordTIL.getEditText().setText(password);
        if (!TextUtils.isEmpty(password)) {
            // set cursor to the end
            mPasswordTIL.getEditText().setSelection(password.length());
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlayout_register:
                // start register activity
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
                break;
            case R.id.btn_login:
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow((getCurrentFocus() == null) ?
                        null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                // internet is available so try login
                if (Utils.isNetworkAvailable(this)) {
                    mLoginStatusTV.setVisibility(View.VISIBLE);
                    // make sure message is set to logging in
                    mLoginStatusTV.setText(R.string.msg_logging_in);
                    // and text color is white
                    mLoginStatusTV.setTextColor(ContextCompat.getColor(this, android.R.color.white));
                    // logging in show progressbar
                    mLoginPB.setVisibility(View.VISIBLE);
                    // valid irish number is 10 digits long
                    boolean validNumber = mPhoneNumberTIL.getEditText().getText().length() == 10;
                    if (!validNumber) {
                        // not valid length set error messages
                        mPhoneNumberTIL.setErrorEnabled(true);
                        mPhoneNumberTIL.setError(getString(R.string.til_error_invalid_number_length));
                    } else {
                        mPhoneNumberTIL.setErrorEnabled(false);
                    }
                    // if input fields are not empty then logUserIn and add/remove their error fields if necessary
                    if (validNumber
                            && UIHelper.tilIsNotEmpty(mPasswordTIL, getString(R.string.til_error_password_empty))) {
                        APIRequestUtility.logUserIn(mPhoneNumberTIL.getEditText().getText().toString(),
                                mPasswordTIL.getEditText().getText().toString(), this,
                                new APIRequestUtility.CommonServerCallback() {
                                    @Override
                                    public void onComplete(boolean error) {
                                        mLoginPB.setVisibility(View.GONE);
                                        mLoginStatusTV.setVisibility(View.GONE);
                                        if (!error) {
                                            // start the menu_chat_home chat activity
                                            startActivity(new Intent(LoginActivity.this, ChatHomeActivity.class));
                                            // finish this activity
                                            LoginActivity.this.finish();
                                        }
                                    }

                                    @Override
                                    public void onError(String error) {
                                        // error user is not registered or incorrect password, hide progressbar
                                        // and set appropriate error message in TextInputlayout
                                        mLoginPB.setVisibility(View.GONE);
                                        mLoginStatusTV.setVisibility(View.GONE);
                                        if (error != null) {
                                            switch (error) {
                                                case RESPONSE_NOT_REGISTERED:
                                                    mPhoneNumberTIL.setError(RESPONSE_NOT_REGISTERED);
                                                    break;
                                                case RESPONSE_INCORRECT_PASSWORD:
                                                    mPasswordTIL.setError(RESPONSE_INCORRECT_PASSWORD);
                                                    break;
                                            }
                                        }
                                    }
                                });
                    } else {
                        // invalid entries, hide progress
                        mLoginPB.setVisibility(View.GONE);
                        mLoginStatusTV.setVisibility(View.GONE);
                    }
                } else {
                    mLoginStatusTV.setVisibility(View.VISIBLE);
                    // no internet show errors
                    mLoginStatusTV.setText(R.string.msg_login_failed_no_internet);
                    mLoginStatusTV.setTextColor(Color.RED);
                }
                break;
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
                Log.e(TAG, "Google Play Services not installed.");
            } else {
                Log.e(TAG, "This device is not supported.");
                Toast.makeText(this, R.string.toast_msg_google_play_services_not_supported, Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register network change receiver
        registerReceiver(mNetworkBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregister network change broadcast reciever
        unregisterReceiver(mNetworkBroadcastReceiver);
    }

    // network change receiver for displaying no internet connection TV
    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // make sure network is available and then if no internet text is displayed hide it
            if (Utils.isNetworkAvailable(context)) {
                if (mLoginStatusTV.getVisibility() == View.VISIBLE) {
                    mLoginStatusTV.setVisibility(View.GONE);
                }
            } else {
                // if not connected and no internet message is not displayed, display it
                if (mLoginStatusTV.getVisibility() == View.GONE) {
                    mLoginStatusTV.setVisibility(View.VISIBLE);
                    // no internet show errors
                    mLoginStatusTV.setText(R.string.msg_no_internet_cannot_login);
                    mLoginStatusTV.setTextColor(Color.RED);
                }
            }
        }
    };
}
