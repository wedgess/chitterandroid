package eu.wedgess.chitterandroid.activities;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.fragments.RegisterAccountInfoFragment;
import eu.wedgess.chitterandroid.fragments.RegisterPersonalInfoFragment;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.SlidingCoordinatorLayout;

/**
 * The Register Activity is the base activity for two fragments:
 * {@link RegisterAccountInfoFragment} - used to set the members number and password
 * {@link RegisterPersonalInfoFragment} - used for setting members profile image, name and status
 * The current fragment which the user is on must be remebered for orientation changes or else
 * if the user is on the second fragment and the screen orientation is changed then they will be
 * brought back to the first fragment.
 * <p>
 * Created by Gareth on 12/12/2016.
 */

public class RegisterActivity extends AppCompatActivity {

    private final String TAG = RegisterActivity.class.getSimpleName();

    //private SlidingCoordinatorLayout mSlidingCoordinatorLayout;
    private Snackbar mSnackbar;
    private Fragment mCurrentFragment;

    // Save the current fragment on orientation change
    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return mCurrentFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // getting saved current fragment
        mCurrentFragment = (Fragment) getLastCustomNonConfigurationInstance();

        if (mCurrentFragment == null) {
            // commit the initial fragment as this is the first time the fragment is being committed
            mCurrentFragment = new RegisterAccountInfoFragment();
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.flayout_fragment_container, mCurrentFragment,
                            RegisterAccountInfoFragment.class.getSimpleName())
                    .commit();
        }
    }

    private SlidingCoordinatorLayout getCustomRootLayout() {
        return (SlidingCoordinatorLayout) findViewById(R.id.sl_register_root);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Runtime permissions are only for Marshmallow and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // send the permission request back to the fragment (this is for permission to WRITE_EXTERNAL_STORAGE/READ
            Fragment myFragment = getFragmentManager().findFragmentByTag(RegisterPersonalInfoFragment.class.getSimpleName());
            // make sure the fragment is not null, is visible and it is instance of the PersonalInfoFragment
            if (myFragment != null
                    && myFragment instanceof RegisterPersonalInfoFragment
                    && myFragment.isVisible()) {
                myFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

    // netwrk change reciver for displaying Snackbar message about being offline
    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // make sure network is available and then dismiss Snackbar
            toggleNoInternetSnackbar();
        }
    };

    /**
     * Used to toggle snackbar if internet is connected or not
     * Used also by {@link RegisterAccountInfoFragment}
     * And {@link RegisterPersonalInfoFragment}
     */
    public void toggleNoInternetSnackbar() {
        if (Utils.isNetworkAvailable(this)) {
            if (mSnackbar != null) {
                if (mSnackbar.isShown()) {
                    mSnackbar.dismiss();
                }
                mSnackbar = null;
            }
        } else {
            // if no connection then display snackbar
            mSnackbar = Snackbar.make(getCustomRootLayout(),
                    R.string.snackbar_msg_no_internet_cannot_register, Snackbar.LENGTH_INDEFINITE);
            mSnackbar.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // register network change receiver
        registerReceiver(mNetworkBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregister receiver onPause
        unregisterReceiver(mNetworkBroadcastReceiver);
    }

    /**
     * Called before committing a fragment to this activities fragment manager. As we need
     * to remeber which fragment the user was on before orientation change.
     *
     * @param fragment - the fragment to save
     */
    public void setCurrentFragment(Fragment fragment) {
        this.mCurrentFragment = fragment;
    }
}
