package eu.wedgess.chitterandroid.activities;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.adapters.ChatHomeAdapter;
import eu.wedgess.chitterandroid.callbacks.RecyclerScrollListener;
import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.fcm.ChitterFirebaseMessagingService;
import eu.wedgess.chitterandroid.helpers.ChitterModel;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.NetworkChangeService;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.ChatMainItemDivider;

import static eu.wedgess.chitterandroid.helpers.Constants.PREF_FCM_TOKEN;

/**
 * Activity which displays all the users chats, as it extends {@link BaseSelectableActivity}
 * items can be multi-selected and multi-delete action can be performed on the selection.
 */
public class ChatHomeActivity extends BaseSelectableActivity {

    private static final String TAG = ChatHomeActivity.class.getSimpleName();

    private TextView mEmptyTV;
    private RecyclerView mRecyclerView;
    // snackbar used for displaying no internet connection
    private Snackbar mSnackbar;
    // reference to coordinator layout needed for attaching snackbar to
    private CoordinatorLayout mCoordinatorLayout;
    private FloatingActionButton mNewChatFAB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_home);

        // this sets the ActionBar title in the recents screen to white and the ActionBar darker
        // only works on Lollipop and Above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
            ActivityManager.TaskDescription taskDesc = new ActivityManager.TaskDescription(getString(R.string.app_name), bm,
                    ContextCompat.getColor(this, R.color.colorPrimaryDark));
            setTaskDescription(taskDesc);
        }

        // perform sync when app starts up
        ContentResolver.requestSync(Utils.createDummyAccount(this),
                ChitterContentProvider.AUTHORITY, Bundle.EMPTY);

        // prevent there being multiple instances of activty ehrn launched with different intents
        // http://stackoverflow.com/a/7748416/3748532
        if (!isTaskRoot()) {
            finish();
            return;
        }

        /*
         * Android N has started to block NetworkChange broadcast receivers and more
         * but allows to register receivers from within the app. So while not exactly
         * a clean solution I start a service which registers the network change receiver
         * and performs sync when network changes.
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            startService(new Intent(this, NetworkChangeService.class));
        }

        // bind the widgets to activity
        bindActivity();
        // initialize the loader with a unique ID
        getLoaderManager().initLoader(Constants.LOADER_ID_CHAT_HOME, null, this);

        // fetch contacts before anything, but make sure we have permission (Runtime permission)
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            // not granted request permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS,
                            Manifest.permission.WRITE_CONTACTS}, Constants.CHECK_CONTACTS_REQUEST);
        } else {
            // granted: check registered chitter users with device contacts
            APIRequestUtility.checkContacts(Utils.getAllDeviceContacts(getContentResolver()), this);
        }

        // check if user is null
        User user = ChitterModel.getInstance(ChatHomeActivity.this).getUser(getApplicationContext());
        // The user can be null if app is opened after being in the BG for long periods.
        // So if it is get the user from shared prefs
        if (user == null) {
            // get the user in a new threas to avoid blocking menu_chat_home thread
            new Thread(new Runnable() {
                @Override
                public void run() {
                    // get user from shared preferences
                    User user = Utils.getUserFromPrefs(getApplicationContext());
                    if (user != null) {
                        ChitterModel.getInstance(ChatHomeActivity.this).setUser(user);
                        //Log.i(TAG, "User String, ChatRoom: " + user.toString());
                        checkFCMToken(user);
                    }
                }
            }).start();
        } else {
            // check the users FCM token is ok or else update it on server and in PREFS
            checkFCMToken(user);
        }



        // if coming from a notification we first open this activity so when user presses back button they get taken
        // to their menu_chat_home chat home screen rather then exiting the app
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey(ChatRoomActivity.KEY_FROM_NOTIF)) {
            Intent intent = new Intent(this, ChatRoomActivity.class);
            intent.putExtras(getIntent().getExtras());
            // start the chat room activity
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            overridePendingTransition(R.anim.activity_enter_from_right, R.anim.do_not_move);
        } else if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void checkFCMToken(User user) {
        // current app FCM Token
        String currentFcmToken = user.getFcmToken();
        if (currentFcmToken == null) {
            APIRequestUtility.updateFcmTokenOnServer(this);
            // if user in prefs is not the same as the one from model then update on server
        } else if (!Utils.getStringPref(PREF_FCM_TOKEN, currentFcmToken, this).equals(currentFcmToken)) {
            APIRequestUtility.updateFcmTokenOnServer(this);
        }
    }

    private void bindActivity() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_chat_home);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setTitle(getString(R.string.activity_title_chat_home));
            // disable toolbar title as I use a custom layout instead
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_chat_home);
        mEmptyTV = (TextView) findViewById(R.id.tv_empty_chats);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_chats);
        // 80 is the sum of views and margins so the divider is only under text and doesn't start from the edge of screen
        mRecyclerView.addItemDecoration(new ChatMainItemDivider(UIHelper.dpToPx(80, this), this));
        // set adapter, with items as null as they are loaded through the loader
        setAdapter(new ChatHomeAdapter(null, this, this));
        mRecyclerView.setAdapter(getAdapter());

        // hide/show the empty messages layout
        toggleEmptyLayout();

        mNewChatFAB = (FloatingActionButton) findViewById(R.id.fab);
        mNewChatFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check for permissions before being able to display contacts
                if (ContextCompat.checkSelfPermission(ChatHomeActivity.this, Manifest.permission.READ_CONTACTS)
                        != PackageManager.PERMISSION_GRANTED) {
                    // not granted - request permissions
                    ActivityCompat.requestPermissions(ChatHomeActivity.this,
                            new String[]{Manifest.permission.READ_CONTACTS,
                                    Manifest.permission.WRITE_CONTACTS}, Constants.NEW_MESSAGE_CONTACTS_REQUEST);
                } else {
                    // granted - start the contacts activity
                    startActivity(new Intent(ChatHomeActivity.this, ContactsActivity.class));
                    overridePendingTransition(R.anim.activity_enter_from_right, R.anim.do_not_move);
                }
            }
        });

        // hide/show FAB on scroll
        mRecyclerView.addOnScrollListener(new RecyclerScrollListener() {
            @Override
            public void show() {
                mNewChatFAB.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                mNewChatFAB.animate().translationY(mNewChatFAB.getHeight()
                        + getResources().getDimensionPixelSize(R.dimen.fab_margin))
                        .setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Log.i(TAG, "onActivityResult in activity");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // if request code is to start a new chat
        if (requestCode == Constants.NEW_MESSAGE_CONTACTS_REQUEST) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // start the contacts activity permission was granted
                startActivity(new Intent(ChatHomeActivity.this, ContactsActivity.class));
                overridePendingTransition(R.anim.activity_enter_from_right, R.anim.do_not_move);
            } else {
                Toast.makeText(this, R.string.toast_msg_no_permission, Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == Constants.CHECK_CONTACTS_REQUEST) {
            // check contacts request code then check all device contacts, if granted
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                APIRequestUtility.checkContacts(Utils.getAllDeviceContacts(getContentResolver()), this);
            } else {
                Toast.makeText(this, R.string.toast_msg_no_permission, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onItemClicked(int position) {
        if (getActionMode() != null) {
            // if action mode is started already toggle items selection
            getAdapter().toggleSelection(position);
            setActionModeTitle();
        } else {
            // otherwise adapters is in single mode so open chat room
            startChatRoomActivity(position);
        }
    }

    private void startChatRoomActivity(int position) {
        // create new intent for the ChatRoomActivity
        Intent intent = new Intent(this, ChatRoomActivity.class);
        // new bundle to store the values of the extras
        Bundle bundle = new Bundle();
        ChatRoom chatRoom = ((ChatHomeAdapter) getAdapter()).getItemAtPosition(position);
        bundle.putParcelable(Constants.KEY_CHAT, chatRoom);
        // only if its a user to user chat do we need to set the contact
        if (chatRoom.getChatType() == ChatRoom.USER_TO_USER_CHAT) {
            bundle.putParcelable(Constants.KEY_CONTACT, ((ChatHomeAdapter) getAdapter()).getContactPos(position));
        }
        //Log.i(TAG, "Contact name: " + );
        // set the bundle as the extras and start the activity while finishing this one
        intent.putExtras(bundle);
        startActivity(intent);
        overridePendingTransition(R.anim.activity_enter_from_right, R.anim.do_not_move);
    }

    @Override
    public int getActionModeMenuId() {
        return R.menu.cab_chats;
    }

    @Override
    public String getEmptySelectedTitle() {
        return getString(R.string.title_cab_select_chats);
    }

    @Override
    public int getSelectedTitleId() {
        return R.string.title_x_chats;
    }

    @Override
    public Uri getContentProviderUri() {
        return ChitterContentProvider.CONTENT_URI_CHAT_MSG_JOIN;
    }

    /**
     * If there are no chats hide the recylerview and display a TextView with drawable
     * otherwise show RecyclerView and hide textview
     */
    private void toggleEmptyLayout() {
        //Log.d(TAG, "ToggleEmptyLayout called!");
        if (getAdapter().getItemCount() > 0) {
            if (mRecyclerView.getVisibility() == View.GONE) {
                mRecyclerView.setVisibility(View.VISIBLE);
                mEmptyTV.setVisibility(View.GONE);
            }
        } else {
            if (mEmptyTV.getVisibility() == View.GONE) {
                mEmptyTV.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register network change receiver
        registerReceiver(mNetworkBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        // Register for the particular broadcast based on ACTION string
        IntentFilter filter = new IntentFilter(ChitterFirebaseMessagingService.ACTION);
        LocalBroadcastManager.getInstance(this).registerReceiver(messageBroadcastReceiver, filter);
        // ChatRoomActivity disables it so re-enable it here
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(true);
        }

        // toggle empty message layout
        if (getAdapter() != null) {
            toggleEmptyLayout();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregister network change broadcast reciever
        unregisterReceiver(mNetworkBroadcastReceiver);
        // Unregister the listener when the application is paused
        LocalBroadcastManager.getInstance(this).unregisterReceiver(messageBroadcastReceiver);
    }

    // Define the callback for what to do when data is received
    private BroadcastReceiver messageBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //Log.i(TAG, "Received result in receiver");
            if (intent.hasExtra(ChitterFirebaseMessagingService.BROADCAST_MESSAGE)) {
                // play sound for message in
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.soundin);
                mp.start();
            }
        }
    };

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        super.onLoadFinished(loader, data);
        toggleEmptyLayout();
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.action_delete_chats) {
            if (getAdapter().getSelectedItemCount() > 0) {
                // get all the selected items position in adapter
                List<Integer> selectedItems = getAdapter().getSelectedItems();
                // for each of the positions
                for (Integer pos : selectedItems) {
                    // get the message
                    ChatRoom chat = ((ChatHomeAdapter) getAdapter()).getItemAtPosition(pos);
                    if (chat != null) {
                        // delete chat from chats table in local DB only
                        DBHandler.getInstance(ChatHomeActivity.this).deleteChatById(chat.getId(), false);
                    }
                }
                // let the super class finish the actionmode
                super.onActionItemClicked(actionMode, menuItem);
            } else {
                Toast.makeText(this, R.string.toast_msg_not_items_selected, Toast.LENGTH_SHORT).show();
            }
        }
        return false;
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        mNewChatFAB.hide();
        return super.onCreateActionMode(actionMode, menu);
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {
        super.onDestroyActionMode(actionMode);
        mNewChatFAB.show();
    }

    // netwrk change reciver for displaying Snackbar message about being offline
    private BroadcastReceiver mNetworkBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // make sure network is available and then dismiss Snackbar
            if (Utils.isNetworkAvailable(context)) {
                if (mSnackbar != null) {
                    if (mSnackbar.isShown()) {
                        mSnackbar.dismiss();
                    }
                    mSnackbar = null;
                }
            } else {
                // if no connection then display snackbar
                mSnackbar = Snackbar.make(mCoordinatorLayout,
                        R.string.snackbar_msg_no_internet, Snackbar.LENGTH_LONG);
                mSnackbar.show();
            }
        }
    };
}
