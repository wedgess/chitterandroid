package eu.wedgess.chitterandroid.activities;

import android.Manifest;
import android.animation.Animator;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.ParseException;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.fragments.EditProfileNameDialogFragment;
import eu.wedgess.chitterandroid.fragments.EditProfileStatusDialogFragment;
import eu.wedgess.chitterandroid.fragments.MyProfileConfirmSaveDialogFragment;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.User;
import eu.wedgess.chitterandroid.tasks.MediaUploadTask;
import eu.wedgess.chitterandroid.utils.APIRequestUtility;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;
import eu.wedgess.chitterandroid.views.RoundedImageView;

public class MyProfileActivity extends AppCompatActivity implements
        EditProfileStatusDialogFragment.StatusDialogCompleteListener,
        EditProfileNameDialogFragment.UsernameDialogCompleteListener,
        MyProfileConfirmSaveDialogFragment.ConfirmSaveListener,
        View.OnClickListener {

    private static final String TAG = MyProfileActivity.class.getSimpleName();
    private static final int IMAGE_REQUEST_CODE = 101;
    static final String MY_PROFILE_NAME_KEY = "name";
    static final String MY_PROFILE_STATUS_KEY = "status";
    static final String MY_PROFILE_IMG_URL_KEY = "img_url";

    private RoundedImageView mUserProfileImgRIV;
    private TextView mStatusTV;
    private TextView mUsernameTV;
    private FloatingActionButton selectProfileImgFAB;
    private ProgressBar mUploadImgProgress;
    private User mUser;
    private MenuItem mSaveMenuItem;
    private String mOriginalUsername, mOriginalImgUrl, mOriginalStatus, mNewProfileImgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        ActivityCompat.postponeEnterTransition(this);
        bindActivity();
    }

    private void bindActivity() {
        mUser = Utils.getUserFromPrefs(this);
        mOriginalUsername = mUser.getName();
        mOriginalImgUrl = mUser.getImgUrl();
        mOriginalStatus = mUser.getStatus();

        mUploadImgProgress = (ProgressBar) findViewById(R.id.pb_my_profile_img);
        mUserProfileImgRIV = (RoundedImageView) findViewById(R.id.riv_my_profile_img);
        mUsernameTV = (TextView) findViewById(R.id.tv_my_profile_name);
        mStatusTV = (TextView) findViewById(R.id.tv_my_profile_status);
        final ImageButton editStatusIB = (ImageButton) findViewById(R.id.ib_edit_profile_status);
        final ImageButton editNameIB = (ImageButton) findViewById(R.id.ib_edit_profile_name);
        final TextView joinDateTV = (TextView) findViewById(R.id.tv_my_profile_join_date);
        final TextView phoneNumTV = (TextView) findViewById(R.id.tv_my_profile_phone_num);
        editStatusIB.setOnClickListener(this);
        editNameIB.setOnClickListener(this);
        selectProfileImgFAB = (FloatingActionButton) findViewById(R.id.fab_my_profile_img);
        selectProfileImgFAB.setScaleX(0);
        selectProfileImgFAB.setScaleY(0);
        selectProfileImgFAB.setOnClickListener(this);
        String createdAt = "Unknown";
        try {
            // try format the created date
            createdAt = Utils.getJoinDate(mUser.getCreatedAt());
        } catch (ParseException e) {
            e.printStackTrace();
            // if parse exception just set default format
            createdAt = mUser.getCreatedAt();
        }
        mUsernameTV.setText(mOriginalUsername);
        mStatusTV.setText(mOriginalStatus);
        joinDateTV.setText(createdAt);
        phoneNumTV.setText(mUser.getPhoneNumber());
        if (mOriginalImgUrl != null && !mOriginalImgUrl.isEmpty()) {
            final int imgDimens = (int) getResources().getDimension(R.dimen.chat_profile_img_size);
            Picasso.with(this)
                    .load(mOriginalImgUrl)
                    .resize(imgDimens, imgDimens)
                    .placeholder(R.drawable.ic_avatar_placeholder)
                    .centerCrop()
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(mUserProfileImgRIV, new Callback() {
                        @Override
                        public void onSuccess() {
                            // image loaded start postponed transition
                            ActivityCompat.startPostponedEnterTransition(MyProfileActivity.this);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.i(TAG, "Playing is get from cache");
                                    selectProfileImgFAB.clearAnimation();
                                    UIHelper.getScaleAnimatorSet(selectProfileImgFAB, 0, 1, 200).start();
                                }
                            }, 350);
                        }

                        @Override
                        public void onError() {
                            // on error fetch the image from the server
                            Picasso.with(MyProfileActivity.this)
                                    .load(mUser.getImgUrl())
                                    .resize(imgDimens, imgDimens)
                                    .placeholder(R.drawable.ic_avatar_placeholder)
                                    .centerCrop()
                                    .into(mUserProfileImgRIV);
                            // image loaded start postponed transition
                            ActivityCompat.startPostponedEnterTransition(MyProfileActivity.this);
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    Log.i(TAG, "Playing is get from URL");
                                    selectProfileImgFAB.clearAnimation();
                                    UIHelper.getScaleAnimatorSet(selectProfileImgFAB, 0, 1, 200).start();
                                }
                            }, 350);
                        }
                    });
        } else {
            mUserProfileImgRIV.setBackground(new CharacterDrawable(mUser.getName(), MyProfileActivity.this));
            ActivityCompat.startPostponedEnterTransition(MyProfileActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "Playing is else");
                    selectProfileImgFAB.clearAnimation();
                    UIHelper.getScaleAnimatorSet(selectProfileImgFAB, 0, 1, 200).start();
                }
            }, 350);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_my_profile, menu);
        mSaveMenuItem = menu.findItem(R.id.action_save_profile);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        } else if (item.getItemId() == R.id.action_save_profile) {
            saveData();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // if an image is being uploaded don't exit
        if (mUploadImgProgress.getVisibility() != View.VISIBLE) {
            if (mSaveMenuItem.isVisible()) {
                MyProfileConfirmSaveDialogFragment.newInstance()
                        .show(getFragmentManager(), TAG);
            } else {
                Animator anim = UIHelper.getScaleAnimatorSet(selectProfileImgFAB, 1, 0, 100);
                anim.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        finishAfterTransition();
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
                anim.start();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d(TAG, "Permission callback called in fragment");
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openMediaPicker();
        } else {
            Toast.makeText(MyProfileActivity.this, R.string.toast_msg_no_permission, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        if (requestCode == IMAGE_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            // hide register button and show progressbar while uploading image
            mUploadImgProgress.setVisibility(View.VISIBLE);
            Uri filePath = data.getData();
            Bitmap bitmap = null;
            try {
                //Getting the Bitmap from Gallery
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                //Setting the Bitmap to RoundedImageView
                mUserProfileImgRIV.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
            new MediaUploadTask(MyProfileActivity.this, new MediaUploadTask.UploadProgressListener() {
                @Override
                public void onUploadComplete(int responseCode, String fileLocation) {
                    // if successful upload
                    if (responseCode == 200) {
                        // replace the spaces with encoded spaces
                        String decodedFileLocation = fileLocation.replaceAll(" ", "%20");
                        mSaveMenuItem.setVisible(!mOriginalImgUrl.equals(decodedFileLocation));
                        mNewProfileImgUrl = decodedFileLocation;
                        Toast.makeText(MyProfileActivity.this, R.string.toast_msg_img_upload_success, Toast.LENGTH_LONG).show();
                    }
                    // show register button and hide progressbar upload finished due to error
                    mUploadImgProgress.setVisibility(View.GONE);
                }

                @Override
                public void onUploadError(final String error) {
                    // show register button and hide progressbar upload finished due to error
                    mUploadImgProgress.setVisibility(View.GONE);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MyProfileActivity.this, error, Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }).execute(filePath);
        }
    }

    // open media picker chooser to select image
    private void openMediaPicker() {
        Intent intent = new Intent();
        // the type which we will be picking to upload is image of any format
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.title_profile_img_chooser)), IMAGE_REQUEST_CODE);
    }

    @Override
    public void sendStatusDialogResult(String result) {
        mStatusTV.setText(result);
        mSaveMenuItem.setVisible(!result.equals(mOriginalStatus));
    }

    @Override
    public void sendUsernameDialogResult(String result) {
        mUsernameTV.setText(result);
        mSaveMenuItem.setVisible(!result.equals(mOriginalUsername));
    }

    @Override
    public void sendSaveResult(boolean save) {
        if (save) {
            saveData();
        } else {
            mSaveMenuItem.setVisible(false);
        }
        onBackPressed();
    }

    private void saveData() {
        if (mNewProfileImgUrl != null) {
            mUser.setImgUrl(mNewProfileImgUrl);
        }
        mUser.setStatus(mStatusTV.getText().toString());
        mUser.setName(mUsernameTV.getText().toString());
        APIRequestUtility.updateUserProfile(mUser.getId(), mUser.getName(), mUser.getStatus(),
                mUser.getImgUrl(), getApplicationContext());
        Log.i(TAG, "Saving info");
        // send result back to settings activity
        Intent resultIntent = new Intent();
        resultIntent.putExtra(MY_PROFILE_NAME_KEY, mUser.getName());
        resultIntent.putExtra(MY_PROFILE_STATUS_KEY, mUser.getStatus());
        resultIntent.putExtra(MY_PROFILE_IMG_URL_KEY, mUser.getImgUrl());
        setResult(RESULT_OK, resultIntent);
        mSaveMenuItem.setVisible(false);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.ib_edit_profile_status:
                EditProfileStatusDialogFragment.newInstance(mStatusTV.getText().toString())
                        .show(getFragmentManager(), TAG);
                break;
            case R.id.ib_edit_profile_name:
                EditProfileNameDialogFragment.newInstance(mUsernameTV.getText().toString())
                        .show(getFragmentManager(), TAG);
                break;
            case R.id.fab_my_profile_img:
                // make sure we have an internet connection
                if (Utils.isNetworkAvailable(MyProfileActivity.this)) {
                    // make sure we have WRITE/READ EXTERNAL storage permission
                    if (ContextCompat.checkSelfPermission(MyProfileActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MyProfileActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 911);
                    } else {
                        // we have permission so open picker
                        openMediaPicker();
                    }
                } else {
                    // no internet so show toast as image can;t be uploaded
                    Toast.makeText(MyProfileActivity.this, getString(R.string.snackbar_msg_no_internet), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
