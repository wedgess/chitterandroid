package eu.wedgess.chitterandroid.activities;

import android.Manifest;
import android.app.Activity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;

import eu.wedgess.chitterandroid.R;
import eu.wedgess.chitterandroid.adapters.ContactsAdapter;
import eu.wedgess.chitterandroid.callbacks.CreateContactListener;
import eu.wedgess.chitterandroid.db.ChitterContentProvider;
import eu.wedgess.chitterandroid.db.DBConstants;
import eu.wedgess.chitterandroid.db.DBHandler;
import eu.wedgess.chitterandroid.fragments.CreateContactDialogFragment;
import eu.wedgess.chitterandroid.helpers.Constants;
import eu.wedgess.chitterandroid.helpers.UIHelper;
import eu.wedgess.chitterandroid.model.ChatRoom;
import eu.wedgess.chitterandroid.model.Contact;
import eu.wedgess.chitterandroid.utils.Utils;
import eu.wedgess.chitterandroid.views.CharacterDrawable;
import eu.wedgess.chitterandroid.views.ChatMainItemDivider;

public class GroupInfoActivity extends BaseInfoActivity implements CreateContactDialogFragment.OnCreateDialogCloseListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    // chat toom of the group
    private ChatRoom mChatRoom;
    private ContactsAdapter mAdapter;
    private RecyclerView mParticipantsRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Bundle bundle = getIntent().getExtras();
        // get the chat room from the bundle if the key exists (will always)
        // this must be set before call to super
        if (bundle != null && bundle.containsKey(Constants.KEY_CHAT)) {
            mChatRoom = bundle.getParcelable(Constants.KEY_CHAT);
        }
        super.onCreate(savedInstanceState);
        // loader must be set after call to super
        getLoaderManager().initLoader(Constants.LOADER_ID_GROUP_INFO, null, this);
    }

    /**
     * Bind the group info stub layout to that of the superclasses view
     */
    @Override
    public void bindStubLayout() {
        // do group info view stuff here
        if (mChatRoom != null) {
            CharacterDrawable characterDrawable = null;
            // if group info img url is set use picasso to load the image from URl otherwise set character drawable
            if (mChatRoom.getGroupProfileImgUrl() != null && !TextUtils.isEmpty(mChatRoom.getGroupProfileImgUrl())) {
                final int imgDimens = (int) getResources().getDimension(R.dimen.chat_profile_img_size);
                // try fetch image from cache
                Picasso.with(this)
                        .load(mChatRoom.getGroupProfileImgUrl())
                        .resize(imgDimens, imgDimens)
                        .placeholder(R.drawable.ic_avatar_placeholder)
                        .centerCrop()
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(getToolbarLogoIV(), new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError() {
                                // fetch from server as image is not cached
                                Picasso.with(GroupInfoActivity.this)
                                        .load(mChatRoom.getGroupProfileImgUrl())
                                        .resize(imgDimens, imgDimens)
                                        .placeholder(R.drawable.ic_avatar_placeholder)
                                        .centerCrop()
                                        .into(getToolbarLogoIV());
                            }
                        });
            } else {
                characterDrawable = new CharacterDrawable(mChatRoom.getTitle(), this);
            }
            int onlineCount = 0;
            for (Contact contact : mChatRoom.getMembers()) {
                if (contact.getLastSeen() != null && contact.getLastSeen().equals(Constants.STATUS_ONLINE)) {
                    onlineCount++;
                }
            }
            String subTitle;
            // group chat so set participant count as toolbar sub title and count of online users
            if (onlineCount > 1) {
                subTitle = getString(R.string.title_x_participants_online, mChatRoom.getMembers().size(), onlineCount);
            } else {
                subTitle = getString(R.string.title_x_participants, mChatRoom.getMembers().size());
            }
            // set the toolbar data
            setCollapseToolbar(characterDrawable, mChatRoom.getTitle(), subTitle);


            TextView groupAdminTV = (TextView) findViewById(R.id.tv_group_admin);
            TextView createdAtTV = (TextView) findViewById(R.id.tv_group_created_at);
            Switch mutedSwitch = (Switch) findViewById(R.id.switch_group_muted);
            // set the recyclerviews adapter with a callback to create the contact (if not exist in device contacts)
            // if contact does not exist in device contacts then we show an add contact button so that
            // the user can add the contact to their device contacts
            mParticipantsRecycler = (RecyclerView) findViewById(R.id.recycler_participants);
            mAdapter = new ContactsAdapter(null, null, new CreateContactListener() {
                @Override
                public void onCreateContact(int position) {
                    // need to request permission to write to contacts
                    if (ContextCompat.checkSelfPermission(GroupInfoActivity.this, Manifest.permission.WRITE_CONTACTS)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(GroupInfoActivity.this,
                                new String[]{Manifest.permission.WRITE_CONTACTS}, Constants.WRITE_CONTACT_START);
                    } else {
                        // show the create contact dialog
                        CreateContactDialogFragment.newInstance(
                                ((ContactsAdapter) mParticipantsRecycler.getAdapter()).getItemAtPosition(position))
                                .show(getFragmentManager(), TAG);
                    }
                }
            }, this);
            // set adapter to recyclerview and add item decoration (the divider between items)
            mParticipantsRecycler.setAdapter(mAdapter);
            mParticipantsRecycler.addItemDecoration(new ChatMainItemDivider(UIHelper.dpToPx(80, this), this));

            // character drawables are set as the BG while images are set as drawables
            // so asume that if the background is null then allow the user to view the img full size
            if (getToolbarLogoIV().getBackground() == null) {
                getToolbarLogoIV().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startViewImageActivity(mChatRoom.getGroupProfileImgUrl());
                    }
                });
            }

            // loop through each chat member to get the admin of the chat (creator)
            for (Contact contact : mChatRoom.getMembers()) {
                if (contact.getId() == mChatRoom.getAdminId()) {
                    groupAdminTV.setText(contact.getName());
                }
            }

            // get the date the chat was created at
            String createdAt;
            try {
                createdAt = Utils.getJoinDate(mChatRoom.getCreatedAt());
            } catch (ParseException e) {
                e.printStackTrace();
                createdAt = mChatRoom.getCreatedAt();
            }
            createdAtTV.setText(createdAt);

            // mute switch to mute notifications from this chat
            mutedSwitch.setChecked(mChatRoom.isMuted());
            mutedSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (mChatRoom != null) {
                        mChatRoom.setMuted(isChecked);
                        // update chat room to set chat as muted in DB
                        DBHandler.getInstance(GroupInfoActivity.this).updateChatRoom(mChatRoom);
                    }
                }
            });
        }

    }

    /**
     * Callback for when the contact has been added to device contacts from {@link CreateContactDialogFragment}
     */
    @Override
    public void onDialogComplete() {
        // when dialog is complete notify chat members UI of change
        Uri chatMembersURI = Uri.withAppendedPath(
                ChitterContentProvider.CONTENT_URI_USERS_CHATS_MEMBERS, mChatRoom.getId());
        getContentResolver().notifyChange(chatMembersURI, null);

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        // chat member URI
        Uri providerUri = Uri.withAppendedPath(ChitterContentProvider.CONTENT_URI_USERS_CHATS_MEMBERS, mChatRoom.getId());
        return new CursorLoader(this, providerUri, null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
        // set the sub title which is the number of participants, when loader has finished loading
        // this member is online
        int onlineCount = 1;
        while (data.moveToNext()) {
            String lastSeen = data.getString(data.getColumnIndex(DBConstants.COLUMN_CONTACTS_LAST_SEEN));
            if (lastSeen != null && lastSeen.equals(Constants.STATUS_ONLINE)) {
                onlineCount++;
            }
        }
        // group chat so set participant count as toolbar sub title
        // FIX: on orientation change it resets and cannot get correct number of online participants
        if (onlineCount > 1) {
            getToolbarSubTitleTV().setText(getString(R.string.title_x_participants_online, data.getCount(), onlineCount));
        } else {
            getToolbarSubTitleTV().setText(getString(R.string.title_x_participants, data.getCount()));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void finishAfterTransition() {
        // send back the chatroom object t ChatRoomActivity, this is because the chat could be muted
        // and we want to update the current mChatRoom in CHatRoomActivity, so send result back activity
        Intent data = new Intent();
        data.putExtra(Constants.KEY_CHAT, mChatRoom);
        setResult(Activity.RESULT_OK, data);
        super.finishAfterTransition();
    }

    @Override
    public void onBackPressed() {
        // send back the chatroom object t ChatRoomActivity, this is because the chat could be muted
        // and we want to update the current mChatRoom in CHatRoomActivity, so send result back activity
        Intent data = new Intent();
        data.putExtra(Constants.KEY_CHAT, mChatRoom);
        setResult(Activity.RESULT_OK, data);
        super.onBackPressed();
    }
}
